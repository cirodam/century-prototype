﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAI 
{
    string groupID;
    IUnit unit;
    List<UnitAI> enemies;
    UnitAI target;

    UnitSync sync;

    bool started;
    bool dead;
    bool routed;

    Vector2 vectorPos;

    float lastDelta;
    float cooldown = 4;

    public UnitAI(string groupIDArg, IUnit targetArg, Vector2 startingPos)
    {
        groupID = groupIDArg;
        unit = targetArg;
        sync = new UnitSync();
        sync.unitID = unit.GetID();
        vectorPos = startingPos;
        target = null;
        enemies = new List<UnitAI>();
    }

    public void StartFight(List<UnitAI> enemiesArg)
    {
        foreach(UnitAI ai in enemiesArg)
        {
            Debug.Log("Should add enemy to unit ai");
            enemies.Add(ai);
        }
        started = true;
        routed = false;
        dead = false;
    }

    public void OnUnitKilled(string unitID)
    {
        if(target != null)
        {
            if (target.unit.GetID() == unitID)
            {
                target = null;
            }
        }
    }

    public void OnUnitRouted(string unitID)
    {
        if(target != null)
        {
            if (target.unit.GetID() == unitID)
            {
                target = null;
            }
        }
    }

    public void Update()
    {
        if(started && !routed && !dead)
        {
            if(lastDelta < cooldown)
            {
                lastDelta += Time.deltaTime;
            }

            if(target == null)
            {
                int ind = Random.Range(0, enemies.Count - 1);
                if(!enemies[ind].dead && !enemies[ind].routed)
                {
                    target = enemies[ind];
                }
                else
                {
                    enemies.RemoveAt(ind);
                }
            }
            else
            {
                if (Vector2.Distance(vectorPos, target.GetVectorPos()) > 1)
                {
                    vectorPos = Vector2.MoveTowards(vectorPos, target.GetVectorPos(), 0.05f);
                }
                else
                {
                    if(lastDelta >= cooldown)
                    {
                        target.OnAttack(unit.GetAttackStrength());
                        lastDelta = 0;
                    }
                }
            }

            sync.vectorPos = vectorPos;
        }
    }

    public void OnAttack(float damage)
    {
        unit.OnDamage(damage);
        sync.health = unit.GetHealth();

        if(unit.GetHealth() < 0.3f)
        {
            routed = true;
            MessagePump.instance.QueueMsg(new UnitRoutedMessage(groupID, unit.GetID()));
        }
        else if(unit.GetHealth() < 0.1f)
        {
            dead = true;
            MessagePump.instance.QueueMsg(new UnitKilledMessage(groupID, unit.GetID()));
        }
    }

    public Vector2 GetVectorPos()
    {
        return vectorPos;
    }

    public UnitSync GetSync()
    {
        return sync;
    }
}
