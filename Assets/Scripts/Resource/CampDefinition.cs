﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CampDefinition
{
    public string campName;
    public string campType;

    public string spritePath;
    public GameObject selectedUI;

    public List<StringValueEntry> options;

    public string GetString(string key)
    {
        foreach (StringValueEntry v in options)
        {
            if(v.key == key)
            {
                return v.value;
            }
        }
        return "";
    }

    public float GetFloat(string key)
    {
        foreach (StringValueEntry v in options)
        {
            if (v.key == key)
            {
                return float.Parse(v.value);
            }
        }
        return -1;
    }
}
