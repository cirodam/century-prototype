﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceInstance 
{
    public string resourceName;
    public float resourceAmount;
    public float lastAmount;
    public bool canNegate;

    public ResourceInstance(string newName, float startingAmount, bool newNegate)
    {
        resourceName = newName;
        resourceAmount = startingAmount;
        canNegate = newNegate;
    }
}
