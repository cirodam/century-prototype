﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stockpile : IStockpile
{
    private string ownerID;
    Dictionary<string, ResourceInstance> resources;

    public Stockpile(string newOwnerID)
    {
        ownerID = newOwnerID;
        resources = new Dictionary<string, ResourceInstance>();
    }

    public void OnPreNewDay()
    {
        foreach(ResourceInstance r in resources.Values)
        {
            r.lastAmount = r.resourceAmount;
        }
    }

    public void OnNewDay()
    {

    }

    public void OnPostNewDay()
    {

    }

    public void AddResourceAmount(string name, float amount)
    {
        if(!resources.ContainsKey(name))
        {
            resources.Add(name, new ResourceInstance(name, 0, false));
        }
        resources[name].resourceAmount += amount;
        MessagePump.instance.QueueMsg(new StockpileResourceUpdatedMessage(ownerID, name, resources[name].resourceAmount));
    }

    public int SubtractResourceAmount(string name, float amount)
    {
        if(resources.ContainsKey(name))
        {
            resources[name].resourceAmount -= amount;
            MessagePump.instance.QueueMsg(new StockpileResourceUpdatedMessage(ownerID, name, resources[name].resourceAmount));
        }
        return 0;
    }

    public bool HasResourceAmount(string name, int amount)
    {
        if(resources.ContainsKey(name))
        {
            if(resources[name].resourceAmount >= amount)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    public Dictionary<string, ResourceInstance> GetResources()
    {
        return resources;
    }

    public float GetResourceAmount(string name)
    {
        if(resources.ContainsKey(name))
        {
            return resources[name].resourceAmount;
        }
        return 0;
    }

    public float GetLastResourceAmount(string name)
    {
        if (resources.ContainsKey(name))
        {
            return resources[name].lastAmount;
        }
        return 0;
    }

    public string GetOwnerID()
    {
        return ownerID;
    }
}
