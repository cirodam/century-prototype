﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockpileRemote
{
    public Dictionary<string, ResourceInstance> resources;

    public StockpileRemote()
    {
        resources = new Dictionary<string, ResourceInstance>();
    }

    public void UpdateResourceAmount(string resourceName, float resourceAmount)
    {
        if(!resources.ContainsKey(resourceName))
        {
            resources.Add(resourceName, new ResourceInstance(resourceName, 0.0f, false));
        }
        resources[resourceName].resourceAmount = resourceAmount;
    }

    public float GetResourceAmount(string resourceName)
    {
        if(resources.ContainsKey(resourceName))
        {
            return resources[resourceName].resourceAmount;
        }
        return 0.0f;
    }

    public void OnPreNewDay()
    {
        foreach (ResourceInstance r in resources.Values)
        {
            r.lastAmount = r.resourceAmount;
        }
    }
}
