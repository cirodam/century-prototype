﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStockpile 
{
    string GetOwnerID();
    void AddResourceAmount(string name, float amount);
    int SubtractResourceAmount(string name, float amount);
    bool HasResourceAmount(string name, int amount);
    float GetResourceAmount(string name);
    float GetLastResourceAmount(string name);
    Dictionary<string, ResourceInstance> GetResources();
}
