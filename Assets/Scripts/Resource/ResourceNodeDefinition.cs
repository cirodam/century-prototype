﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceNodeDefinition 
{
    public string resourceNodeName;
    public string spritePath;
}
