﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NaturalDefinition 
{
    public string naturalName;
    public string techToSee;
    public string spritePath;
    public float prevalence;
    public string campName;
}
