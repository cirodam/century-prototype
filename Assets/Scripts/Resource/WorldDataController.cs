﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldTileData
{
    public int tileID;
    public int tileLevel;
    public string tileName;
    public float prevalance;
}

public class WorldDataController : MonoBehaviour
{
    public static WorldDataController instance;

    public List<NaturalDefinition> naturals;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public NaturalDefinition GetDataForNatural(string name)
    {
        foreach(NaturalDefinition nd in naturals)
        {
            if(nd.naturalName == name)
            {
                return nd;
            }
        }
        return null;
    }
}
