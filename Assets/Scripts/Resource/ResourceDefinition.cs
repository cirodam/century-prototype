﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceDefinition
{
    public string resourceName;
    public string resourceRole;
    public string resourceCategory;

    public string iconPath;

    public List<ResourceSlot> slots;

    public List<StringValueEntry> options;
    public float value;

    public string GetString(string key)
    {
        foreach(StringValueEntry v in options)
        {
            if(v.key == key)
            {
                return v.value;
            }
        }
        return "Absent";
    }

    public float GetFloat(string key)
    {
        foreach (StringValueEntry v in options)
        {
            if (v.key == key)
            {
                return float.Parse(v.value);
            }
        }
        return -1;
    }
}
