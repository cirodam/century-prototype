﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceSlot 
{
    public string slotName;
    public ResourceAmount resource;
}
