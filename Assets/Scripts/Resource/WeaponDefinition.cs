﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WeaponDefinition 
{
    public string weaponName;
    public float attackStrength;
}
