﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceAmount
{
    public string resourceName;
    public int resourceAmount;

    public ResourceAmount()
    {

    }

    public ResourceAmount(string newName, int newAmout)
    {
        resourceName = newName;
        resourceAmount = newAmout;
    }
}
