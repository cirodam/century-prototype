﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityTile 
{
    public bool locked;
    public int terrain;
    public string buildingID;
    
    public CityTile()
    {
        locked = false;
        terrain = 1;
        buildingID = "";
    }

    public bool IsLocked()
    {
        return locked;
    }

    public bool IsOccupied()
    {
        if(buildingID == "")
        {
            return false;
        }
        return true;
    }
}
