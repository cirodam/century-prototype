﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListenerCoupling
{
    public Object obj;
    public MessageListener listener;

    public ListenerCoupling(Object newObj, MessageListener newListener)
    {
        obj = newObj;
        listener = newListener;
    }
}

public class MessagePump : MonoBehaviour
{
    public static MessagePump instance;

    Dictionary<MessageType, List<ListenerCoupling>> listeners;

    Queue<Message> messageQueue;

    public List<MessageType> debugList;

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
            listeners = new Dictionary<MessageType, List<ListenerCoupling>>();
            messageQueue = new Queue<Message>();

            if(debugList == null)
            {
                debugList = new List<MessageType>();
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        while (messageQueue.Count > 0)
        {
            PostMsg(messageQueue.Dequeue());
        }
    }

    public void Register(Object obj, MessageType type, MessageListener listener)
    {
        if (!listeners.ContainsKey(type))
        {
            listeners.Add(type, new List<ListenerCoupling>());
        }

        listeners[type].Add(new ListenerCoupling(obj, listener));
    }

    public void Unregister(MessageType type, Object obj, MessageListener listener)
    {
        if (listeners.ContainsKey(type))
        {
            foreach(ListenerCoupling coupling in listeners[type])
            {
                if(coupling.obj == obj && coupling.listener == listener)
                {
                    listeners[type].Remove(coupling);
                    return;
                }
            }
        }
    }

    public void QueueMsg(Message msg)
    {
        messageQueue.Enqueue(msg);
    }

    public void PostMsg(Message msg)
    {
        if(debugList.Contains(msg.GetMessageType()))
        {
            Debug.Log("Sending " + msg.GetMessageType());
        }
        List<ListenerCoupling> expired = new List<ListenerCoupling>();

        if (listeners.ContainsKey(msg.GetMessageType()))
        {
            foreach (ListenerCoupling coupling in listeners[msg.GetMessageType()])
            {
                if(coupling.obj != null)
                {
                    coupling.listener(msg);
                }
                else
                {
                    expired.Add(coupling);
                }
            }
        }

        foreach(ListenerCoupling l in expired)
        {
            listeners[msg.GetMessageType()].Remove(l);
        }
    }
}
