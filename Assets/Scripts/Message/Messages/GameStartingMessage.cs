﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartingMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.GAME_STARTING_MESSAGE;
    }
}
