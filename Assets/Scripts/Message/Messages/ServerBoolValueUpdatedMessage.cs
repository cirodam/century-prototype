﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerBoolValueUpdatedMessage : Message
{
    public string affectedID;
    public string key;
    public bool value;

    public ServerBoolValueUpdatedMessage(string affectedIDArg, string keyArg, bool valueArg)
    {
        affectedID = affectedIDArg;
        key = keyArg;
        value = valueArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.SERVER_BOOL_VALUE_UPDATED_MESSAGE;
    }
}
