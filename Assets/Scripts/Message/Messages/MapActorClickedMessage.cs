﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapActorClickedMessage : Message
{
    public string mapActorID;

    public MapActorClickedMessage(string newID)
    {
        mapActorID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MAP_ACTOR_CLICKED_MESSAGE;
    }
}
