﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitRoutedMessage : Message
{
    public string groupID;
    public string unitID;

    public UnitRoutedMessage(string groupIDArg, string unitIDArg)
    {
        groupID = groupIDArg;
        unitID = unitIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_ROUTED_MESSAGE;
    }
}
