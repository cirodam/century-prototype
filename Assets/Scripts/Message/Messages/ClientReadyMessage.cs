﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientReadyMessage : Message
{
    public string clientID;
    public ClientReadyMessage(string newClientID)
    {
        clientID = newClientID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_READY_MESSAGE;
    }
}
