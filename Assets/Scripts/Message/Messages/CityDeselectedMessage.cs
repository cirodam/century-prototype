﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityDeselectedMessage : Message
{
    public int cityID;

    public CityDeselectedMessage(int newID)
    {
        cityID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CITY_DESELECTED_MESSAGE;
    }
}
