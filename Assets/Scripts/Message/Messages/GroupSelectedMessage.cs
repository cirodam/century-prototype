﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupSelectedMessage : Message
{
    public int groupID;

    public GroupSelectedMessage(int newID)
    {
        groupID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_SELECTED_MESSAGE;
    }
}
