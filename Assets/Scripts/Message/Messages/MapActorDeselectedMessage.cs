﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapActorDeselectedMessage : Message
{
    public string mapActorID;

    public MapActorDeselectedMessage(string newID)
    {
        mapActorID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MAP_ACTOR_DESELECTED_MESSAGE;
    }
}
