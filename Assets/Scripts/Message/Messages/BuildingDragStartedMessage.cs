﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDragStartedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_DRAG_STARTED_MESSAGE;
    }
}
