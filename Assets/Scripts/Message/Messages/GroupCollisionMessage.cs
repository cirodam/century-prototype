﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupCollisionMessage : Message
{
    public string groupID;
    public string hitID;

    public GroupCollisionMessage(string groupIDArg, string hitIDArg)
    {
        groupID = groupIDArg;
        hitID = hitIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_COLLISION_MESSAGE;
    }
}
