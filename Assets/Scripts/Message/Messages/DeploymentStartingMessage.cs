﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeploymentStartingMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.DEPLOYMENT_STARTING_MESSAGE;
    }
}
