﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewDayMessage : Message
{
    public int day;
    public int month;
    public int year;

    public NewDayMessage(int newDay, int newMonth, int newYear)
    {
        day   = newDay;
        month = newMonth;
        year  = newYear;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.NEW_DAY_MESSAGE;
    }
}
