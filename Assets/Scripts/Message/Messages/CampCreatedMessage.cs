﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampCreatedMessage : Message
{
    public string campID;
    public string ownerID;
    public MapPos worldPos;
    public string campName;
    public string campType;

    public CampCreatedMessage(string campIDArg, string ownerIDArg, MapPos worldPosArg, string campNameArg, string campTypeArg)
    {
        campID = campIDArg;
        ownerID = ownerIDArg;
        worldPos = worldPosArg;
        campName = campNameArg;
        campType = campTypeArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CAMP_CREATED_MESSAGE;
    }
}
