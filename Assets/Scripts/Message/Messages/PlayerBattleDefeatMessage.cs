﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBattleDefeatMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.PLAYER_BATTLE_DEFEAT_MESSAGE;
    }
}
