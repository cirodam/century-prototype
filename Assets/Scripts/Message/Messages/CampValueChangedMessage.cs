﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampValueChangedMessage : Message
{
    public string campID;
    public string key;
    public object value;

    public CampValueChangedMessage(string campIDArg, string keyArg, object valueArg)
    {
        campID = campIDArg;
        key    = keyArg;
        value  = valueArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CAMP_VALUE_CHANGED_MESSAGE;
    }
}
