﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupCollisionHostileMessage : Message
{
    public int initiatorID;
    public int recevierID;

    public GroupCollisionHostileMessage(int newInitiator, int newReceiver)
    {
        initiatorID = newInitiator;
        recevierID = newReceiver;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_COLLISION_HOSTILE_MESSAGE;
    }
}
