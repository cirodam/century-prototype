﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerIntValueUpdatedMessage : Message
{
    public string affectedID;
    public string childID;
    public string key;
    public int value;

    public ServerIntValueUpdatedMessage(string affectedIDArg, string childIDArg, string keyArg, int valueArg)
    {
        affectedID = affectedIDArg;
        childID    = childIDArg;
        key        = keyArg;
        value      = valueArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.SERVER_INT_VALUE_UPDATED_MESSAGE;
    }
}
