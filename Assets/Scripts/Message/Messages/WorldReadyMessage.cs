﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldReadyMessage : Message
{
    public int[,,] grid;
    public List<Natural> naturals;
    public int worldSizeX;
    public int worldSizeY;
    public int zLevels;

    public WorldReadyMessage(int[,,] newGrid, List<Natural> naturalsArg, int newSizeX, int newSizeY, int newZLevels)
    {
        grid = newGrid;
        naturals = naturalsArg;
        worldSizeX = newSizeX;
        worldSizeY = newSizeY;
        zLevels = newZLevels;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.WORLD_READY_MESSAGE;
    }
}
