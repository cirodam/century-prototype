﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerStringValueUpdatedMessage : Message
{
    public string affectedID;
    public string key;
    public string value;

    public ServerStringValueUpdatedMessage(string affectedIDArg, string keyArg, string valueArg)
    {
        affectedID = affectedIDArg;
        key = keyArg;
        value = valueArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.SERVER_STRING_VALUE_UPDATED_MESSAGE;
    }
}
