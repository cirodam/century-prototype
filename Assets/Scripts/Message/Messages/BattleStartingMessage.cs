﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStartingMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.BATTLE_STARTING_MESSAGE;
    }
}
