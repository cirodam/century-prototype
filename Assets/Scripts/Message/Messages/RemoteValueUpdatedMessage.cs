﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteValueUpdatedMessage : Message
{
    public string id;

    public RemoteValueUpdatedMessage(string idArg)
    {
        id = idArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.REMOTE_VALUE_UPDATED_MESSAGE;
    }
}
