﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityRemoteUpdatedMessage : Message
{
    public string cityID;

    public CityRemoteUpdatedMessage(string newCityID)
    {
        cityID = newCityID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CITY_REMOTE_UPDATED_MESSAGE;
    }
}
