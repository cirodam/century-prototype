﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDragCompletedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_DRAG_COMPLETED_MESSAGE;
    }
}
