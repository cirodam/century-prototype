﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationPausedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.SIMULATION_PAUSED_MESSAGE;
    }
}
