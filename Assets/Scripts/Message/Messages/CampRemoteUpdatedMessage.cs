﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampRemoteUpdatedMessage : Message
{
    public string campID;

    public CampRemoteUpdatedMessage(string newID)
    {
        campID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CAMP_REMOTE_UPDATED_MESSAGE;
    }
}
