﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingActorClickedMessage : Message
{
    public string buildingID;
    public string cityID;

    public BuildingActorClickedMessage(string newBuilding, string newCity)
    {
        buildingID = newBuilding;
        cityID = newCity;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_ACTOR_CLICKED_MESSAGE;
    }
}
