﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupMapPosUpdatedMessage : Message
{
    public string groupID;
    public MapPos oldMapPos;
    public MapPos newMapPos;

    public GroupMapPosUpdatedMessage(string groupIDArg, MapPos oldMapPosArg, MapPos newMapPosArg)
    {
        groupID = groupIDArg;
        oldMapPos = oldMapPosArg;
        newMapPos = newMapPosArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_MAP_POS_UPDATED_MESSAGE;
    }
}
