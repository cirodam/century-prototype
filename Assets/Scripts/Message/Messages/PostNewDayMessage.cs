﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostNewDayMessage : Message
{
    public int day;
    public int month;
    public int year;

    public PostNewDayMessage(int newDay, int newMonth, int newYear)
    {
        day   = newDay;
        month = newMonth;
        year  = newYear;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.POST_NEW_DAY_MESSAGE;
    }
}
