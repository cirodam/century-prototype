﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingRemovedFromCityMessage : Message
{
    public string cityID;
    public int buildingID;

    public BuildingRemovedFromCityMessage(string newCityID, int newBuildingID)
    {
        cityID = newCityID;
        buildingID = newBuildingID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_REMOVED_FROM_CITY_MESSAGE;
    }
}
