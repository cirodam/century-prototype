﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapActorCreatedMessage : Message
{
    public string mapActorID;
    public IMapActor mapActor;

    public MapActorCreatedMessage(string newID, IMapActor newActor)
    {
        mapActorID = newID;
        mapActor = newActor;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MAP_ACTOR_CREATED_MESSAGE;
    }
}
