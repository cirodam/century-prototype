﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCreatedMessage : Message
{
    public string ownerID;
    public string groupID;
    public string unitID;

    public string title;
    public int count;

    public List<ValueEntry> options;

    public UnitCreatedMessage(string ownerIDArg, string groupIDArg, string unitIDArg, string titleArg, int countArg, List<ValueEntry> optionsArg)
    {
        ownerID = ownerIDArg;
        groupID = groupIDArg;
        unitID  = unitIDArg;
        title   = titleArg;
        count   = countArg;
        options = optionsArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_CREATED_MESSAGE;
    }
}
