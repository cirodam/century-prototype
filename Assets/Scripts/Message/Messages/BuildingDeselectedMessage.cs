﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDeselectedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_DESELECTED_MESSAGE;
    }
}
