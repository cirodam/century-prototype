﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBattleVictoryMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.PLAYER_BATTLE_VICTORY_MESSAGE;
    }
}
