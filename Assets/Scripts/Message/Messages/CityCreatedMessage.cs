﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityCreatedMessage : Message
{
    public string cityID;
    public string ownerID;
    public string cityName;
    public MapPos worldPos;

    public CityTile[,] cityMap;
    public bool coastal;

    public List<ValueEntry> options;

    public CityCreatedMessage(string ownerIDArg, string cityIDArg, string cityNameArg, MapPos worldPosArg, CityTile[,] cityMapArg, bool coastalArg, List<ValueEntry> optionsArg)
    {
        ownerID  = ownerIDArg;
        cityID   = cityIDArg;
        cityName = cityNameArg;
        worldPos = worldPosArg;
        cityMap  = cityMapArg;
        coastal  = coastalArg;
        options  = optionsArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CITY_CREATED_MESSAGE;
    }
}
