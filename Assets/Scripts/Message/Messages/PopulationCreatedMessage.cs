﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulationCreatedMessage : Message
{
    int populationID;

    public PopulationCreatedMessage(int newID)
    {
        populationID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.POPULATION_CREATED_MESSAGE;
    }
}
