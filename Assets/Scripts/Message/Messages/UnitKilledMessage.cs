﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitKilledMessage : Message
{
    public string groupID;
    public string unitID;

    public UnitKilledMessage(string groupIDArg, string unitIDArg)
    {
        groupID = groupIDArg;
        unitID = unitIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_KILLED_MESSAGE;
    }
}
