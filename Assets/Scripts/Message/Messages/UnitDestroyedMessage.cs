﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDestroyedMessage : Message
{
    public string groupID;
    public string unitID;

    public UnitDestroyedMessage(string groupIDArg, string unitIDArg)
    {
        groupID = groupIDArg;
        unitID  = unitIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_DESTROYED_MESSAGE;
    }
}
