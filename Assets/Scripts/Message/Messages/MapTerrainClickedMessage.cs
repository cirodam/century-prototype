﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTerrainClickedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.MAP_TERRAIN_CLICKED_MESSAGE;
    }
}
