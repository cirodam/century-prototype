﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientChangeBuildingValueIntent : Message
{

    public string clientID;
    public string cityID;
    public string buildingID;

    public string key;
    public object value;

    public ClientChangeBuildingValueIntent(string clientIDArg, string cityIDArg, string buildingIDArg, string keyArg, object valueArg)
    {
        clientID   = clientIDArg;
        cityID     = cityIDArg;
        buildingID = buildingIDArg;
        key        = keyArg;
        value      = valueArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_CHANGE_BUILDING_VALUE_INTENT;
    }
}
