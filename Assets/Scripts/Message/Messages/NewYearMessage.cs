﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewYearMessage : Message
{
    public int year;

    public NewYearMessage(int newYear)
    {
        year = newYear;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.NEW_YEAR_MESSAGE;
    }
}
