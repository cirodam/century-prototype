﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockpileCreatedMessage : Message
{
    public string ownerID;
    public IStockpile stockpile;

    public StockpileCreatedMessage(string newOwnerID, IStockpile newStockpile)
    {
        ownerID = newOwnerID;
        stockpile = newStockpile;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.STOCKPILE_CREATED_MESSAGE;
    }
}
