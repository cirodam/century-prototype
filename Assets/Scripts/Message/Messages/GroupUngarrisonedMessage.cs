﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupUngarrisonedMessage : Message
{
    public string groupID;
    public string cityID;

    public GroupUngarrisonedMessage(string newGroup, string newCity)
    {
        groupID = newGroup;
        cityID = newCity;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_UNGARRISONED_MESSAGE;
    }
}
