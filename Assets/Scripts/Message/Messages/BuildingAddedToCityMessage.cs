﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingAddedToCityMessage : Message
{
    public string cityID;
    public string buildingID;
    public string buildingName;
    public MapPos pos;
    public BuildingFacing facing;
    public GenericDictionary options;

    public BuildingAddedToCityMessage(string cityIDArg, string buildingIDArg, string buildingNameArg, MapPos posArg, BuildingFacing facingArg, GenericDictionary optionsArg)
    {
        cityID       = cityIDArg;
        buildingID   = buildingIDArg;
        buildingName = buildingNameArg;
        pos          = posArg;
        facing       = facingArg;
        options      = optionsArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_ADDED_TO_CITY_MESSAGE;
    }
}
