﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupGroupCollisionMessage : Message
{
    public string initiator;
    public string receiver;

    public GroupGroupCollisionMessage(string newInitiator, string newReceiver)
    {
        initiator = newInitiator;
        receiver  = newReceiver;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_GROUP_COLLISION_MESSAGE;
    }
}
