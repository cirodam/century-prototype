﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupDestroyedMessage : Message
{
    public string groupID;

    public GroupDestroyedMessage(string groupIDArg)
    {
        groupID = groupIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_DESTROYED_MESSAGE;
    }
}
