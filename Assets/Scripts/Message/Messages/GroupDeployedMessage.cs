﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupDeployedMessage : Message
{
    public string groupID;
    public string defendableID;
    public MapPos worldPos;

    public GroupDeployedMessage(string groupIDArg, string defendableIDArg, MapPos worldPosArg)
    {
        groupID = groupIDArg;
        defendableID = defendableIDArg;
        worldPos = worldPosArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_DEPLOYED_MESSAGE;
    }
}
