﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientMoveGroupIntent : Message
{
    public string clientID;
    public string groupID;
    public MapPos startingPos;
    public MapPos endingPos;

    public ClientMoveGroupIntent(string newClientID, string newGroupID, MapPos newStartingPos, MapPos newEndingPos)
    {
        clientID = newClientID;
        groupID = newGroupID;
        startingPos = newStartingPos;
        endingPos = newEndingPos;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_MOVE_GROUP_INTENT;
    }
}
