﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampActorClickedMessage : Message
{
    public string campID;

    public CampActorClickedMessage(string newID)
    {
        campID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CAMP_ACTOR_CLICKED_MESSAGE;
    }
}
