﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupCityCollisionMessage : Message
{
    public string groupID;
    public string cityID;

    public GroupCityCollisionMessage(string newGroup, string newCity)
    {
        groupID = newGroup;
        cityID  = newCity;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_CITY_COLLISION_MESSAGE;
    }
}
