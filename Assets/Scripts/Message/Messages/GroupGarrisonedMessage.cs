﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupGarrisonedMessage : Message
{
    public string groupID;
    public string cityID;

    public GroupGarrisonedMessage(string newGroup, string newCity)
    {
        groupID = newGroup;
        cityID = newCity;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_GARRISONED_MESSAGE;
    }
}
