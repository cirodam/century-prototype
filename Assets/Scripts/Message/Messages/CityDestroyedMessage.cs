﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityDestroyedMessage : Message
{
    public string cityID;

    public CityDestroyedMessage(string cityIDArg)
    {
        cityID = cityIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CITY_DESTROYED_MESSAGE;
    }
}
