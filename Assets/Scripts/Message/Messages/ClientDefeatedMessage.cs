﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientDefeatedMessage : Message
{
    public int clientID;

    public ClientDefeatedMessage(int newClientID)
    {
        clientID = newClientID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_DEFEATED_MESSAGE;
    }
}
