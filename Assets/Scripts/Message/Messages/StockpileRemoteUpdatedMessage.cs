﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockpileRemoteUpdatedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.STOCKPILE_REMOTE_UPDATED_MESSAGE;
    }
}
