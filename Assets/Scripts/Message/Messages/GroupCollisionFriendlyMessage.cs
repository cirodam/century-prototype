﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupCollisionFriendlyMessage : Message
{
    public int initiatorID;
    public int recevierID;

    public GroupCollisionFriendlyMessage(int newInitiator, int newReceiver)
    {
        initiatorID = newInitiator;
        recevierID = newReceiver;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_COLLISION_FRIENDLY_MESSAGE;
    }
}
