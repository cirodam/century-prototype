﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerPresentMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.SERVER_PRESENT_MESSAGE;
    }
}
