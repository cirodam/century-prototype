﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationUnpausedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.SIMULATION_UNPAUSED_MESSAGE;
    }
}
