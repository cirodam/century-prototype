﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitValueUpdatedMessage : Message
{
    public string groupID;
    public string unitID;

    public string key;
    public object value;

    public UnitValueUpdatedMessage(string groupIDArg, string unitIDArg, string keyArg, object valueArg)
    {
        groupID = groupIDArg;
        unitID = unitIDArg;
        key = keyArg;
        value = valueArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_VALUE_UPDATED_MESSAGE;
    }
}
