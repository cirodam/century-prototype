﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientBattlePendingMessage : Message
{
    public string battleID;

    public string attackerID;
    public string defenderID;

    public ClientBattlePendingMessage(string battleIDArg, string attackerIDArg, string defenderIDArg)
    {
        battleID = battleIDArg;

        attackerID = attackerIDArg;
        defenderID = defenderIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_BATTLE_PENDING_MESSAGE;
    }
}
