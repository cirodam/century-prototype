﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientPresentMessage : Message
{
    public string clientID;

    public ClientPresentMessage(string newClientID)
    {
        clientID = newClientID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_PRESENT_MESSAGE;
    }
}
