﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupMovingAlongPathMessage : Message
{
    public string groupID;
    public List<MapPos> path;

    public GroupMovingAlongPathMessage(string groupIDArg, List<MapPos> pathArg)
    {
        groupID = groupIDArg;
        path = pathArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_MOVING_ALONG_PATH_MESSAGE;
    }
}
