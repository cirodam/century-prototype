﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDeployedMessage : Message
{
    public MapPos tilePos;
    public int groupID;
    public IUnit unit;

    public UnitDeployedMessage(MapPos newTilePos, int newGroupID, IUnit newUnit)
    {
        tilePos = newTilePos;
        groupID = newGroupID;
        unit = newUnit;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_DEPLOYED_MESSAGE;
    }
}
