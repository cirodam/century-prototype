﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSyncMessage : Message
{
    public string battleID;
    public List<UnitSync> syncs;

    public BattleSyncMessage(string battleIDArg, List<UnitSync> syncsArg)
    {
        battleID = battleIDArg;
        syncs = syncsArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BATTLE_SYNC_MESSAGE;
    }
}
