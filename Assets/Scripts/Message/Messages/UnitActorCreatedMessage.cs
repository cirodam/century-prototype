﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitActorCreatedMessage : Message
{
    public UnitActor unitActor;

    public UnitActorCreatedMessage(UnitActor newUnit)
    {
        unitActor = newUnit;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_ACTOR_CREATED_MESSAGE;
    }
}
