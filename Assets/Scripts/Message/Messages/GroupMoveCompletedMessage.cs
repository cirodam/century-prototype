﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupMoveCompletedMessage : Message
{
    public string groupID;

    public GroupMoveCompletedMessage(string newGroup)
    {
        groupID = newGroup;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_MOVE_COMPLETED_MESSAGE;
    }
}
