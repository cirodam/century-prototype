﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockpileResourceUpdatedMessage : Message
{
    public string ownerID;
    public string resourceName;
    public float resourceAmount;

    public StockpileResourceUpdatedMessage(string newOwner, string newResourceName, float newResourceAmount)
    {
        ownerID = newOwner;
        resourceName = newResourceName;
        resourceAmount = newResourceAmount;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.STOCKPILE_RESOURCE_UPDATED_MESSAGE;
    }
}
