﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityValueUpdatedMessage : Message
{
    public string cityID;
    public string key;
    public object value;

    public CityValueUpdatedMessage(string cityIDArg, string keyArg, object valArg)
    {
        cityID = cityIDArg;
        key = keyArg;
        value = valArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CITY_VALUE_UPDATED_MESSAGE;
    }
}
