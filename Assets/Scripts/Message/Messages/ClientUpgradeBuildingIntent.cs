﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientUpgradeBuildingIntent : Message
{
    public string clientID;
    public string cityID;
    public string buildingID;

    public ClientUpgradeBuildingIntent(string clientIDArg, string cityIDArg, string buildingIDArg)
    {
        clientID = clientIDArg;
        cityID = cityIDArg;
        buildingID = buildingIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_UPGRADE_BUILDING_INTENT;
    }
}
