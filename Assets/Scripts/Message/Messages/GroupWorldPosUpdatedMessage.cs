﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupWorldPosUpdatedMessage : Message
{
    public string groupID;
    public Vector2 newPos;

    public GroupWorldPosUpdatedMessage(string newID, Vector2 newPosArg)
    {
        groupID = newID;
        newPos = newPosArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_WORLD_POS_UPDATED_MESSAGE;
    }
}
