﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllClientsPresentMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.ALL_CLIENTS_PRESENT_MESSAGE;
    }
}
