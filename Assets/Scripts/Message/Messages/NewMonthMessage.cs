﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewMonthMessage : Message
{
    public int month;

    public NewMonthMessage(int newMonth)
    {
        month = newMonth;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.NEW_MONTH_MESSAGE;
    }
}
