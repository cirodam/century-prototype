﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingValueUpdatedMessage : Message
{
    public string cityID;
    public string buildingID;

    public string key;
    public object value;

    public BuildingValueUpdatedMessage(string cityIDArg, string buildingIDArg, string keyArg, object valueArg)
    {
        cityID     = cityIDArg;
        buildingID = buildingIDArg;
        key        = keyArg;
        value      = valueArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_VALUE_UPDATED_MESSAGE;
    }
}
