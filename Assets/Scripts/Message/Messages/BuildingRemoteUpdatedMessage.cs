﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingRemoteUpdatedMessage : Message
{
    public string buildingID;

    public BuildingRemoteUpdatedMessage(string buildingIDArg)
    {
        buildingID = buildingIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_REMOTE_UPDATED_MESSAGE;
    }
}
