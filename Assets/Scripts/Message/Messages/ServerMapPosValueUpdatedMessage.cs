﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerMapPosValueUpdatedMessage : Message
{
    public string affectedID;
    public string key;
    public MapPos value;

    public ServerMapPosValueUpdatedMessage(string affectedIDArg, string keyArg, MapPos valueArg)
    {
        affectedID = affectedIDArg;
        key = keyArg;
        value = valueArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.SERVER_MAPPOS_VALUE_UPDATED_MESSAGE;
    }
}
