﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreNewDayMessage : Message
{
    public int day;
    public int month;
    public int year;

    public PreNewDayMessage(int newDay, int newMonth, int newYear)
    {
        day   = newDay;
        month = newMonth;
        year  = newYear;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.PRE_NEW_DAY_MESSAGE;
    }
}
