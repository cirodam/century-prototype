﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStateChangedMessage : Message
{
    public string battleID;
    public BattleState newState;

    public BattleStateChangedMessage(string battleIDArg, BattleState newStateArg)
    {
        battleID = battleIDArg;
        newState = newStateArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BATTLE_STATE_CHANGED_MESSAGE;
    }
}
