﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCreatedMessage : Message
{
    public string ownerID;
    public string cityID;
    public string buildingID;
    public string buildingName;
    public int level;
    public float upkeep;

    public MapPos cityPos;
    public BuildingFacing facing;

    public List<ValueEntry> options;

    public BuildingCreatedMessage(string ownerIDArg, string cityIDArg, string buildingIDArg, string buildingNameArg, int levelArg, float upkeepArg, MapPos cityPosArg, BuildingFacing facingArg, List<ValueEntry> optionsArg)
    {
        ownerID      = ownerIDArg;
        cityID       = cityIDArg;
        buildingID   = buildingIDArg;
        buildingName = buildingNameArg;
        level        = levelArg;
        upkeep       = upkeepArg;
        cityPos      = cityPosArg;
        facing       = facingArg;
        options      = optionsArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_CREATED_MESSAGE;
    }
}
