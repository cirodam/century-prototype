﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupDeselectedMessage : Message
{
    public int groupID;

    public GroupDeselectedMessage(int newID)
    {
        groupID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_DESELECTED_MESSAGE;
    }
}
