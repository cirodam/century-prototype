﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeploymentAreaDefinedMessage : Message
{
    public string battleID;
    public string groupID;
    public List<MapPos> deploymentArea;

    public DeploymentAreaDefinedMessage(string battleIDArg, string groupIDArg, List<MapPos> areaArg)
    {
        battleID       = battleIDArg;
        groupID        = groupIDArg;
        deploymentArea = areaArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.DEPLOYMENT_AREA_DEFINED_MESSAGE;
    }
}
