﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapActorSelectedMessage : Message
{
    public string mapActorID;

    public MapActorSelectedMessage(string newID)
    {
        mapActorID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.MAP_ACTOR_SELECTED_MESSAGE;
    }
}
