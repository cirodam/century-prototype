﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupActorClickedMessage : Message
{
    public string groupID;

    public GroupActorClickedMessage(string newGroup)
    {
        groupID = newGroup;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_ACTOR_CLICKED_MESSAGE;
    }
}
