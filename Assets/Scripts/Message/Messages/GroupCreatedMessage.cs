﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupCreatedMessage : Message
{
    public string ownerID;
    public string groupID;
    public string title;

    public string cityID;
    public MapPos worldPos;
    public bool garrison;
    public MovementType movementType;

    public List<ValueEntry> options;

    public GroupCreatedMessage(string ownerIDArg, string groupIDArg, string titleArg, string cityIDArg, MapPos worldPosArg, MovementType movementArg, bool garrisonArg, List<ValueEntry> optionsArg)
    {
        ownerID      = ownerIDArg;
        groupID      = groupIDArg;
        title        = titleArg;
        cityID       = cityIDArg;
        worldPos     = worldPosArg;
        garrison     = garrisonArg;
        options      = optionsArg;
        movementType = movementArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_CREATED_MESSAGE;
    }
}
