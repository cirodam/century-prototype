﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientAddBuildingToCityIntent : Message
{
    public string clientID;
    public string cityID;
    public string buildingName;
    public MapPos buildingPos;
    public BuildingFacing buildingFacing;

    public ClientAddBuildingToCityIntent(string newClientID, string newCityID, string newBuildingName, MapPos newBuildingPos, BuildingFacing newBuildingFacing)
    {
        clientID       = newClientID;
        cityID         = newCityID;
        buildingName   = newBuildingName;
        buildingPos    = newBuildingPos;
        buildingFacing = newBuildingFacing;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_ADD_BUILDING_TO_CITY_INTENT;
    }
}
