﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityOwnerChangedMessage : Message
{
    public int cityID;
    public int oldOwnerID;
    public int ownerID;

    public CityOwnerChangedMessage(int newCity, int oldOwner, int newOwner)
    {
        cityID = newCity;
        oldOwnerID = oldOwner;
        ownerID = newOwner;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CITY_OWNER_CHANGED_MESSAGE;
    }
}
