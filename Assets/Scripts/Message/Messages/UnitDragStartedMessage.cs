﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDragStartedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_DRAG_STARTED_MESSAGE;
    }
}
