﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitySelectedMessage : Message
{
    public string cityID;

    public CitySelectedMessage(string newID)
    {
        cityID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CITY_SELECTED_MESSAGE;
    }
}
