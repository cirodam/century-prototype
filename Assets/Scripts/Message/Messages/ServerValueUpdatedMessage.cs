﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerValueUpdatedMessage : Message
{
    public string id;
    public string key;
    public object val;
    public ValueType type;

    public ServerValueUpdatedMessage(string idArg, string keyArg, object valArg, ValueType typeArg)
    {
        id   = idArg;
        key  = keyArg;
        val  = valArg;
        type = typeArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.SERVER_VALUE_UPDATED_MESSAGE;
    }
}
