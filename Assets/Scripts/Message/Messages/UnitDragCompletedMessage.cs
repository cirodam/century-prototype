﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDragCompletedMessage : Message
{
    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_DRAG_COMPLETED_MESSAGE;
    }
}
