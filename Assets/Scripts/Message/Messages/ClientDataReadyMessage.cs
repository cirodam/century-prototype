﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientDataReadyMessage : Message
{
    public string clientID;
    public ClientData data;

    public ClientDataReadyMessage(string newID, ClientData newData)
    {
        clientID = newID;
        data = newData;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_DATA_READY_MESSAGE;
    }
}
