﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientFactionChangedMessage : Message
{
    public int clientID;
    public Faction faction;

    public ClientFactionChangedMessage(int newClientID, Faction newFaction)
    {
        clientID = newClientID;
        faction = newFaction;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_FACTION_CHANGED_MESSAGE;
    }
}
