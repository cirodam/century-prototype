﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientCreateGroupIntent : Message
{
    public string clientID;
    public string cityID;
    public GroupTemplate template;

    public ClientCreateGroupIntent(string newClientID, string newCityID, GroupTemplate newTemplate)
    {
        clientID = newClientID;
        cityID = newCityID;
        template = newTemplate;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_CREATE_GROUP_INTENT;
    }
}
