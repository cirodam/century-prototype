﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientGameVictoryMessage : Message
{
    public int clientID;

    public ClientGameVictoryMessage(int newClient)
    {
        clientID = newClient;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CLIENT_GAME_VICTORY_MESSAGE;
    }
}
