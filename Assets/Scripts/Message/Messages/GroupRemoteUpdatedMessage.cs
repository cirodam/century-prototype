﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupRemoteUpdatedMessage : Message
{
    public string groupID;

    public GroupRemoteUpdatedMessage(string newID)
    {
        groupID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.GROUP_REMOTE_UPDATED_MESSAGE;
    }
}
