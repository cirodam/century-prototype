﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingSelectedMessage : Message
{
    public string buildingID;

    public BuildingSelectedMessage(string newID)
    {
        buildingID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_SELECTED_MESSAGE;
    }
}
