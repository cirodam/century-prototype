﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitDeployedOnBattlefieldMessage : Message
{
    public string battleID;
    public string groupID;
    public string unitID;
    public MapPos battlePos;

    public UnitDeployedOnBattlefieldMessage(string battleIDArg, string groupIDArg, string unitIDArg, MapPos battlePosArg)
    {
        battleID  = battleIDArg;
        groupID   = groupIDArg;
        unitID    = unitIDArg;
        battlePos = battlePosArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.UNIT_DEPLOYED_ON_BATTLEFIELD_MESSAGE;
    }
}
