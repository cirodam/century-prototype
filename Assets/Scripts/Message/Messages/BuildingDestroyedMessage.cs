﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDestroyedMessage : Message
{
    public string cityID;
    public string buildingID;

    public BuildingDestroyedMessage(string cityIDArg, string buildingIDArg)
    {
        cityID = cityIDArg;
        buildingID = buildingIDArg;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.BUILDING_DESTROYED_MESSAGE;
    }
}
