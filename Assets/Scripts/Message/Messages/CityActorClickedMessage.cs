﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityActorClickedMessage : Message
{
    public string cityID;

    public CityActorClickedMessage(string newID)
    {
        cityID = newID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.CITY_ACTOR_CLICKED_MESSAGE;
    }
}
