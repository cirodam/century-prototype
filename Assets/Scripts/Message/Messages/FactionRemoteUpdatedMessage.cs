﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionRemoteUpdatedMessage : Message
{
    public int clientID;

    public FactionRemoteUpdatedMessage(int newClientID)
    {
        clientID = newClientID;
    }

    public override MessageType GetMessageType()
    {
        return MessageType.FACTION_REMOTE_UPDATED_MESSAGE;
    }
}
