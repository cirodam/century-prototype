﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameClient : MonoBehaviour
{
    public static GameClient instance;

    private string clientID;
    private ClientData clientData;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        MessagePump.instance.Register(this, MessageType.ALL_CLIENTS_PRESENT_MESSAGE, OnAllClientsPresent);
        MessagePump.instance.Register(this, MessageType.SERVER_PRESENT_MESSAGE, OnServerPresent);

        clientID = SessionController.instance.GetLocalClientID();
        clientData = SessionController.instance.GetClientData(clientID);
    }

    private void OnServerPresent(Message msg)
    {
        MessagePump.instance.QueueMsg(new ClientPresentMessage(clientID));
    }

    private void OnAllClientsPresent(Message msg)
    {
        MessagePump.instance.QueueMsg(new ClientDataReadyMessage(clientID, clientData));
        MessagePump.instance.QueueMsg(new ClientReadyMessage(clientID));
    }

    public string GetID()
    {
        return clientID;
    }
}
