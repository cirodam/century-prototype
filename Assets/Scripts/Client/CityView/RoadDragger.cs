﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadDragger : MonoBehaviour
{
    public static RoadDragger instance;

    bool active;
    MapPos currentTile;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        currentTile = new MapPos();

        MessagePump.instance.Register(this, MessageType.BUILDING_DRAG_STARTED_MESSAGE, OnBuildingDragStarted);
    }

    private void OnBuildingDragStarted(Message msg)
    {
        active = false;
    }

    public void Activate()
    {
        active = true;
    }

    public void Deactivate()
    {
        active = false;
    }

    private void Update()
    {
        if(active)
        {
            if(Input.GetMouseButton(0))
            {
                MapPos target = Isomap.instance.GetTileAtWorldPos(Camera.main.ScreenToWorldPoint(Input.mousePosition));
                if(target != currentTile)
                {
                    if (!ViewController.instance.targetCity.IsPositionOccupied(target) && !ViewController.instance.targetCity.IsPositionLocked(target))
                    {
                        GameServer.instance.CreateBuilding(GameClient.instance.GetID(), ViewController.instance.targetCity.GetID(), "Road", target, BuildingFacing.FACING_EAST);
                    }
                    currentTile = target;
                }
            }
        }
    }
}
