﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuildingCostPanelController : MonoBehaviour
{
    public static BuildingCostPanelController instance;

    public GameObject panel;
    public TextMeshProUGUI nameText;

    public GameObject entryPrefab;
    public GameObject gridRoot;
    List<GameObject> list;

    private void Awake()
    {
        if(instance == null)
        {
            list = new List<GameObject>();
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Show(BuildingDefinition bd)
    {
        panel.SetActive(true);

        nameText.text = bd.buildingName;
        foreach(ResourceAmount r in bd.buildingCosts)
        {
            GameObject g = Instantiate(entryPrefab);
            g.transform.SetParent(gridRoot.transform);
            g.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(r.resourceName).iconPath);
            g.GetComponentInChildren<TextMeshProUGUI>().text = r.resourceAmount.ToString();
            list.Add(g);
        }
    }

    public void Hide()
    {
        panel.SetActive(false);
        foreach(GameObject g in list)
        {
            Destroy(g);
        }
        list.Clear();
    }

    private void Update()
    {
        if(panel.activeSelf)
        {
            Vector3 m = Input.mousePosition;
            panel.transform.position = new Vector3(m.x, m.y + 90, 0);
        }
    }
}
