﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoadButtonController : MonoBehaviour
{
    bool active;

    public Sprite buttonSprite;
    public Sprite buttonDownSprite;

    public Image buttonImage;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.BUILDING_DRAG_STARTED_MESSAGE, OnBuildingDragStarted);
    }

    public void OnRoadButton()
    {
        if(active)
        {
            buttonImage.sprite = buttonSprite;
            active = false;
            RoadDragger.instance.Deactivate();
        }
        else
        {
            buttonImage.sprite = buttonDownSprite;
            active = true;
            RoadDragger.instance.Activate();
        }
    }

    private void OnBuildingDragStarted(Message msg)
    {
        if (active)
        {
            buttonImage.sprite = buttonSprite;
            active = false;
        }
    }
}
