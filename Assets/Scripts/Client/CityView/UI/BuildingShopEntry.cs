﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildingShopEntry : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    public Image buildingImage;
    public UnityEvent DragStart;
    public BuildingDefinition building;

    public void OnPointerDown(PointerEventData eventData)
    {
        DragStart.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        BuildingCostPanelController.instance.Show(building);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        BuildingCostPanelController.instance.Hide();
    }
}
