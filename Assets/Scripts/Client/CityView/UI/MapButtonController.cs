﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapButtonController : MonoBehaviour
{
    public void OnMapButtonClicked()
    {
        ViewController.instance.ToMapViewFromCity();
    }
}
