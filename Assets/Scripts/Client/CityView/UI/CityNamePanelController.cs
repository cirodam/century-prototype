﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CityNamePanelController : MonoBehaviour
{
    public TextMeshProUGUI cityNameText;

    public TextMeshProUGUI citizenText;
    public TextMeshProUGUI housingText;
    public TextMeshProUGUI jobText;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
        MessagePump.instance.Register(this, MessageType.REMOTE_VALUE_UPDATED_MESSAGE, OnRemoteValueUpdated);

        cityNameText.text = ViewController.instance.targetCity.GetCityName();
        RefreshUI();
    }

    private void OnNewDay(Message msg)
    {
        RefreshUI();
    }

    private void OnRemoteValueUpdated(Message msg)
    {
        RemoteValueUpdatedMessage cMsg = (RemoteValueUpdatedMessage)msg;

        if(cMsg.id == ViewController.instance.targetCity.GetID())
        {
            RefreshUI();
        }
    }

    private void RefreshUI()
    {
        citizenText.text = ViewController.instance.targetCity.GetCitizenCount().ToString();
        housingText.text = ViewController.instance.targetCity.GetHousingCount().ToString();
        jobText.text = ViewController.instance.targetCity.GetInt("Jobs").ToString();
    }
}
