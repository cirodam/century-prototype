﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDetailPanelController : MonoBehaviour
{
    GameObject panel;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.BUILDING_SELECTED_MESSAGE, OnBuildingSelected);
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
        MessagePump.instance.Register(this, MessageType.BUILDING_REMOTE_UPDATED_MESSAGE, OnBuildingRemoteUpdated);
    }

    private void OnBuildingSelected(Message msg)
    {
        BuildingSelectedMessage cMsg = (BuildingSelectedMessage)msg;

        BuildingRemote building = ViewController.instance.targetCity.GetBuilding(cMsg.buildingID);
        BuildingDefinition def = BuildingDataController.instance.GetDataForBuilding(building.GetBuildingName());

        panel = Instantiate(def.panel);
        panel.transform.SetParent(transform);
        panel.transform.position = Input.mousePosition;
        panel.GetComponent<IBuildingDetailPanelController>().Show(building);
        panel.GetComponent<IBuildingDetailPanelController>().GetBackButton().onClick.AddListener(OnBackButton);
        panel.GetComponent<IBuildingDetailPanelController>().GetDestroyButton().onClick.AddListener(() => { OnDestroyButton(cMsg.buildingID); });
    }

    private void OnNewDay(Message msg)
    {
        if(panel != null)
        {
            panel.GetComponent<IBuildingDetailPanelController>().OnNewDay();
        }
    }

    private void OnBuildingRemoteUpdated(Message msg)
    {
        BuildingRemoteUpdatedMessage cMsg = (BuildingRemoteUpdatedMessage)msg;

        if(panel != null)
        {
            panel.GetComponent<IBuildingDetailPanelController>().OnBuildingRemoteUpdated(cMsg.buildingID);
        }
    }

    private void OnBackButton()
    {
        Destroy(panel);
        panel = null;
        CitySelectionController.instance.DeselectBuilding();
    }

    private void OnDestroyButton(string buildingID)
    {
        Destroy(panel);
        panel = null;
        CitySelectionController.instance.DeselectBuilding();
        GameServer.instance.DestroyBuilding(ViewController.instance.targetCity.GetID(), buildingID);
    }
}
