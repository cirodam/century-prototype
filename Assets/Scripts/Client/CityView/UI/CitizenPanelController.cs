﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CitizenPanelController : MonoBehaviour
{
    public GameObject panel;

    public TextMeshProUGUI foodSecurityText;
    public TextMeshProUGUI GDPText;
    public TextMeshProUGUI prosperityText;

    public TextMeshProUGUI workerCountText;
    public TextMeshProUGUI soldierCountText;

    CityRemote city;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
    }

    private void OnNewDay(Message msg)
    {
        if(panel.activeSelf)
        {
            RefreshUI();
        }
    }

    public void Show()
    {
        city = ViewController.instance.targetCity;

        panel.SetActive(true);
        RefreshUI();
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    private void RefreshUI()
    {
        foodSecurityText.text = city.GetFloat("Food Security").ToString();
        GDPText.text = city.GetFloat("GDP").ToString();
        prosperityText.text = city.GetFloat("Prosperity").ToString();

        workerCountText.text = city.GetInt("Workers").ToString();
        soldierCountText.text = city.GetInt("Soldiers").ToString();
    }
}
