﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingShopPanelController : MonoBehaviour
{
    public GameObject buildingShopPanel;

    public GameObject buildingEntryPrefab;
    public GameObject listRoot;
    List<GameObject> list;

    int startingIndex;
    int length;

    private void Awake()
    {
        list = new List<GameObject>();
    }

    private void Start()
    {
        RefreshList();
    }

    private void RefreshList()
    {
        ClearList();

        if(BuildingDataController.instance.buildings.Count < 5)
        {
            length = BuildingDataController.instance.buildings.Count;
        }
        else
        {
            length = 5;
        }

        for(int i=0; i<length; i++)
        {
            int index = startingIndex + i;
            if (startingIndex + i >= BuildingDataController.instance.buildings.Count)
            {
                index -= BuildingDataController.instance.buildings.Count;
            }
            BuildingDefinition building = BuildingDataController.instance.buildings[index];

            GameObject g = Instantiate(buildingEntryPrefab);
            g.transform.SetParent(listRoot.transform);
            g.GetComponent<BuildingShopEntry>().buildingImage.sprite = Resources.Load<Sprite>(building.uiSpritePath);
            g.GetComponent<BuildingShopEntry>().DragStart.AddListener(() => { OnBuildingShopEntryDragStart(building); });
            g.GetComponent<BuildingShopEntry>().building = building;

            list.Add(g);
        }
    }

    private void OnBuildingShopEntryDragStart(BuildingDefinition b)
    {
        BuildingDragController.instance.StartDrag(b);
    }

    private void ClearList()
    {
        foreach(GameObject g in list)
        {
            Destroy(g);
        }
        list.Clear();
    }

    public void OnLeftButton()
    {
        startingIndex--;
        if (startingIndex < 0)
        {
            startingIndex = BuildingDataController.instance.buildings.Count - 1;
        }
        RefreshList();
    }

    public void OnRightButton()
    {
        startingIndex++;
        if (startingIndex == BuildingDataController.instance.buildings.Count)
        {
            startingIndex = 0;
        }
        RefreshList();
    }
}
