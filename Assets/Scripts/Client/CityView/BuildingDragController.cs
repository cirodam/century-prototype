﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingDragController : MonoBehaviour
{
    public static BuildingDragController instance;

    bool dragging;

    public GameObject dragDummyPrefab;
    public GameObject dragDummyRoot;
    GameObject dragDummy;

    BuildingDefinition targetBuilding;
    BuildingFacing targetFacing;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void StartDrag(BuildingDefinition newTarget)
    {
        targetBuilding = newTarget;
        targetFacing = BuildingFacing.FACING_EAST;

        dragDummy = Instantiate(dragDummyPrefab);
        dragDummy.transform.SetParent(dragDummyRoot.transform);
        SetImage();

        dragging = true;

        MessagePump.instance.QueueMsg(new BuildingDragStartedMessage());
    }

    private void Update()
    {
        if(dragging)
        {
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            dragDummy.transform.position = new Vector3(worldPos.x, worldPos.y, 0);

            if(Input.GetMouseButtonUp(0))
            {
                dragging = false;
                Destroy(dragDummy);
                MessagePump.instance.QueueMsg(new BuildingDragCompletedMessage());
                TestResult();
            }

            if(Input.GetKeyDown(KeyCode.E))
            {
                int current = (int)targetFacing;
                current--;

                if(current < 0)
                {
                    current = 3;
                }
                targetFacing = (BuildingFacing) current;
                SetImage();
            }
            if(Input.GetKeyDown(KeyCode.Q))
            {
                int current = (int)targetFacing;
                current++;

                if(current > 3)
                {
                    current = 0;
                }
                targetFacing = (BuildingFacing)current;
                SetImage();
            }
        }
    }

    private void SetImage()
    {
        dragDummy.GetComponent<SpriteRenderer>().sprite = GetSprite(targetBuilding.spritePath, targetFacing);
    }

    private Sprite GetSprite(string spriteName, BuildingFacing targetFacing)
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>(spriteName);
        Dictionary<string, Sprite> sorted = new Dictionary<string, Sprite>();

        foreach (Sprite s in sprites)
        {
            sorted.Add(s.name, s);
        }

        switch (targetFacing)
        {
            case BuildingFacing.FACING_SOUTH:
                return sorted["south"];
                break;

            case BuildingFacing.FACING_EAST:
                return sorted["east"];
                break;

            case BuildingFacing.FACING_WEST:
                return sorted["west"];
                break;

            case BuildingFacing.FACING_NORTH:
                return sorted["north"];
                break;
        }
        return null;
    }

    private void TestResult()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            GameObject g = hit.collider.gameObject;
            if(g.tag == "Isotile")
            {
                if(!ViewController.instance.targetCity.IsPositionOccupied(g.GetComponent<Isotile>().pos))
                {
                    StockpileRemote s = StockpileStore.instance.remote;
                    bool enough = true;

                    foreach (ResourceAmount r in targetBuilding.buildingCosts)
                    {
                        if (!(s.GetResourceAmount(r.resourceName) >= r.resourceAmount))
                        {
                            enough = false;
                        }
                    }

                    if (enough)
                    {
                        GameServer.instance.CreateBuilding(GameClient.instance.GetID(), ViewController.instance.targetCity.GetID(), targetBuilding.buildingName, g.GetComponent<Isotile>().pos, targetFacing);
                        //MessagePump.instance.QueueMsg(new ClientAddBuildingToCityIntent(GameClient.instance.GetID(), ViewController.instance.targetCity.GetID(), targetBuilding.buildingName, g.GetComponent<Isotile>().pos, targetFacing));
                    }
                }
            }
        }
    }
}
