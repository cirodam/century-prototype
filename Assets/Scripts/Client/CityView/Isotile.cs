﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Isotile : MonoBehaviour
{
    public MapPos pos;

    public GameObject highlight;
    public TextMeshPro text;

    public bool active;
    public IBuilding building;

    private void Awake()
    {
        pos = new MapPos(0, 0);
    }

    public bool isEmpty()
    {
        if(active && building == null)
        {
            return true;
        }
        return false;
    }
}
