﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CityClickListener : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && (!EventSystem.current.IsPointerOverGameObject()))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null)
            {
                switch (hit.collider.gameObject.tag)
                {
                    case "BuildingActor":
                        MessagePump.instance.QueueMsg(new BuildingActorClickedMessage(hit.collider.gameObject.GetComponentInParent<BuildingActor>().buildingID, ViewController.instance.targetCity.GetID()));
                        break;
                }
            }
        }
    }
}
