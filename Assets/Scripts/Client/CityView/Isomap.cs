﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Isomap : MonoBehaviour
{
    public static Isomap instance;

    Isotile[,] tiles;

    public GameObject isotilePrefab;

    public int mapWidth = 40;
    public int mapHeight = 40;

    int xMin = 15;
    int xMax = 23;

    int yMin = 15;
    int yMax = 23;

    public Sprite land;
    public Sprite water;

    float tileWidth;
    float tileHeight;

    Dictionary<string, Sprite> sprites;

    public Sprite IsoSprite;
    public Sprite IsoSprite2;
    public Sprite IsoSprite3;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {

        sprites = new Dictionary<string, Sprite>();
        Sprite[] tiles = Resources.LoadAll<Sprite>("gridhighlights");
        for (int i = 0; i < tiles.Length; i++)
        {
            sprites.Add(tiles[i].name, tiles[i]);
        }

        InitMap(40, 40, ViewController.instance.targetCity);
    }

    private void InitMap(int xWidth, int yWidth, CityRemote targetCity)
    {
        tiles = new Isotile[xWidth, yWidth];

        for (int x = 0; x < xWidth; x++)
        {
            for (int y = 0; y < yWidth; y++)
            {
                GameObject g = Instantiate(isotilePrefab);

                tileWidth = g.GetComponent<SpriteRenderer>().bounds.size.x;
                tileHeight = g.GetComponent<SpriteRenderer>().bounds.size.y;

                int ran = Random.Range(0, 2);
                if(ran == 0)
                {
                    g.GetComponent<SpriteRenderer>().sprite = IsoSprite;
                }
                else if(ran == 1)
                {
                    g.GetComponent<SpriteRenderer>().sprite = IsoSprite2;
                }
                else if(ran == 2)
                {
                    g.GetComponent<SpriteRenderer>().sprite = IsoSprite3;
                }

                Vector2 pos = new Vector2();
                pos.x = (tileWidth / 2) * (x + y);
                pos.y = (tileHeight / 2) * (y - x);

                g.transform.SetParent(transform);
                g.transform.localPosition = pos;
                g.GetComponent<Isotile>().pos = new MapPos(x, y);
                g.GetComponent<Isotile>().text.text = targetCity.GetCityMap()[x,y].ToString();

                tiles[x, y] = g.GetComponent<Isotile>();

                if(!targetCity.GetCityMap()[x,y].IsLocked())
                {
                    tiles[x, y].active = true;
                }

                if (targetCity.GetCityMap()[x, y].terrain == 0)
                {
                    tiles[x, y].GetComponent<SpriteRenderer>().sprite = water; 
                }

                if (x > 1 && x < 39 && y > 1 && y < 39)
                {
                    if(GetTileNeighbors(new MapPos(x,y), targetCity.GetCityMap()) != new Vector4(1,1,1,1) && GetTileNeighbors(new MapPos(x, y), targetCity.GetCityMap()) != new Vector4(0, 0, 0, 0) && !targetCity.GetCityMap()[x,y].IsLocked())
                    {
                        tiles[x, y].highlight.GetComponent<SpriteRenderer>().sprite = sprites[GetTileHighlightName(new MapPos(x, y), targetCity.GetCityMap())];
                        tiles[x, y].highlight.SetActive(true);
                    }
                }
            }
        }
    }

    public bool TileIsEmpty(MapPos pos)
    {
        if(tiles[pos.x, pos.y].active == true && tiles[pos.x, pos.y].building == null)
        {
            return true;
        }
        return false;
    }

    public Isotile GetTile(MapPos pos)
    {
        return tiles[pos.x, pos.y];
    }

    public MapPos GetTileAtWorldPos(Vector2 pos)
    {
        pos.x += -gameObject.transform.position.x;

        float tileWidthHalf = tileWidth / 2;
        float tileheightHalf = tileHeight / 2;

        MapPos m = new MapPos(0, 0);

        pos.x += tileWidthHalf;

        m.x = (int)((((pos.x/tileWidthHalf) - (pos.y/tileheightHalf)) / 2));
        m.y = (int)((((pos.y / tileheightHalf) + (pos.x / tileWidthHalf)) / 2));

        return m;
    }

    public Vector2 GetWorldPosOfTile(MapPos pos)
    {
        return tiles[pos.x, pos.y].transform.position;
    }

    public string GetTileHighlightName(MapPos pos, CityTile[,] map)
    {
        string tilename = "highlight";
        string transition = "";

        Vector4 neighbors = GetTileNeighbors(pos, map);

        if(neighbors == new Vector4(0,1,1,1))
        {
            transition = "N";
        }
        else if(neighbors == new Vector4(0,0,1,1))
        {
            transition = "NE";
        }
        else if(neighbors == new Vector4(1,0,1,1))
        {
            transition = "E";
        }
        else if(neighbors == new Vector4(1,0,0,1))
        {
            transition = "SE";
        }
        else if(neighbors == new Vector4(1,1,0,1))
        {
            transition = "S";
        }
        else if(neighbors == new Vector4(1,1,0,0))
        {
            transition = "SW";
        }
        else if(neighbors == new Vector4(1,1,1,0))
        {
            transition = "W";
        }
        else if(neighbors == new Vector4(0,1,1,0))
        {
            transition = "NW";
        }
        else if (neighbors == new Vector4(0, 1, 0, 0))
        {
            transition = "NW";
        }
        else if (neighbors == new Vector4(1, 0, 0, 0))
        {
            transition = "NW";
        }
        else if (neighbors == new Vector4(0, 0, 1, 0))
        {
            transition = "NW";
        }
        else if (neighbors == new Vector4(0, 0, 0, 1))
        {
            transition = "NW";
        }

        return tilename + "_" + transition;
    }

    public Vector4 GetTileNeighbors(MapPos pos, CityTile[,] map)
    {
        Vector4 ret = new Vector4(1, 1, 1, 1);

        if(map[pos.x, pos.y + 1].IsLocked())
        {
            ret.x = 0;
        }
        if (map[pos.x + 1, pos.y].IsLocked())
        {
            ret.y = 0;
        }
        if (map[pos.x, pos.y - 1].IsLocked())
        {
            ret.z = 0;
        }
        if (map[pos.x - 1, pos.y].IsLocked())
        {
            ret.w = 0;
        }

        return ret;
    }
}
