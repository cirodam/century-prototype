﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitySelectionController : MonoBehaviour
{
    public static CitySelectionController instance;
    string selectedBuildingID;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        selectedBuildingID = "";
        MessagePump.instance.Register(this, MessageType.BUILDING_ACTOR_CLICKED_MESSAGE, OnBuildingActorClicked);
    }

    private void OnBuildingActorClicked(Message msg)
    {
        BuildingActorClickedMessage cMsg = (BuildingActorClickedMessage)msg;

        if(selectedBuildingID == "")
        {
            selectedBuildingID = cMsg.buildingID;
            MessagePump.instance.QueueMsg(new BuildingSelectedMessage(selectedBuildingID));
        }
    }

    public void DeselectBuilding()
    {
        if(selectedBuildingID != "")
        {
            MessagePump.instance.QueueMsg(new BuildingDeselectedMessage());
            selectedBuildingID = "";
        }
    }
}

