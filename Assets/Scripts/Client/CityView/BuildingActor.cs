﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingActor : MonoBehaviour
{
    public string buildingID;
    public MapPos mapPos;

    public GameObject buildingImage;
    public GameObject outlineImage;

    public void SetBuildingSprite(Sprite sprite)
    {
        buildingImage.GetComponent<SpriteRenderer>().sprite = sprite;
    }

    public void ShowOutline()
    {
        buildingImage.SetActive(false);
        outlineImage.SetActive(true);
    }

    public void ShowBuilding()
    {
        buildingImage.SetActive(true);
        outlineImage.SetActive(false);
    }
}
