﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityCameraController : MonoBehaviour
{
    public float maxUnzoom = 20f;
    public float minUnzoom = 2f;

    [HideInInspector] public float size = 6f;
    public int mouseWheelZoomMultiplier = 10;

    private void Awake()
    {
        size = 6f;
        Camera.main.farClipPlane = 100000f;
        Camera.main.orthographicSize = size;
    }

    private void Update()
    {
        size = Camera.main.orthographicSize;
        Camera.main.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * mouseWheelZoomMultiplier;
    }
}
