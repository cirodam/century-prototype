﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CitizenActor : MonoBehaviour
{
    public UnityEvent OnFinishedMoving;
    bool moving;
    Vector2 targetPos;

    public void MoveToPosition(MapPos dest)
    {
        moving = true;
        targetPos = Isomap.instance.GetWorldPosOfTile(dest);
    }

    private void Update()
    {
        if(moving)
        {
            if(Vector2.Distance(transform.position, targetPos) > 0.01f)
            {
                transform.position = Vector2.MoveTowards(transform.position, targetPos, 0.01f);
            }
            else
            {
                transform.position = targetPos;
                moving = false;
                OnFinishedMoving.Invoke();
            }
        }
    }
}
