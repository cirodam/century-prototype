﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitizenActorController : MonoBehaviour
{
    public GameObject citizenPrefab;
    public GameObject citizenRoot;
    List<GameObject> citizenActors;

    List<int> standing;
    List<int> moving;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);

        citizenActors = new List<GameObject>();
        standing = new List<int>();
        moving = new List<int>();

        for(int i=0; i< 0; i++)
        {
            AddCitizenActor();
        }
    }

    private void AddCitizenActor()
    {
        GameObject g = Instantiate(citizenPrefab);
        g.transform.SetParent(citizenRoot.transform);
        //g.transform.position = Isomap.instance.GetWorldPosOfTile(ViewController.instance.targetCity.SampleRandomUnlockedPosition());

        citizenActors.Add(g);
        standing.Add(citizenActors.IndexOf(g));
        g.GetComponent<CitizenActor>().OnFinishedMoving.AddListener(() => { OnCitizenFinishedMoving(citizenActors.IndexOf(g)); });
    }

    private void OnCitizenFinishedMoving(int index)
    {
        moving.Remove(index);
        standing.Add(index);
    }

    private void OnNewDay(Message msg)
    {
        for (int i = 0; i < 3; i++)
        {
            if (standing.Count > 0)
            {
                int target = Random.Range(0, standing.Count - 1);

                CitizenActor ta = citizenActors[target].GetComponent<CitizenActor>();
                //MapPos dest = ViewController.instance.targetCity.SampleRandomUnlockedPosition();
                //ta.MoveToPosition(dest);
                //moving.Add(target);
                //standing.Remove(target);
            }
            else
            {
                break;
            }
        }
    }
}
