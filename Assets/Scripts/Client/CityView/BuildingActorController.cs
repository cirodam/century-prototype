﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingActorController : MonoBehaviour
{
    public GameObject buildingActorRoot;

    Dictionary<string, BuildingActor> buildingActors;
    Dictionary<string, BuildingActor> roadActors;

    Dictionary<string, Sprite> roads;

    private void Awake()
    {
        buildingActors = new Dictionary<string, BuildingActor>();
        roadActors = new Dictionary<string, BuildingActor>();

        roads = new Dictionary<string, Sprite>();
        Sprite[] roadSprites = Resources.LoadAll<Sprite>("Road");
        foreach(Sprite s in roadSprites)
        {
            roads.Add(s.name, s);
        }

        MessagePump.instance.Register(this, MessageType.BUILDING_CREATED_MESSAGE, OnBuildingCreated);
        MessagePump.instance.Register(this, MessageType.BUILDING_DESTROYED_MESSAGE, OnBuildingDestroyed);

        MessagePump.instance.Register(this, MessageType.BUILDING_DRAG_STARTED_MESSAGE, OnBuildingDragStarted);
        MessagePump.instance.Register(this, MessageType.BUILDING_DRAG_COMPLETED_MESSAGE, OnBuildingDragComplete);
    }

    private void Start()
    {
        foreach (BuildingRemote b in CityStore.instance.remotes[ViewController.instance.targetCity.GetID()].GetBuildings().Values)
        {
            AddBuildingActor(b);
        }
    }

    private void OnBuildingCreated(Message msg)
    {
        BuildingCreatedMessage cMsg = (BuildingCreatedMessage)msg;
        if(cMsg.cityID == ViewController.instance.targetCity.GetID())
        {
            AddBuildingActor(cMsg.buildingID, cMsg.buildingName, cMsg.cityPos, cMsg.facing);
        }
    }

    private void OnBuildingDestroyed(Message msg)
    {
        BuildingDestroyedMessage cMsg = (BuildingDestroyedMessage) msg;
        if(cMsg.cityID == ViewController.instance.targetCity.GetID())
        {
            if(buildingActors.ContainsKey(cMsg.buildingID))
            {
                Destroy(buildingActors[cMsg.buildingID].gameObject);
                buildingActors.Remove(cMsg.buildingID);
            }
        }
    }

    private void AddBuildingActor(BuildingRemote b)
    {
        AddBuildingActor(b.GetID(), b.GetBuildingName(), b.GetCityPos(), b.GetFacing());
    }

    private void AddBuildingActor(string buildingID, string buildingName, MapPos pos, BuildingFacing facing)
    {
        BuildingDefinition bd = BuildingDataController.instance.GetDataForBuilding(buildingName);

        GameObject g = Instantiate(bd.actor);
        g.transform.SetParent(buildingActorRoot.transform);
        g.transform.position = Isomap.instance.GetTile(pos).gameObject.transform.position;
        g.GetComponentInChildren<SpriteRenderer>().sortingOrder = (int)(240 - (g.transform.position.y * 100));
        g.GetComponent<BuildingActor>().buildingID = buildingID;
        g.GetComponent<BuildingActor>().mapPos = pos;

        if (buildingName != "Road")
        {
            g.GetComponent<BuildingActor>().SetBuildingSprite(GetSprite(bd.spritePath, facing));
            buildingActors.Add(buildingID, g.GetComponent<BuildingActor>());
        }
        else
        {
            roadActors.Add(buildingID, g.GetComponent<BuildingActor>());
            RefreshRoads();
        }
    }

    public void RefreshRoads()
    {
        foreach(BuildingActor b in roadActors.Values)
        {
            //Debug.Log(GetRoadSprite(b.building.GetPos()));
            b.SetBuildingSprite(roads[GetRoadSprite(b.mapPos)]);
        }
    }

    public string GetRoadSprite(MapPos pos)
    {
        Vector4 neighbors = GetNeighboringRoads(pos);

        if (neighbors == new Vector4(0, 0, 0, 0))
        {
            return "road_none";
        }
        else if (neighbors == new Vector4(1,0,0,0))
        {
            return "road_north";
        }
        else if (neighbors == new Vector4(0, 1, 0, 0))
        {
            return "road_east";
        }
        else if (neighbors == new Vector4(0, 0, 1, 0))
        {
            return "road_south";
        }
        else if (neighbors == new Vector4(0, 0, 0, 1))
        {
            return "road_west";
        }
        else if (neighbors == new Vector4(1, 1, 0, 0))
        {
            return "road_north_east";
        }
        else if (neighbors == new Vector4(0, 1, 1, 0))
        {
            return "road_east_south";
        }
        else if (neighbors == new Vector4(0, 0, 1, 1))
        {
            return "road_south_west";
        }
        else if (neighbors == new Vector4(1, 0, 0, 1))
        {
            return "road_west_north";
        }
        else if (neighbors == new Vector4(1, 0, 1, 0))
        {
            return "road_north_south";
        }
        else if (neighbors == new Vector4(0, 1, 0, 1))
        {
            return "road_east_west";
        }
        else if (neighbors == new Vector4(1, 1, 1, 0))
        {
            return "road_north_east_south";
        }
        else if (neighbors == new Vector4(0, 1, 1, 1))
        {
            return "road_east_south_west";
        }
        else if (neighbors == new Vector4(1, 0, 1, 1))
        {
            return "road_south_west_north";
        }
        else if (neighbors == new Vector4(1, 1, 0, 1))
        {
            return "road_west_north_east";
        }
        else if (neighbors == new Vector4(1, 1, 1, 1))
        {
            return "road_north_east_south_west";
        }

        return "";
    }

    public Vector4 GetNeighboringRoads(MapPos pos)
    {
        Vector4 ret = new Vector4(0, 0, 0, 0);

        if(ViewController.instance.targetCity.GetBuildingAtPosition(new MapPos(pos.x, pos.y+1)) != null)
        {
            if (ViewController.instance.targetCity.GetBuildingAtPosition(new MapPos(pos.x, pos.y + 1)).GetBuildingName() == "Road")
            {
                ret.x = 1;
            }
        }
        if (ViewController.instance.targetCity.GetBuildingAtPosition(new MapPos(pos.x+1, pos.y)) != null)
        {
            if (ViewController.instance.targetCity.GetBuildingAtPosition(new MapPos(pos.x + 1, pos.y)).GetBuildingName() == "Road")
            {
                ret.y = 1;
            }
        }
        if (ViewController.instance.targetCity.GetBuildingAtPosition(new MapPos(pos.x, pos.y-1)) != null)
        {
            if (ViewController.instance.targetCity.GetBuildingAtPosition(new MapPos(pos.x, pos.y - 1)).GetBuildingName() == "Road")
            {
                ret.z = 1;
            }
        }
        if (ViewController.instance.targetCity.GetBuildingAtPosition(new MapPos(pos.x-1, pos.y)) != null)
        {
            if (ViewController.instance.targetCity.GetBuildingAtPosition(new MapPos(pos.x - 1, pos.y)).GetBuildingName() == "Road")
            {
                ret.w = 1;
            }
        }

        return ret;
    }

    private Sprite GetSprite(string spriteName, BuildingFacing targetFacing)
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>(spriteName);
        Dictionary<string, Sprite> sorted = new Dictionary<string, Sprite>();
        
        foreach(Sprite s in sprites)
        {
            sorted.Add(s.name, s);
        }

        switch (targetFacing)
        {
            case BuildingFacing.FACING_SOUTH:
                return sorted["south"];
                break;

            case BuildingFacing.FACING_EAST:
                return sorted["east"];
                break;

            case BuildingFacing.FACING_WEST:
                return sorted["west"];
                break;

            case BuildingFacing.FACING_NORTH:
                return sorted["north"];
                break;
        }
        return null;
    }

    private void OnBuildingDragStarted(Message msg)
    {
        foreach(BuildingActor b in buildingActors.Values)
        {
            b.ShowOutline();
        }
    }

    private void OnBuildingDragComplete(Message msg)
    {
        foreach (BuildingActor b in buildingActors.Values)
        {
            b.ShowBuilding();
        }
    }
}
