﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCache : MonoBehaviour
{
    public static WorldCache instance;

    public Dictionary<string, Natural> naturals;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        naturals = new Dictionary<string, Natural>();

        MessagePump.instance.Register(this, MessageType.WORLD_READY_MESSAGE, OnWorldReady);
    }

    private void OnWorldReady(Message msg)
    {
        WorldReadyMessage cMsg = (WorldReadyMessage) msg;

        foreach(Natural n in cMsg.naturals)
        {
            naturals.Add(n.naturalID, n);
        }
    }

    public Natural GetNaturalAtPosition(MapPos pos)
    {
        foreach(Natural n in naturals.Values)
        {
            if(n.pos == pos)
            {
                return n;
            }
        }

        return null;
    }

    public void GetEntityAtPosition(MapPos pos)
    {

    }
}
