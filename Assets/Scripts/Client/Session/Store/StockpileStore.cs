﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockpileStore : MonoBehaviour
{
    public static StockpileStore instance;
    public StockpileRemote remote;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        MessagePump.instance.Register(this, MessageType.STOCKPILE_CREATED_MESSAGE, OnStockpileCreated);
        MessagePump.instance.Register(this, MessageType.STOCKPILE_RESOURCE_UPDATED_MESSAGE, OnStockpileResourceUpdated);
        MessagePump.instance.Register(this, MessageType.PRE_NEW_DAY_MESSAGE, OnPreNewDay);
    }

    private void OnPreNewDay(Message msg)
    {
        remote.OnPreNewDay();
    }

    private void OnStockpileCreated(Message msg)
    {
        StockpileCreatedMessage cMsg = (StockpileCreatedMessage)msg;

        if(cMsg.ownerID == GameClient.instance.GetID())
        {
            remote = new StockpileRemote();
            MessagePump.instance.QueueMsg(new StockpileRemoteUpdatedMessage());
        }
    }

    private void OnStockpileResourceUpdated(Message msg)
    {
        StockpileResourceUpdatedMessage cMsg = (StockpileResourceUpdatedMessage)msg;

        if(cMsg.ownerID == GameClient.instance.GetID())
        {
            remote.UpdateResourceAmount(cMsg.resourceName, cMsg.resourceAmount);
            MessagePump.instance.QueueMsg(new StockpileRemoteUpdatedMessage());
        }
    }
}
