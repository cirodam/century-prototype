﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampStore : MonoBehaviour
{
    public static CampStore instance;
    public Dictionary<string, CampRemote> remotes;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public CampRemote GetCamp(string id)
    {
        if(remotes.ContainsKey(id))
        {
            return remotes[id];
        }
        return null;
    }

    private void Init()
    {
        remotes = new Dictionary<string, CampRemote>();

        MessagePump.instance.Register(this, MessageType.CAMP_CREATED_MESSAGE, OnCampCreated);
        MessagePump.instance.Register(this, MessageType.CAMP_VALUE_CHANGED_MESSAGE, OnCampValueUpdated);
        MessagePump.instance.Register(this, MessageType.GROUP_GARRISONED_MESSAGE, OnGroupGarrisoned);
        MessagePump.instance.Register(this, MessageType.GROUP_UNGARRISONED_MESSAGE, OnGroupUnGarrisoned);
    }

    private void OnCampCreated(Message msg)
    {
        CampCreatedMessage cMsg = (CampCreatedMessage)msg;

        if(!remotes.ContainsKey(cMsg.campID))
        {
            remotes.Add(cMsg.campID, new CampRemote(cMsg.campID, cMsg.ownerID, cMsg.campName, cMsg.worldPos));
            MessagePump.instance.QueueMsg(new CampRemoteUpdatedMessage(cMsg.campID));
        }
    }

    private void OnGroupGarrisoned(Message msg)
    {
        GroupGarrisonedMessage cMsg = (GroupGarrisonedMessage)msg;

        if(remotes.ContainsKey(cMsg.cityID))
        {
            remotes[cMsg.cityID].AddGarrison(cMsg.groupID);
            MessagePump.instance.QueueMsg(new CampRemoteUpdatedMessage(cMsg.cityID));
        }
    }

    private void OnGroupUnGarrisoned(Message msg)
    {
        GroupUngarrisonedMessage cMsg = (GroupUngarrisonedMessage) msg;

        if(remotes.ContainsKey(cMsg.cityID))
        {
            remotes[cMsg.cityID].RemoveGarrison(cMsg.groupID);
            MessagePump.instance.QueueMsg(new CampRemoteUpdatedMessage(cMsg.cityID));
        }
    }

    private void OnCampValueUpdated(Message msg)
    {
        CampValueChangedMessage cMsg = (CampValueChangedMessage)msg;

        if(cMsg.key == "OwnerID")
        {
            remotes[cMsg.campID].SetOwnerID((string)cMsg.value);
        }
        MessagePump.instance.QueueMsg(new CampRemoteUpdatedMessage(cMsg.campID));
    }
}
