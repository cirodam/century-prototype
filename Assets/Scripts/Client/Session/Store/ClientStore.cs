﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientStore : MonoBehaviour
{
    public static ClientStore instance;

    public Dictionary<string, ClientData> clientData;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        MessagePump.instance.Register(this, MessageType.CLIENT_DATA_READY_MESSAGE, OnClientDataReady);

        clientData = new Dictionary<string, ClientData>();
    }

    private void OnClientDataReady(Message msg)
    {
        ClientDataReadyMessage cMsg = (ClientDataReadyMessage)msg;

        if(!clientData.ContainsKey(cMsg.clientID))
        {
            clientData.Add(cMsg.clientID, cMsg.data);
        }
    }
}
