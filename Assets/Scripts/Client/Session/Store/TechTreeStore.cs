﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TechTreeStore : MonoBehaviour
{
    public static TechTreeStore instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
        }
    }

    public bool IsTechUnlocked(string name)
    {
        return true;
    }
}
