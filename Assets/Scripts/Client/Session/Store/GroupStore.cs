﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupStore : MonoBehaviour
{
    public static GroupStore instance;
    public Dictionary<string, GroupRemote> remotes;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        remotes = new Dictionary<string, GroupRemote>();

        MessagePump.instance.Register(this, MessageType.GROUP_CREATED_MESSAGE, OnGroupCreated);
        MessagePump.instance.Register(this, MessageType.GROUP_DESTROYED_MESSAGE, OnGroupDestroyed);
        MessagePump.instance.Register(this, MessageType.UNIT_CREATED_MESSAGE, OnUnitCreated);
        MessagePump.instance.Register(this, MessageType.UNIT_DESTROYED_MESSAGE, OnUnitDestroyed);

        MessagePump.instance.Register(this, MessageType.SERVER_VALUE_UPDATED_MESSAGE, OnServerValueUpdated);

        MessagePump.instance.Register(this, MessageType.SERVER_INT_VALUE_UPDATED_MESSAGE, OnServerIntValueUpdated);
        MessagePump.instance.Register(this, MessageType.SERVER_MAPPOS_VALUE_UPDATED_MESSAGE, OnServerMapPosValueUpdated);
    }

    private void OnGroupCreated(Message msg)
    {
        GroupCreatedMessage cMsg = (GroupCreatedMessage) msg;

        if(!remotes.ContainsKey(cMsg.groupID))
        {
            remotes.Add(cMsg.groupID, new GroupRemote(cMsg.ownerID, cMsg.groupID, cMsg.title, cMsg.worldPos, cMsg.movementType, cMsg.garrison, cMsg.options));
            MessagePump.instance.QueueMsg(new GroupRemoteUpdatedMessage(cMsg.groupID));
        }
    }

    private void OnGroupDestroyed(Message msg)
    {
        GroupDestroyedMessage cMsg = (GroupDestroyedMessage)msg;

        if(remotes.ContainsKey(cMsg.groupID))
        {
            remotes.Remove(cMsg.groupID);
        }
    }

    private void OnUnitCreated(Message msg)
    {
        UnitCreatedMessage cMsg = (UnitCreatedMessage)msg;

        if(remotes.ContainsKey(cMsg.groupID))
        {
            UnitRemote r = new UnitRemote(cMsg.ownerID, cMsg.groupID, cMsg.unitID, cMsg.title, cMsg.count, cMsg.options);
            remotes[cMsg.groupID].OnUnitCreated(r);
        }
    }

    private void OnUnitDestroyed(Message msg)
    {
        UnitDestroyedMessage cMsg = (UnitDestroyedMessage)msg;

        if(remotes.ContainsKey(cMsg.groupID))
        {
            remotes[cMsg.groupID].OnUnitDestroyed(cMsg.unitID);
        }
    }

    private void OnServerValueUpdated(Message msg)
    {
        ServerValueUpdatedMessage cMsg = (ServerValueUpdatedMessage)msg;

        if(remotes.ContainsKey(cMsg.id))
        {
            remotes[cMsg.id].OnValueUpdated(cMsg.key, cMsg.val);
        }
        else
        {
            foreach(GroupRemote g in remotes.Values)
            {
                UnitRemote unit = g.GetUnit(cMsg.id);
                if(unit != null)
                {
                    unit.OnValueUpdated(cMsg.key, cMsg.val);
                    break;
                }
            }
        }
    }

    private void OnServerMapPosValueUpdated(Message msg)
    {
        ServerMapPosValueUpdatedMessage cMsg = (ServerMapPosValueUpdatedMessage)msg;

        if(remotes.ContainsKey(cMsg.affectedID))
        {
            remotes[cMsg.affectedID].SetValue(cMsg.key, cMsg.value);
        }
    }

    private void OnServerIntValueUpdated(Message msg)
    {
        ServerIntValueUpdatedMessage cMsg = (ServerIntValueUpdatedMessage)msg;

        if (remotes.ContainsKey(cMsg.affectedID))
        {
            remotes[cMsg.affectedID].SetValue(cMsg.key, cMsg.value);
        }
    }

    public GroupRemote GetGroup(string groupID)
    {
        if(remotes.ContainsKey(groupID))
        {
            return remotes[groupID];
        }
        return null;
    }

    public UnitRemote GetUnit(string groupID, string unitID)
    {
        if(remotes.ContainsKey(groupID))
        {
            return remotes[groupID].GetUnit(unitID);
        }
        return null;
    }
}
