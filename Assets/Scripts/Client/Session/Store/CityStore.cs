﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityStore : MonoBehaviour
{
    public static CityStore instance;

    public Dictionary<string, CityRemote> remotes;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        MessagePump.instance.Register(this, MessageType.CITY_CREATED_MESSAGE, OnCityCreated);
        MessagePump.instance.Register(this, MessageType.CITY_DESTROYED_MESSAGE, OnCityDestroyed);
        MessagePump.instance.Register(this, MessageType.BUILDING_CREATED_MESSAGE, OnBuildingCreated);
        MessagePump.instance.Register(this, MessageType.BUILDING_DESTROYED_MESSAGE, OnBuildingDestroyed);
        MessagePump.instance.Register(this, MessageType.SERVER_VALUE_UPDATED_MESSAGE, OnServerValueUpdated);

        remotes = new Dictionary<string, CityRemote>();
    }

    private void OnCityCreated(Message msg)
    {
        CityCreatedMessage cMsg = (CityCreatedMessage)msg;

        if(!remotes.ContainsKey(cMsg.cityID))
        {
            remotes.Add(cMsg.cityID, new CityRemote(cMsg.ownerID, cMsg.cityID, cMsg.cityName, cMsg.worldPos, cMsg.coastal, cMsg.cityMap, cMsg.options));
            MessagePump.instance.QueueMsg(new CityRemoteUpdatedMessage(cMsg.cityID));
        }
    }

    private void OnCityDestroyed(Message msg)
    {
        CityDestroyedMessage cMsg = (CityDestroyedMessage)msg;

        if(remotes.ContainsKey(cMsg.cityID))
        {
            remotes.Remove(cMsg.cityID);
        }
    }

    private void OnBuildingCreated(Message msg)
    {
        BuildingCreatedMessage cMsg = (BuildingCreatedMessage)msg;

        if(remotes.ContainsKey(cMsg.cityID))
        {
            BuildingRemote b = new BuildingRemote(cMsg.cityID, cMsg.buildingID, cMsg.buildingName, cMsg.level, cMsg.upkeep, cMsg.cityPos, cMsg.facing, cMsg.options);
            remotes[cMsg.cityID].OnBuildingCreated(b);
        }
    }

    private void OnBuildingDestroyed(Message msg)
    {
        BuildingDestroyedMessage cMsg = (BuildingDestroyedMessage)msg;

        if(remotes.ContainsKey(cMsg.cityID))
        {
            remotes[cMsg.cityID].OnBuildingDestroyed(cMsg.buildingID);
        }
    }

    private void OnServerValueUpdated(Message msg)
    {
        ServerValueUpdatedMessage cMsg = (ServerValueUpdatedMessage)msg;

        if(remotes.ContainsKey(cMsg.id))
        {
            remotes[cMsg.id].OnValueUpdated(cMsg.key, cMsg.val);
        }
    }

    public CityRemote GetCity(string id)
    {
        if(remotes.ContainsKey(id))
        {
            return remotes[id];
        }
        return null;
    }
}
