﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStore : MonoBehaviour
{
    public static BattleStore instance;
    public string BattleID;

    public Dictionary<string, MapPos> starting;
    public Dictionary<string, string> groups;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        starting = new Dictionary<string, MapPos>();
        groups = new Dictionary<string, string>();

        MessagePump.instance.Register(this, MessageType.UNIT_DEPLOYED_ON_BATTLEFIELD_MESSAGE, OnUnitDeployedOnBattlefield);
        MessagePump.instance.Register(this, MessageType.BATTLE_STATE_CHANGED_MESSAGE, OnBattleStateChanged);
    }

    private void OnUnitDeployedOnBattlefield(Message msg)
    {
        UnitDeployedOnBattlefieldMessage cMsg = (UnitDeployedOnBattlefieldMessage)msg;

        if(!starting.ContainsKey(cMsg.unitID))
        {
            starting.Add(cMsg.unitID, cMsg.battlePos);
            groups.Add(cMsg.unitID, cMsg.groupID);
        }
    }

    private void OnBattleStateChanged(Message msg)
    {
        BattleStateChangedMessage cMsg = (BattleStateChangedMessage)msg;

        if(cMsg.newState == BattleState.RESULTS)
        {
            starting.Clear();
        }
    }
}
