﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Remote 
{
    Dictionary<string, object> options;

    public Remote()
    {
        options = new Dictionary<string, object>();
    }
    public void SetValue(string key, object value)
    {
        if (!options.ContainsKey(key))
        {
            options.Add(key, null);
        }
        options[key] = value;
    }

    public int GetInt(string key)
    {
        if (options.ContainsKey(key))
        {
            return (int)options[key];
        }

        Debug.LogWarning("Trying to retrive a remote option that doesn't exist");
        return -1;
    }

    public float GetFloat(string key)
    {
        if (options.ContainsKey(key))
        {
            return (float)options[key];
        }

        Debug.LogWarning("Trying to retrive a remote option that doesn't exist");
        return -1;
    }

    public bool GetBool(string key)
    {
        if (options.ContainsKey(key))
        {
            return (bool)options[key];
        }

        Debug.LogWarning("Trying to retrive a remote option that doesn't exist");
        return false;
    }

    public string GetString(string key)
    {
        if (options.ContainsKey(key))
        {
            return (string)options[key];
        }

        Debug.LogWarning("Trying to retrive a remote option that doesn't exist");
        return "Absent";
    }

    public MapPos GetMapPos(string key)
    {
        if (options.ContainsKey(key))
        {
            return (MapPos)options[key];
        }

        Debug.LogWarning("Trying to retrive a remote option that doesn't exist");
        return new MapPos();
    }

    public Vector2 GetVector2(string key)
    {
        if(options.ContainsKey(key))
        {
            return (Vector2)options[key];
        }

        Debug.LogWarning("Trying to retrive a remote option that doesn't exist");
        return new Vector2();
    }
}
