﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitRemote : Remote
{
    private string ownerID;
    private string groupID;
    private string unitID;

    private string title;
    private int count;

    public UnitRemote(string ownerIDArg, string groupIDArg, string unitIDArg, string titleArg, int countArg, List<ValueEntry> options)
    {
        ownerID = ownerIDArg;
        groupID = groupIDArg;
        unitID  = unitIDArg;
        title   = titleArg;
        count   = countArg;

        foreach (ValueEntry v in options)
        {
            OnValueUpdated(v.key, v.val);
        }
    }

    public void OnValueUpdated(string key, object val)
    {
        switch(key)
        {
            case "OwnerID":
                ownerID = (string) val;
                break;
            case "GroupID":
                groupID = (string) val;
                break;
            case "Title":
                title = (string) val;
                break;
            case "Count":
                count = (int) val;
                break;
            default:
                SetValue(key, val);
                break;
        }
        MessagePump.instance.QueueMsg(new RemoteValueUpdatedMessage(unitID));
    }

    public string GetID()
    {
        return unitID;
    }

    public string GetGroupID()
    {
        return groupID;
    }

    public string GetOwnerID()
    {
        return ownerID;
    }

    public string GetTitle()
    {
        return title;
    }

    public int GetCount()
    {
        return count;
    }
}
