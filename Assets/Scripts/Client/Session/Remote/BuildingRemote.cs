﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingRemote : Remote
{
    private string cityID;
    private string buildingID;
    private string buildingName;

    private int level;
    private float upkeep;

    private MapPos cityPos;
    private BuildingFacing facing;

    public BuildingRemote(string cityIDArg, string buildingIDArg, string buildingNameArg, int levelArg, float upkeepArg, MapPos cityPosArg, BuildingFacing facingArg, List<ValueEntry> options)
    {

        cityID       = cityIDArg;
        buildingID   = buildingIDArg;
        buildingName = buildingNameArg;
        level        = levelArg;
        upkeep       = upkeepArg;
        cityPos      = cityPosArg;
        facing       = facingArg;

        foreach (ValueEntry v in options)
        {
            OnValueUpdated(v.key, v.val);
        }
    }

    public void OnValueUpdated(string key, object val)
    {
        switch (key)
        {
            case "Level":
                level = (int)val;
                break;
            case "Upkeep":
                upkeep = (float)val;
                break;
            default:
                SetValue(key, val);
                break;
        }
    }

    public string GetCityID()
    {
        return cityID;
    }

    public string GetID()
    {
        return buildingID;
    }

    public string GetBuildingName()
    {
        return buildingName;
    }

    public MapPos GetCityPos()
    {
        return cityPos;
    }

    public BuildingFacing GetFacing()
    {
        return facing;
    }

    public int GetLevel()
    {
        return level;
    }

    public float GetUpkeep()
    {
        return upkeep;
    }
}
