﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityRemote : Remote 
{
    private string cityID;
    private string ownerID;
    private string cityName;
    private MapPos worldPos;

    private Dictionary<string, BuildingRemote> buildings;
    private List<string> garrisons;

    private CityTile[,] cityMap;
    private bool coastal;

    private int citizens;
    private int housing;

    public CityRemote(string ownerIDArg, string cityIDArg, string cityNameArg, MapPos worldPosArg, bool coastalArg, CityTile[,] cityMapArg, List<ValueEntry> options)
    {
        ownerID  = ownerIDArg;
        cityID   = cityIDArg;
        cityName = cityNameArg;
        worldPos = worldPosArg;
        coastal  = coastalArg;
        cityMap  = cityMapArg;
        housing  = 0;

        buildings = new Dictionary<string, BuildingRemote>();
        garrisons = new List<string>();

        foreach (ValueEntry v in options)
        {
            OnValueUpdated(v.key, v.val);
        }
    }

    public void OnValueUpdated(string key, object val)
    {
        switch (key)
        {
            case "OwnerID":
                ownerID = (string)val;
                break;
            case "CityName":
                cityName = (string)val;
                break;
            case "Housing":
                housing = (int)val;
                break;
            case "Citizens":
                citizens = (int)val;
                break;
            case "AddGarrison":
                OnGroupAddedAsGarrison((string)val);
                break;
            case "RemoveGarrison":
                OnGroupRemovedAsGarrison((string)val);
                break;
            default:
                SetValue(key, val);
                break;
        }
        MessagePump.instance.QueueMsg(new RemoteValueUpdatedMessage(cityID));
    }

    public void OnBuildingCreated(BuildingRemote b)
    {
        buildings.Add(b.GetID(), b);
        cityMap[b.GetCityPos().x, b.GetCityPos().y].buildingID = b.GetID();
    }

    public void OnBuildingDestroyed(string buildingID)
    {
        if(buildings.ContainsKey(buildingID))
        {
            MapPos pos = buildings[buildingID].GetCityPos();

            cityMap[pos.x, pos.y].buildingID = "";
            buildings.Remove(buildingID);
        }
    }

    public void OnGroupAddedAsGarrison(string groupID)
    {
        garrisons.Add(groupID);
    }

    public void OnGroupRemovedAsGarrison(string groupID)
    {
        garrisons.Remove(groupID);
    }

    public List<string> GetGarrisons()
    {
        return new List<string>(garrisons);
    }

    public string GetID()
    {
        return cityID;
    }

    public string GetOwnerID()
    {
        return ownerID;
    }

    public string GetCityName()
    {
        return cityName;
    }

    public MapPos GetWorldPos()
    {
        return worldPos;
    }

    public Dictionary<string, BuildingRemote> GetBuildings()
    {
        return buildings;
    }

    public CityTile[,] GetCityMap()
    {
        return cityMap;
    }

    public int GetCitizenCount()
    {
        return citizens;
    }

    public int GetHousingCount()
    {
        return housing;
    }

    public bool IsPositionOccupied(MapPos pos)
    {
        if (cityMap[pos.x, pos.y].IsOccupied())
        {
            return true;
        }
        return false;
    }

    public bool IsPositionLocked(MapPos pos)
    {
        return cityMap[pos.x, pos.y].IsLocked();
    }

    public BuildingRemote GetBuildingAtPosition(MapPos pos)
    {
        if (cityMap[pos.x, pos.y].IsOccupied())
        {
            return buildings[cityMap[pos.x, pos.y].buildingID];
        }
        return null;
    }

    public BuildingRemote GetBuilding(string buildingID)
    {
        if(buildings.ContainsKey(buildingID))
        {
            return buildings[buildingID];
        }
        return null;
    }
}
