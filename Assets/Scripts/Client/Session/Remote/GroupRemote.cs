﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupRemote : Remote
{
    private string ownerID;
    private string groupID;
    private string title;

    private MapPos worldPos;
    private Vector2 vectorPos;

    private MovementType movementType;
    private bool stationed;

    public Dictionary<string, UnitRemote> units;

    public GroupRemote(string ownerIDArg, string groupIDArg, string titleArg, MapPos posArg, MovementType movementArg, bool garrisonArg, List<ValueEntry> options)
    {
        ownerID  = ownerIDArg;
        groupID  = groupIDArg;
        title    = titleArg;
        worldPos = posArg;
        stationed = garrisonArg;
        vectorPos = WorldRenderer.instance.GetPositionOfTile(worldPos);
        units    = new Dictionary<string, UnitRemote>();
        movementType = movementArg;

        foreach (ValueEntry v in options)
        {
            OnValueUpdated(v.key, v.val);
        }
    }

    public void OnValueUpdated(string key, object val)
    {
        switch (key)
        {
            case "OwnerID":
                ownerID = (string)val;
                break;
            case "Title":
                title = (string)val;
                break;
            case "WorldPos":
                worldPos = (MapPos)val;
                break;
            case "VectorPos":
                vectorPos = (Vector2)val;
                break;
            case "Garrison":
                stationed = (bool)val;
                break;
            default:
                SetValue(key, val);
                break;
        }

        MessagePump.instance.QueueMsg(new RemoteValueUpdatedMessage(groupID));
    }

    public void OnUnitCreated(UnitRemote c)
    {
        units.Add(c.GetID(), c);
    }

    public void OnUnitDestroyed(string unitID)
    {
        if(units.ContainsKey(unitID))
        {
            units.Remove(unitID);
        }
    }

    public string GetID()
    {
        return groupID;
    }

    public string GetOwnerID()
    {
        return ownerID;
    }

    public string GetTitle()
    {
        return title;
    }

    public MapPos GetWorldPos()
    {
        return worldPos;
    }

    public void SetVectorPos(Vector2 val)
    {
        vectorPos = val;
    }

    public Vector2 GetVectorPos()
    {
        return vectorPos;
    }

    public bool IsStationed()
    {
        return stationed;
    }

    public MovementType GetMovementType()
    {
        return movementType;
    }

    public UnitRemote GetUnit(string id)
    {
        if(units.ContainsKey(id))
        {
            return units[id];
        }
        return null;
    }
}
