﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampRemote
{
    private string campID;
    private string ownerID;
    private string campName;
    private MapPos mapPos;

    private List<string> garrisons;

    public CampRemote(string newID, string newOwner, string newCampName, MapPos newPos)
    {
        campID = newID;
        ownerID = newOwner;
        mapPos = newPos;
        campName = newCampName;
        garrisons = new List<string>();
    }

    public void SetOwnerID(string idArg)
    {
        ownerID = idArg;
    }

    public string GetID()
    {
        return campID;
    }

    public string GetOwnerID()
    {
        return ownerID;
    }

    public string GetCampName()
    {
        return campName;
    }

    public MapPos GetMapPos()
    {
        return mapPos;
    }

    public void AddGarrison(string target)
    {
        garrisons.Add(target);
    }

    public void RemoveGarrison(string target)
    {
        garrisons.Remove(target);
    }

    public List<string> GetGarrisons()
    {
        return garrisons;
    }
}
