﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ViewController : MonoBehaviour
{
    public static ViewController instance;
    public int playerFactionID;

    public CityRemote targetCity;

    public string battleID;
    public string playerGroupID;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
        }
    }

    public void ToCityView(CityRemote newTarget)
    {
        targetCity = newTarget;

        SceneManager.UnloadSceneAsync("MapView");
        SceneManager.LoadScene("CityView", LoadSceneMode.Additive);
    }

    public void ToMapViewFromCity()
    {
        SceneManager.UnloadSceneAsync("CityView");
        SceneManager.LoadScene("MapView", LoadSceneMode.Additive);
    }

    public void ToBattleView(string battleIDArg, string playerGroupIDArg)
    {
        battleID = battleIDArg;
        playerGroupID = playerGroupIDArg;


        SceneManager.UnloadSceneAsync("MapView");
        SceneManager.LoadScene("BattleView", LoadSceneMode.Additive);
    }

    public void ToMapViewFromBattle()
    {
        SceneManager.UnloadSceneAsync("BattleView");
        SceneManager.LoadScene("MapView", LoadSceneMode.Additive);
    }
}
