﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleTile : MonoBehaviour
{
    public bool deploymentTile;
    public GameObject highlight;

    public int groupID;
    public MapPos pos;

    public string unitID;

    private void Awake()
    {
        unitID = "";
    }

    public bool IsOccupied()
    {
        if(unitID == "")
        {
            return false;
        }
        return true;
    }
}
