﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleActorController : MonoBehaviour
{
    public static BattleActorController instance;

    public GameObject unitPrefab;
    public GameObject unitListRoot;
    Dictionary<string, GameObject> unitActors;

    string battleID;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        unitActors = new Dictionary<string, GameObject>();
        MessagePump.instance.Register(this, MessageType.UNIT_DEPLOYED_ON_BATTLEFIELD_MESSAGE, OnUnitDeployedOnBattlefield);
        MessagePump.instance.Register(this, MessageType.BATTLE_SYNC_MESSAGE, OnBattleSync);

        battleID = ViewController.instance.battleID;

        foreach(string s in BattleStore.instance.starting.Keys)
        {
            AddUnitActor(BattleStore.instance.groups[s], s, BattleStore.instance.starting[s]);
        }
    }

    private void OnUnitDeployedOnBattlefield(Message msg)
    {
        UnitDeployedOnBattlefieldMessage cMsg = (UnitDeployedOnBattlefieldMessage)msg;
        AddUnitActor(cMsg.groupID, cMsg.unitID, cMsg.battlePos);
    }

    private void OnBattleSync(Message msg)
    {
        BattleSyncMessage cMsg = (BattleSyncMessage)msg;
        if(cMsg.battleID == battleID)
        {
            foreach(UnitSync s in cMsg.syncs)
            {
                unitActors[s.unitID].transform.position = s.vectorPos;
            }
        }
    }

    public void AddUnitActor(string groupID, string unitID, MapPos battlePos)
    {
        GameObject g = Instantiate(unitPrefab);
        g.transform.SetParent(unitListRoot.transform);
        g.transform.position = BattleMap.instance.GetPositionOfTile(battlePos);
        g.GetComponent<SpriteRenderer>().sortingOrder = (int) -(BattleMap.instance.GetPositionOfTile(battlePos).y*100);
        g.GetComponent<UnitActor>().remote = GroupStore.instance.GetUnit(groupID, unitID);
       
        unitActors.Add(unitID, g);
        MessagePump.instance.QueueMsg(new UnitActorCreatedMessage(g.GetComponent<UnitActor>()));
    }

    public int GetRetreatDirection(int groupID)
    {
        //if(ViewController.instance.attackingGroup.GetGroupID() == groupID)
        //{
        //    return -1;
        //}
        //else
        //{
        //    return 1;
        //} 
        return 0;
    }
}
