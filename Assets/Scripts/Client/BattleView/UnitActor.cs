﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UnitActor : MonoBehaviour
{
    public TextMeshPro nameText;
    public UnitRemote remote;

    private void Update()
    {
        if(remote != null)
        {
            nameText.text = remote.GetFloat("Health").ToString();
        }
    }
}
