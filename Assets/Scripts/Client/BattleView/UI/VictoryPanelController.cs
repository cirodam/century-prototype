﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryPanelController : MonoBehaviour
{
    public GameObject panel;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.BATTLE_STATE_CHANGED_MESSAGE, OnBattleStateChanged);
    }
    
    private void OnBattleStateChanged(Message msg)
    {
        BattleStateChangedMessage cMsg = (BattleStateChangedMessage)msg;

        if(cMsg.newState == BattleState.RESULTS)
        {
            Show();
        }
    }

    public void Show()
    {
        panel.SetActive(true);
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    public void OnReturnButton()
    {
        ViewController.instance.ToMapViewFromBattle();
        TimeController.instance.UnpauseSimulation();
    }
}
