﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeploymentPanelController : MonoBehaviour
{
    public GameObject panel;

    public GameObject entryPrefab;
    public GameObject listRoot;
    Dictionary<string, GameObject> entryList;

    public string battleID;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.UNIT_DEPLOYED_ON_BATTLEFIELD_MESSAGE, OnUnitDeployedOnBattlefield);

        entryList = new Dictionary<string, GameObject>();
        Show();
    }

    public void Show()
    {
        panel.SetActive(true);
        RefreshList();
        battleID = ViewController.instance.battleID;
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    private void OnUnitDeployedOnBattlefield(Message msg)
    {
        UnitDeployedOnBattlefieldMessage cMsg = (UnitDeployedOnBattlefieldMessage)msg;

        if(entryList.ContainsKey(cMsg.unitID))
        {
            Destroy(entryList[cMsg.unitID]);
            entryList.Remove(cMsg.unitID);
        }
    }

    private void RefreshList()
    {
        ClearList();

        GroupRemote g = GroupStore.instance.remotes[ViewController.instance.playerGroupID];
        foreach(UnitRemote r in g.units.Values)
        {
            GameObject o = Instantiate(entryPrefab);
            o.transform.SetParent(listRoot.transform);

            o.GetComponent<DeploymentListEntry>().numberText.text = r.GetCount().ToString();
            o.GetComponent<DeploymentListEntry>().nameText.text = r.GetTitle();
            o.GetComponent<DeploymentListEntry>().OnEntryMouseDown.AddListener(() => { OnEntryMouseDown(r); });
            
            entryList.Add(r.GetID(), o);
        }
    }

    public void OnEntryMouseDown(UnitRemote unit)
    {
        BattleDragController.instance.StartDrag(ViewController.instance.playerGroupID, unit);
    }

    private void ClearList()
    {
        foreach(GameObject g in entryList.Values)
        {
            Destroy(g);
        }
        entryList.Clear();
    }

    public void OnStartButton()
    {
        Hide();
        GameServer.instance.StartBattle(GameClient.instance.GetID(), battleID);
    }
}
