﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DeploymentListEntry : MonoBehaviour, IPointerDownHandler
{
    public TextMeshProUGUI numberText;
    public TextMeshProUGUI nameText;

    public UnityEvent OnEntryMouseDown;

    public void OnPointerDown(PointerEventData eventData)
    {
        OnEntryMouseDown.Invoke();
    }
}
