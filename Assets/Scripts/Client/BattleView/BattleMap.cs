﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleMap : MonoBehaviour
{
    public static BattleMap instance;

    public GameObject tilePrefab;
    public GameObject tileRoot;

    BattleTile[,] map;

    public int mapWidth = 60;
    public int mapHeight = 30;

    string battleID;

    public float tileWidth;
    public float tileHeight;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        MessagePump.instance.Register(this, MessageType.DEPLOYMENT_AREA_DEFINED_MESSAGE, OnDeploymentAreaDefined);
        MessagePump.instance.Register(this, MessageType.BATTLE_STARTING_MESSAGE, OnBattleStarting);
        MessagePump.instance.Register(this, MessageType.UNIT_DEPLOYED_ON_BATTLEFIELD_MESSAGE, OnUnitDeployedOnBattleField);

        map = new BattleTile[60, 30];
        for (int i = 0; i < 60; i++)
        {
            for (int e = 0; e < 30; e++)
            {
                GameObject g = Instantiate(tilePrefab);
                g.transform.SetParent(tileRoot.transform);

                tileWidth = g.GetComponent<SpriteRenderer>().bounds.size.x;
                tileHeight = g.GetComponent<SpriteRenderer>().bounds.size.y;

                g.transform.localPosition = new Vector2(i * tileWidth, e * tileHeight);
                g.GetComponent<BattleTile>().pos = new MapPos(i, e);

                map[i, e] = g.GetComponent<BattleTile>();
            }
        }
    }

    public Vector2 GetPositionOfTile(MapPos battlePos)
    {
        return new Vector2((battlePos.x * tileWidth) + tileRoot.transform.position.x, (battlePos.y * tileHeight) + tileRoot.transform.position.y);
    }

    private void OnUnitDeployedOnBattleField(Message msg)
    {
        UnitDeployedOnBattlefieldMessage cMsg = (UnitDeployedOnBattlefieldMessage)msg;

        Debug.Log(cMsg.battlePos);

        map[cMsg.battlePos.x, cMsg.battlePos.y].unitID = cMsg.unitID;
    }

    private void OnDeploymentAreaDefined(Message msg)
    {
        DeploymentAreaDefinedMessage cMsg = (DeploymentAreaDefinedMessage)msg;

        //if(cMsg.groupID == ViewController.instance.playerGroup.GetGroupID())
        //{
        //    foreach(MapPos m in cMsg.deploymentArea)
        //    {
        //        map[m.x, m.y].highlight.SetActive(true);
        //        map[m.x, m.y].deploymentTile = true;
        //        map[m.x, m.y].groupID = cMsg.groupID;
        //    }
        //}
    }

    private void OnBattleStarting(Message msg)
    {
        for (int i = 0; i < 60; i++)
        {
            for (int e = 0; e < 30; e++)
            {
                map[i, e].highlight.SetActive(false);
            }
        }
    }

    public Vector2 GetTileWorldPos(MapPos pos)
    {
        return map[pos.x, pos.y].transform.position;
    }
}
