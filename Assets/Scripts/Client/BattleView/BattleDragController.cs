﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleDragController : MonoBehaviour
{
    public static BattleDragController instance;

    public GameObject deploymentDummyPrefab;
    public GameObject deploymentDummyRoot;
    GameObject deploymentDummy;
    bool dragging;

    UnitRemote targetUnit;
    string targetGroup;

    string battleID;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
        }
    }

    public void StartDrag(string groupID, UnitRemote unit)
    {
        deploymentDummy = Instantiate(deploymentDummyPrefab);
        deploymentDummy.transform.SetParent(deploymentDummyRoot.transform);
        dragging = true;
        targetUnit = unit;
        targetGroup = groupID;

        battleID = ViewController.instance.battleID;

        MessagePump.instance.QueueMsg(new UnitDragStartedMessage());
    }

    private void Update()
    {
        if(dragging)
        {
            deploymentDummy.transform.position = Input.mousePosition;
            if(Input.GetMouseButtonUp(0))
            {
                MessagePump.instance.QueueMsg(new UnitDragCompletedMessage());
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
                if (hit.collider != null)
                {
                    if (hit.collider.gameObject.tag == "BattleTile")
                    {
                        BattleTile b = hit.collider.gameObject.GetComponent<BattleTile>();
                        if(!b.IsOccupied())
                        {
                            GameServer.instance.DeployUnitOnBattleField(GameClient.instance.GetID(), battleID, targetGroup, targetUnit.GetID(), b.pos);
                        }
                    }
                }

                dragging = false;
                Destroy(deploymentDummy);
                deploymentDummy = null;
            }
        }
    }
}
