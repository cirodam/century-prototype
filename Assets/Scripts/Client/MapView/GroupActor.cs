﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GroupActor : MonoBehaviour, IMapActor
{
    public string groupID;
    public GameObject highlight;

    public TextMeshPro nameText;
    GroupRemote remote;

    public string GetID()
    {
        return groupID;
    }

    public void SetHighlight(bool val)
    {
        highlight.SetActive(val);
    }

    private void Update()
    {
        if(remote == null)
        {
            remote = GroupStore.instance.GetGroup(groupID);
        }
        else
        {
            transform.position = remote.GetVectorPos();
        }
    }
}
