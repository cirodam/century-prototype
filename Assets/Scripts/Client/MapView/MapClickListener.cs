﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapClickListener : MonoBehaviour
{
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && (!EventSystem.current.IsPointerOverGameObject()))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null)
            {
                switch (hit.collider.gameObject.tag)
                {
                    case "CityActor":
                        MessagePump.instance.QueueMsg(new MapActorClickedMessage(hit.collider.GetComponent<IMapActor>().GetID()));
                        break;
                    case "GroupActor":
                        MessagePump.instance.QueueMsg(new MapActorClickedMessage(hit.collider.GetComponent<IMapActor>().GetID()));
                        break;
                    case "MapNodeActor":
                        MessagePump.instance.QueueMsg(new MapActorClickedMessage(hit.collider.GetComponent<IMapActor>().GetID()));
                        break;
                    case "NaturalActor":
                        MessagePump.instance.QueueMsg(new MapActorClickedMessage(hit.collider.GetComponent<IMapActor>().GetID()));
                        break;
                }
            }
            else
            {
                MessagePump.instance.QueueMsg(new MapTerrainClickedMessage());
            }
        }
    }
}
