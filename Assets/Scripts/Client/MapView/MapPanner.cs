﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPanner : MonoBehaviour
{
    public static MapPanner instance;
    public float mouseSensitivity = 1.0f;
    private Vector3 lastPosition;

    public float maxUnzoom = 20f;
    public float minUnzoom = 2f;
    public int mouseWheelZoomMultiplier = 10;

    [HideInInspector] public float size = 2f;

    private void Awake()
    {
        if (instance == null)
        {

            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        size = 2f;
        Camera.main.farClipPlane = 100000f;
        Camera.main.orthographicSize = size;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            lastPosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(1))
        {
            Vector3 delta = Input.mousePosition - lastPosition;
            Camera.main.transform.Translate(-(delta.x * mouseSensitivity), -(delta.y * mouseSensitivity), 0);
            lastPosition = Input.mousePosition;
        }

        size = Camera.main.orthographicSize;
        Camera.main.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * mouseWheelZoomMultiplier;

        if (Camera.main.orthographicSize < minUnzoom)
        {
            Camera.main.orthographicSize = minUnzoom;
        }
        // don't allow zoom above maxUnzoom
        if (Camera.main.orthographicSize > maxUnzoom)
        {
            Camera.main.orthographicSize = maxUnzoom;
        }

        // if camera size changed, update the cell number per viewport
        if (Camera.main.orthographicSize != size)
        {
            WorldRenderer.instance.UpdateCellNb();
        }
    }

    public void PanToObject(GameObject g)
    {
        StartCoroutine(Pan(g));
    }

    private IEnumerator Pan(GameObject g)
    {
        Vector2 cam = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
        Vector2 obj = new Vector2(g.transform.position.x, g.transform.position.y);

        while (Vector2.Distance(cam, obj) >= 0.1f)
        {
            Vector3 dest = new Vector3(obj.x, obj.y, Camera.main.transform.position.z);
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, dest, 0.09f);
            cam = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
            obj = new Vector2(g.transform.position.x, g.transform.position.y);
            yield return null;
        }
    }
}
