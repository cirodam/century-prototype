﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CityActor : MonoBehaviour, IMapActor
{
    public TextMeshProUGUI nameText;
    public GameObject highlight;

    public string cityID;

    public string GetID()
    {
        return cityID;
    }

    public void SetHighlight(bool val)
    {
        highlight.SetActive(val);
    }
}
