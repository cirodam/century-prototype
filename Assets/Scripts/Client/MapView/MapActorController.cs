﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MapActorController : MonoBehaviour
{
    public static MapActorController instance;

    public GameObject cityActorPrefab;
    public GameObject campActorPrefab;

    public GameObject armyActorPrefab;
    public GameObject fleetActorPrefab;
    public GameObject airgroupActorPrefab;

    public GameObject actorRoot;
    public Dictionary<string, GameObject> mapActors;

    public GameObject naturalActorPrefab;

    public List<GameObject> naturalActors;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        mapActors = new Dictionary<string, GameObject>();
        naturalActors = new List<GameObject>();

        MessagePump.instance.Register(this, MessageType.CITY_REMOTE_UPDATED_MESSAGE, OnCityRemoteUpdated);
        MessagePump.instance.Register(this, MessageType.CAMP_REMOTE_UPDATED_MESSAGE, OnCampRemoteUpdated);
        MessagePump.instance.Register(this, MessageType.GROUP_REMOTE_UPDATED_MESSAGE, OnGroupRemoteUpdated);

        MessagePump.instance.Register(this, MessageType.REMOTE_VALUE_UPDATED_MESSAGE, OnRemoteValueUpdated);
        MessagePump.instance.Register(this, MessageType.GAME_STARTING_MESSAGE, OnGameStarting);
    }

    private void Start()
    {
        RefreshAllActors();
    }

    private void RefreshAllActors()
    {
        foreach (string s in CityStore.instance.remotes.Keys)
        {
            RefreshCityActor(s);
        }

        foreach (string s in GroupStore.instance.remotes.Keys)
        {
            if (!GroupStore.instance.remotes[s].GetBool("Garrison"))
            {
                RefreshGroupActor(s);
            }
        }

        foreach (string s in CampStore.instance.remotes.Keys)
        {
            RefreshCampActor(s);
        }

        foreach (Natural n in WorldCache.instance.naturals.Values)
        {
            AddNaturalActor(n);
        }
    }

    private void OnGameStarting(Message msg)
    {
        foreach (Natural n in WorldCache.instance.naturals.Values)
        {
            AddNaturalActor(n);
        }
    }

    private void AddNaturalActor(Natural n)
    {
        NaturalDefinition nd = WorldDataController.instance.GetDataForNatural(n.name);
        GameObject g = Instantiate(naturalActorPrefab);
        g.transform.SetParent(actorRoot.transform);
        g.transform.position = WorldRenderer.instance.GetPositionOfTile(n.pos);
        g.GetComponent<NaturalActor>().naturalID = n.naturalID;
        g.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(nd.spritePath);
        naturalActors.Add(g);
        MessagePump.instance.QueueMsg(new MapActorCreatedMessage(n.naturalID, g.GetComponent<IMapActor>()));
    }

    private void OnRemoteValueUpdated(Message msg)
    {
        RemoteValueUpdatedMessage cMsg = (RemoteValueUpdatedMessage)msg;

        if(GroupStore.instance.remotes.ContainsKey(cMsg.id))
        {
            RefreshGroupActor(cMsg.id);
        }
    }

    private void OnCityRemoteUpdated(Message msg)
    {
        CityRemoteUpdatedMessage cMsg = (CityRemoteUpdatedMessage) msg;
        RefreshCityActor(cMsg.cityID);
    }

    private void OnGroupRemoteUpdated(Message msg)
    {
        GroupRemoteUpdatedMessage cMsg = (GroupRemoteUpdatedMessage)msg;
        RefreshGroupActor(cMsg.groupID);
    }

    private void OnCampRemoteUpdated(Message msg)
    {
        CampRemoteUpdatedMessage cMsg = (CampRemoteUpdatedMessage)msg;
        RefreshCampActor(cMsg.campID);
    }

    private void RefreshCityActor(string cityID)
    {
        CityRemote c = CityStore.instance.remotes[cityID];

        if (!mapActors.ContainsKey(cityID))
        {
            GameObject g = Instantiate(cityActorPrefab);
            g.transform.SetParent(actorRoot.transform);
            g.transform.position = WorldRenderer.instance.GetPositionOfTile(c.GetWorldPos());
            mapActors.Add(cityID, g);
            MessagePump.instance.QueueMsg(new MapActorCreatedMessage(cityID, g.GetComponent<IMapActor>()));
        }

        mapActors[cityID].GetComponent<CityActor>().cityID = cityID;
        mapActors[cityID].GetComponent<CityActor>().nameText.text = c.GetCityName();
        mapActors[cityID].GetComponent<CityActor>().nameText.color = ClientStore.instance.clientData[c.GetOwnerID()].clientFactionColor;
    }

    private void RefreshGroupActor(string groupID)
    {
        GroupRemote g = GroupStore.instance.remotes[groupID];

        if (!mapActors.ContainsKey(groupID))
        {
            GameObject o = null;
            switch(g.GetMovementType())
            {
                case MovementType.MOVEMENT_AIR:
                    o = Instantiate(airgroupActorPrefab);
                    break;
                case MovementType.MOVEMENT_LAND:
                    o = Instantiate(armyActorPrefab);
                    break;
                case MovementType.MOVEMENT_SEA:
                    o = Instantiate(fleetActorPrefab);
                    break;
            }
            o.transform.SetParent(actorRoot.transform);
            o.transform.position = g.GetVectorPos();
            mapActors.Add(groupID, o);
            MessagePump.instance.QueueMsg(new MapActorCreatedMessage(groupID, o.GetComponent<IMapActor>()));
        }

        mapActors[groupID].SetActive(!g.IsStationed());

        if (mapActors.ContainsKey(groupID))
        {
            mapActors[groupID].transform.position = g.GetVectorPos();
            mapActors[groupID].GetComponent<GroupActor>().groupID = groupID;
            mapActors[groupID].GetComponent<GroupActor>().nameText.text = g.GetFloat("Strength").ToString();
            mapActors[groupID].GetComponent<GroupActor>().nameText.color = ClientStore.instance.clientData[g.GetOwnerID()].clientFactionColor;
        }
    }

    private void RefreshCampActor(string campID)
    {
        CampRemote c = CampStore.instance.remotes[campID];
        CampDefinition cd = CampDataController.instance.GetDataForCamp(c.GetCampName());

        if (!mapActors.ContainsKey(campID))
        {
            GameObject o = Instantiate(campActorPrefab);
            o.transform.SetParent(actorRoot.transform);
            o.transform.position = WorldRenderer.instance.GetPositionOfTile(c.GetMapPos());
            mapActors.Add(campID, o);
            MessagePump.instance.QueueMsg(new MapActorCreatedMessage(campID, o.GetComponent<IMapActor>()));
        }

        mapActors[campID].GetComponent<CampActor>().campID = campID;
        mapActors[campID].GetComponent<CampActor>().nameText.text = c.GetCampName();
        mapActors[campID].GetComponent<CampActor>().image.sprite = Resources.Load<Sprite>(cd.spritePath);
        if (c.GetOwnerID() != "")
        {
            mapActors[campID].GetComponent<CampActor>().nameText.color = ClientStore.instance.clientData[c.GetOwnerID()].clientFactionColor;
        }
    }
}

