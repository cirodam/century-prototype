﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldRenderer : MonoBehaviour
{
    public static WorldRenderer instance;

    public float ppu = 28f;

    private Vector3 oldCamPosition;
    private int cellNbX;     // number of horizontal cells in the current viewport 
    private int cellNbY;     // number of vertical cells in the current viewport
    private int oldCellNbX;  // number of horizontal cells in the previous viewport (in case of zoom/unzoom) 
    private int oldCellNbY;  // number of vertical cells in the previous viewport (in case of zoom/unzoom)
    private int halfCellX;   // number of horizontal cells divided by 2
    private int halfCellY;   // number of vertical cells divided by 2

    int zLevel = 0;
    private string[,] tilenames;
    private int[,] pool;

    public bool preComputeTileName = true;
    private bool isCalculating;

    private List<int> freeIndexes;
    private List<GameObject> objects;
    public GameObject baseTile;
    Dictionary<string, Sprite> sprites;
    bool initialized;
    public string tilesetName;

    Dictionary<int, TileIndexData> tileIndexes;

    List<Natural> naturals;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(instance);
        }
    }

    private void Init()
    {
        objects = new List<GameObject>();
        freeIndexes = new List<int>();
        sprites = new Dictionary<string, Sprite>();
        Sprite[] tiles = Resources.LoadAll<Sprite>(tilesetName);    
        for (int i = 0; i < tiles.Length; i++)                       
        {
            sprites.Add(tiles[i].name, tiles[i]);
        }
        tileIndexes = new Dictionary<int, TileIndexData>();
        tileIndexes.Add(0, new TileIndexData(0, "water", 0, 1));
        tileIndexes.Add(1, new TileIndexData(1, "grass", 1, 3));
        tileIndexes.Add(2, new TileIndexData(2, "forest", 2, 3));
        tileIndexes.Add(3, new TileIndexData(3, "beach", 2, 1));
        tileIndexes.Add(4, new TileIndexData(4, "dark", 1, 1));
        tileIndexes.Add(5, new TileIndexData(5, "shallow", 1, 1));

        MessagePump.instance.Register(this, MessageType.WORLD_READY_MESSAGE, OnWorldReady);

        if(WorldController.instance.isReady)
        {
            PrepareToDraw();
        }
    }

    private void OnWorldReady(Message msg)
    {
        WorldReadyMessage cMsg = (WorldReadyMessage) msg;

        naturals = cMsg.naturals;

        PrepareToDraw();
    }

    private void PrepareToDraw()
    {
        tilenames = new string[WorldController.instance.worldSizeX, WorldController.instance.worldSizeY];
        pool = new int[WorldController.instance.worldSizeX, WorldController.instance.worldSizeY];
        ResetPool();


        if (preComputeTileName == true)
        {
            ComputeAllTileNames();
        }

        UpdateCellNb();
        Draw(true);
        initialized = true;
    }

    public void ComputeAllTileNames()
    {
        for (int i = 0; i < WorldController.instance.worldSizeX; i++)   // populate the tile name array with all the tile names
        {
            for (int j = 0; j < WorldController.instance.worldSizeY; j++)
            {
                for(int zLevel = 0; zLevel < WorldController.instance.zLevels; zLevel++)
                {
                    tilenames[i, j] = GetTileName(i, j);
                }
            }
        }
    }

    public string GetTileName(int i, int j)
    {
        if(!tileIndexes.ContainsKey(WorldController.instance.grid[i, j, zLevel]))
        {
            return "";
        }

        string tileName = "water_a";
        string variant = "";
        string direction = "";
        string type = "";
        string to = "";

        if (i > 0 && i < WorldController.instance.worldSizeX - 1 && j > 0 && j < WorldController.instance.worldSizeY - 1)
        {
            tileName = tileIndexes[WorldController.instance.grid[i, j, zLevel]].tileName;
            Vector4 nb = GetNeighborComparisonData(i, j, zLevel);

            if (nb == (new Vector4(0, 1, 1, 1)))
            {
                if(tileIndexes.ContainsKey(WorldController.instance.grid[i, j + 1, zLevel]))
                {
                    to = "_" + tileIndexes[WorldController.instance.grid[i, j + 1, zLevel]].tileName;
                }
                type = "_outer";
                direction = "_N";
            }
            else if (nb == (new Vector4(0, 0, 1, 1)))
            {
                if (tileIndexes.ContainsKey(WorldController.instance.grid[i, j + 1, zLevel]))
                {
                    to = "_" + tileIndexes[WorldController.instance.grid[i, j + 1, zLevel]].tileName;
                }
                type = "_outer";
                direction = "_NE";
            }
            else if (nb == (new Vector4(0, 1, 1, 0)))
            {
                if (tileIndexes.ContainsKey(WorldController.instance.grid[i, j + 1, zLevel]))
                {
                    to = "_" + tileIndexes[WorldController.instance.grid[i, j + 1, zLevel]].tileName;
                }
                type = "_outer";
                direction = "_NW";
            }
            else if (nb == (new Vector4(1, 1, 1, 0)))
            {
                if (tileIndexes.ContainsKey(WorldController.instance.grid[i - 1, j, zLevel]))
                {
                    to = "_" + tileIndexes[WorldController.instance.grid[i - 1, j, zLevel]].tileName;
                }
                type = "_outer";
                direction = "_W";
            }
            else if (nb == (new Vector4(1, 1, 0, 0)))
            {
                if (tileIndexes.ContainsKey(WorldController.instance.grid[i - 1, j, zLevel]))
                {
                    to = "_" + tileIndexes[WorldController.instance.grid[i - 1, j, zLevel]].tileName;
                }
                type = "_outer";
                direction = "_SW";
            }
            else if (nb == (new Vector4(1, 1, 0, 1)))
            {
                if (tileIndexes.ContainsKey(WorldController.instance.grid[i, j - 1, zLevel]))
                {
                    to = "_" + tileIndexes[WorldController.instance.grid[i, j - 1, zLevel]].tileName;
                }
                type = "_outer";
                direction = "_S";
            }
            else if (nb == (new Vector4(1, 0, 0, 1)))
            {
                if (tileIndexes.ContainsKey(WorldController.instance.grid[i + 1, j, zLevel]))
                {
                    to = "_" + tileIndexes[WorldController.instance.grid[i + 1, j, zLevel]].tileName;
                }
                type = "_outer";
                direction = "_SE";
            }
            else if (nb == (new Vector4(1, 0, 1, 1)))
            {
                if (tileIndexes.ContainsKey(WorldController.instance.grid[i + 1, j, zLevel]))
                {
                    to = "_" + tileIndexes[WorldController.instance.grid[i + 1, j, zLevel]].tileName;
                }
                type = "_outer";
                direction = "_E";
            }
            else if (nb == (new Vector4(1, 1, 1, 1)))
            {
                Vector4 alts = GetAltNeighborComparisonData(i, j, zLevel);

                if (alts == new Vector4(1, 1, 1, 0))
                {
                    if (tileIndexes.ContainsKey(WorldController.instance.grid[i - 1, j + 1, zLevel]))
                    {
                        to = "_" + tileIndexes[WorldController.instance.grid[i - 1, j + 1, zLevel]].tileName;
                    }
                    type = "_inner";
                    direction = "_NW";
                }
                else if (alts == new Vector4(1, 1, 0, 1))
                {
                    if (tileIndexes.ContainsKey(WorldController.instance.grid[i - 1, j - 1, zLevel]))
                    {
                        to = "_" + tileIndexes[WorldController.instance.grid[i - 1, j - 1, zLevel]].tileName;
                    }
                    type = "_inner";
                    direction = "_SW";
                }
                else if (alts == new Vector4(1, 0, 1, 1))
                {
                    if (tileIndexes.ContainsKey(WorldController.instance.grid[i + 1, j - 1, zLevel]))
                    {
                        to = "_" + tileIndexes[WorldController.instance.grid[i + 1, j - 1, zLevel]].tileName;
                    }
                    type = "_inner";
                    direction = "_SE";
                }
                else if (alts == new Vector4(0, 1, 1, 1))
                {
                    if (tileIndexes.ContainsKey(WorldController.instance.grid[i + 1, j + 1, zLevel]))
                    {
                        to = "_" + tileIndexes[WorldController.instance.grid[i + 1, j + 1, zLevel]].tileName;
                    }
                    type = "_inner";
                    direction = "_NE";
                }
                else if (alts == new Vector4(0, 1, 0, 1))
                {
                    if (tileIndexes.ContainsKey(WorldController.instance.grid[i + 1, j + 1, zLevel]))
                    {
                        to = "_" + tileIndexes[WorldController.instance.grid[i + 1, j + 1, zLevel]].tileName;
                    }
                    type = "_inner";
                    direction = "_NE_SW";
                }
                else if (alts == new Vector4(1, 0, 1, 0))
                {
                    if (tileIndexes.ContainsKey(WorldController.instance.grid[i - 1, j + 1, zLevel]))
                    {
                        to = "_" + tileIndexes[WorldController.instance.grid[i - 1, j + 1, zLevel]].tileName;
                    }
                    type = "_inner";
                    direction = "_NW_SE";
                }
                else
                {
                    int v = Random.Range(0, tileIndexes[WorldController.instance.grid[i, j, zLevel]].variantCount);

                    switch(v)
                    {
                        case 0:
                            variant = "_a";
                            break;

                        case 1:
                            variant = "_b";
                            break;

                        case 2:
                            variant = "_c";
                            break;

                        case 3:
                            variant = "_d";
                            break;
                    }
                }
            }
        }

        return tileName + variant + to + type + direction;
    }

    private Vector4 GetNeighborComparisonData(int x, int y, int zLevel)
    {
        Vector4 ret = new Vector4(0, 0, 0, 0);
       
        if(tileIndexes.ContainsKey(WorldController.instance.grid[x, y + 1, zLevel]))
        {
            if (tileIndexes[WorldController.instance.grid[x, y + 1, zLevel]].tileLevel >= tileIndexes[WorldController.instance.grid[x, y, zLevel]].tileLevel)
            {
                ret.x = 1;
            }
        }

        if (tileIndexes.ContainsKey(WorldController.instance.grid[x + 1, y, zLevel]))
        {
            if (tileIndexes[WorldController.instance.grid[x + 1, y, zLevel]].tileLevel >= tileIndexes[WorldController.instance.grid[x, y, zLevel]].tileLevel)
            {
                ret.y = 1;
            }
        }

        if (tileIndexes.ContainsKey(WorldController.instance.grid[x, y - 1, zLevel]))
        {
            if (tileIndexes[WorldController.instance.grid[x, y - 1, zLevel]].tileLevel >= tileIndexes[WorldController.instance.grid[x, y, zLevel]].tileLevel)
            {
                ret.z = 1;
            }
        }

        if (tileIndexes.ContainsKey(WorldController.instance.grid[x - 1, y, zLevel]))
        {
            if (tileIndexes[WorldController.instance.grid[x - 1, y, zLevel]].tileLevel >= tileIndexes[WorldController.instance.grid[x, y, zLevel]].tileLevel)
            {
                ret.w = 1;
            }
        }

        return ret;
    }
    
    private Vector4 GetAltNeighborComparisonData(int x, int y, int zLevel)
    {
        Vector4 ret = new Vector4(0, 0, 0, 0);

        if (tileIndexes.ContainsKey(WorldController.instance.grid[x + 1, y + 1, zLevel]))
        {
            if (tileIndexes[WorldController.instance.grid[x + 1, y + 1, zLevel]].tileLevel >= tileIndexes[WorldController.instance.grid[x, y, zLevel]].tileLevel)
            {
                ret.x = 1;
            }
        }

        if (tileIndexes.ContainsKey(WorldController.instance.grid[x + 1, y - 1, zLevel]))
        {
            if (tileIndexes[WorldController.instance.grid[x + 1, y - 1, zLevel]].tileLevel >= tileIndexes[WorldController.instance.grid[x, y, zLevel]].tileLevel)
            {
                ret.y = 1;
            }
        }

        if (tileIndexes.ContainsKey(WorldController.instance.grid[x - 1, y - 1, zLevel]))
        {
            if (tileIndexes[WorldController.instance.grid[x - 1, y - 1, zLevel]].tileLevel >= tileIndexes[WorldController.instance.grid[x, y, zLevel]].tileLevel)
            {
                ret.z = 1;
            }
        }

        if (tileIndexes.ContainsKey(WorldController.instance.grid[x - 1, y + 1, zLevel]))
        {
            if (tileIndexes[WorldController.instance.grid[x - 1, y + 1, zLevel]].tileLevel >= tileIndexes[WorldController.instance.grid[x, y, zLevel]].tileLevel)
            {
                ret.w = 1;
            }
        }

        return ret;
    }

    public void Draw(bool first = false)
    {
        isCalculating = true;

        var posX = Mathf.Round(Camera.main.transform.position.x / ppu);
        var posY = Mathf.Round(Camera.main.transform.position.y / ppu);

        var oldPosX = Mathf.Round(oldCamPosition.x / ppu);
        var oldPosY = Mathf.Round(oldCamPosition.y / ppu);

        int[,] boxes = {{
                (int) (oldPosX-oldCellNbX/2),
                (int) (oldPosX+oldCellNbX/2),
                (int) (oldPosY-oldCellNbY/2),
                (int) (oldPosY+oldCellNbY/2)
            },{
                (int) (posX-halfCellX),
                (int) (posX+halfCellX),
                (int) (posY-halfCellY),
                (int) (posY+halfCellY)
            }};

        int minX = boxes[1, 0];
        if (boxes[0, 0] < boxes[1, 0])
        {
            minX = boxes[0, 0];
        }

        int maxX = boxes[1, 1];
        if (boxes[0, 1] > boxes[1, 1])
        {
            maxX = boxes[0, 1];
        }

        maxX++;

        int minY = boxes[1, 2];
        if (boxes[0, 2] < boxes[1, 2])
        {
            minY = boxes[0, 2];
        }

        int maxY = boxes[1, 3];
        if (boxes[0, 3] > boxes[1, 3])
        {
            maxY = boxes[0, 3];
        }
        maxY++;

        int maxX2 = boxes[1, 0];
        if (maxX2 < boxes[0, 0])
        {
            maxX2 = boxes[0, 0];
        }
        maxX2++;

        int maxY2 = boxes[1, 2];
        if (maxY2 < boxes[0, 2])
        {
            maxY2 = boxes[0, 2];
        }
        maxY2++;

        int minX2 = boxes[1, 1];
        if (minX2 > boxes[0, 1])
        {
            minX2 = boxes[0, 1];
        }

        int minY2 = boxes[1, 3];
        if (minY2 > boxes[0, 3])
        {
            minY2 = boxes[0, 3];
        }

        int[] loops = {
				minX, maxX2, minY, maxY,
				minX, maxX,minY, maxY2,
				minX2, maxX, minY, maxY,
				minX, maxX, minY2, maxY
            };

        int[] boundingBox = new int[] { boxes[1, 0], boxes[1, 1], boxes[1, 2], boxes[1, 3] };

        if (first)
        {
            loops = boundingBox;
            loops[0]--;
            loops[1]++;
            loops[2]--;
            loops[3]++;
        }

        int _new      = 0;
        int discarded = 0;
        int reused    = 0;

        for (int nb = 0; nb < (int)(loops.Length / 4); nb++)
        {
            if (loops[(nb * 4) + 1] < 0 || loops[(nb * 4) + 3] < 0 ||
                loops[(nb * 4) + 1] == loops[nb * 4] || loops[(nb * 4) + 3] == loops[(nb * 4) + 2])
            {
                continue;
            }

            if (loops[nb * 4] < 0)
            {
                loops[nb * 4] = 0;
            }
            if (loops[(nb * 4) + 2] < 0)
            {
                loops[(nb * 4) + 2] = 0;
            }

            for (int i = loops[nb * 4]; i < loops[(nb * 4) + 1]; i++)
            {
                for (int j = loops[(nb * 4) + 2]; j < loops[(nb * 4) + 3]; j++)
                {
                    if (i < 0 || i > WorldController.instance.worldSizeX - 1 || j < 0 || j > WorldController.instance.worldSizeY - 1)
                    {
                        continue;
                    }

                    if (i < boundingBox[0] || i > boundingBox[1] || j < boundingBox[2] || j > boundingBox[3]) //If the current object is outside of the viewport
                    {
                        if (pool[i, j] != -1) //If the current object is in the pool
                        {
                            freeIndexes.Add(pool[i, j]);
                            objects[freeIndexes[freeIndexes.Count - 1]].SetActive(false);
                            pool[i, j] = -1;
                            discarded++;
                        }
                        continue;
                    }

                    if (!tileIndexes.ContainsKey(WorldController.instance.grid[i, j, zLevel])) //If the grid value of the current tile is empty, skip it
                    {
                        continue;
                    }

                    if (pool[i, j] == -1) // if the current tile doesn't have an active index in the pool
                    {
                        if (freeIndexes.Count > 0) // if we have free index in the pool to be recycled
                        {
                            pool[i, j] = freeIndexes[0];
                            objects[freeIndexes[0]].transform.position = new Vector3(((float)i) * ppu, ((float)j) * ppu, -(float)zLevel);
                            objects[freeIndexes[0]].SetActive(true);
                            freeIndexes.RemoveAt(0);
                            reused++;
                        }
                        else // if we don't have any free indexes available from the pool
                        {
                            GameObject g = (GameObject)Instantiate(baseTile, new Vector3(((float)i) * ppu, ((float)j) * ppu, -(float)zLevel), Quaternion.identity);
                            g.transform.SetParent(transform);
                            objects.Add(g);
                            pool[i, j] = objects.Count - 1;
                            _new++;
                        }

                        string tileName = preComputeTileName == false ? GetTileName(i, j) : tilenames[i, j];

                        try
                        {
                            objects[pool[i, j]].GetComponent<SpriteRenderer>().sprite = sprites[tileName];
                        }
                        catch (KeyNotFoundException)
                        {
                            Debug.Log("Cannot find tile: " + tileName);
                        }
                    }
                    else
                    {
                        if (objects[pool[i, j]].activeSelf != true)
                        {
                            objects[pool[i, j]].SetActive(true);
                        }
                    }
                }
            }
        }

        isCalculating = false;

        oldCellNbX = cellNbX;
        oldCellNbY = cellNbY;

        oldCamPosition = Camera.main.transform.position;
    }

    public Vector2 GetPositionOfTile(MapPos tilePos)
    {
        Vector2 ret = new Vector2();

        ret.x = ((tilePos.x * ppu));
        ret.y = ((tilePos.y * ppu));

        return ret;
    }

    public MapPos GetTileAtPosition(Vector2 pos)
    {
        MapPos tile = new MapPos(0,0);

        tile.x = (int)((pos.x / ppu) + 0.5f);
        tile.y = (int)((pos.y / ppu) + 0.5f);

        return tile;
    }

    private void Update()
    {
        if (initialized && !isCalculating)
        {
            Draw();
        }
    }

    public void UpdateCellNb()
    {
        cellNbX = (int)((float)(Mathf.Ceil((Screen.width / (ppu * 100)) * (Camera.main.orthographicSize / 2.5f)) + 2));
        cellNbY = (int)((float)(Mathf.Ceil((Screen.height / (ppu * 100)) * (Camera.main.orthographicSize / 2.5f)) + 2));

        if (oldCellNbX == 0)
        {
            oldCellNbX = cellNbX;
        }
        if (oldCellNbY == 0)
        {
            oldCellNbY = cellNbY;
        }

        halfCellX = (cellNbX / 2);
        halfCellY = (cellNbY / 2);
    }

    private void OnDestroy()
    {
        ResetPool();
    }

    private void ResetPool()
    {
        for (int i = 0; i < WorldController.instance.worldSizeX; i++)
        {
            for (int j = 0; j < WorldController.instance.worldSizeY; j++)
            {
                pool[i, j] = -1;
            }
        }
    }
}
