﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MapDebugger : MonoBehaviour
{
    public GameObject debuggerPrefab;
    List<GameObject> debuggerNodes;

    public bool debug;
    public int zLevel;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.WORLD_READY_MESSAGE, OnWorldReady);

        debuggerNodes = new List<GameObject>();
    }

    private void OnWorldReady(Message msg)
    {
        if(debug)
        {
            for (int i = 0; i < 100; i++)
            {
                for (int e = 0; e < 100; e++)
                {
                    GameObject g = Instantiate(debuggerPrefab);
                    g.transform.SetParent(transform);
                    g.transform.position = new Vector3(i * 0.48f, e * 0.48f, 0);
                    g.GetComponent<TextMeshPro>().text = WorldController.instance.grid[i, e, zLevel].ToString();
                    debuggerNodes.Add(g);
                }
            }
        }
    }

    private void OnDestroy()
    {
        foreach(GameObject g in debuggerNodes)
        {
            Destroy(g);
        }
    }
}
