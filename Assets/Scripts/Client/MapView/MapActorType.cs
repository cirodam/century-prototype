﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MapActorType 
{
    MAP_ACTOR_TYPE_CITY,
    MAP_ACTOR_TYPE_GROUP,
    MAP_ACTOR_TYPE_CAMP
}
