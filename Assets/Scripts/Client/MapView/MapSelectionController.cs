﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSelectionController : MonoBehaviour
{
    public static MapSelectionController instance;

    private Dictionary<string, IMapSelectable> selectables;
    private IMapSelectable current;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        selectables = new Dictionary<string, IMapSelectable>();

        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_CREATED_MESSAGE, OnMapActorCreated);
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_CLICKED_MESSAGE, OnMapActorClicked);
        MessagePump.instance.Register(this, MessageType.MAP_TERRAIN_CLICKED_MESSAGE, OnMapTerrainClicked);
    }

    private void OnMapActorCreated(Message msg)
    {
        MapActorCreatedMessage cMsg = (MapActorCreatedMessage)msg;
        if(!selectables.ContainsKey(cMsg.mapActorID))
        {
            selectables.Add(cMsg.mapActorID, cMsg.mapActor);
        }
    }

    private void OnMapActorClicked(Message msg)
    {
        MapActorClickedMessage cMsg = (MapActorClickedMessage)msg;

        if (current == null)
        {
            current = selectables[cMsg.mapActorID];
            current.SetHighlight(true);
            MessagePump.instance.QueueMsg(new MapActorSelectedMessage(cMsg.mapActorID));
        }
        else
        {
            DeselectCurrent();
        }
    }

    private void OnMapTerrainClicked(Message msg)
    {
        DeselectCurrent();
    }

    public void DeselectCurrent()
    {
        if (current != null)
        {
            current.SetHighlight(false);
            MessagePump.instance.QueueMsg(new MapActorDeselectedMessage(current.GetID()));
            current = null;
        }
    }
}
