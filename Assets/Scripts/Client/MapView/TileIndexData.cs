﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileIndexData 
{
    public int tileID;
    public string tileName;
    public int tileLevel;
    public int variantCount;

    public TileIndexData(int newID, string newName, int newLevel, int newVariantCount)
    {
        tileID = newID;
        tileName = newName;
        tileLevel = newLevel;
        variantCount = newVariantCount;
    }
}
