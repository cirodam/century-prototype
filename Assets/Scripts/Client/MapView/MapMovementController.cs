﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMovementController : MonoBehaviour
{
    private GroupRemote selected;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_SELECTED_MESSAGE, OnMapActorSelected);
        MessagePump.instance.Register(this, MessageType.MAP_TERRAIN_CLICKED_MESSAGE, OnMapTerrainClicked);
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_CLICKED_MESSAGE, OnMapActorClicked);
    }

    private void OnMapActorSelected(Message msg)
    {
        MapActorSelectedMessage cMsg = (MapActorSelectedMessage)msg;
        selected = GroupStore.instance.GetGroup(cMsg.mapActorID);
    }

    private void OnMapTerrainClicked(Message msg)
    {
        if(selected != null)
        {
            Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            MapPos pos = WorldRenderer.instance.GetTileAtPosition(new Vector2(mouse.x, mouse.y));
            MessagePump.instance.QueueMsg(new ClientMoveGroupIntent(GameClient.instance.GetID(), selected.GetID(), selected.GetWorldPos(), pos));
            selected = null;
        }
    }

    private void OnMapActorClicked(Message msg)
    {
        if(selected != null)
        {
            Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            MapPos pos = WorldRenderer.instance.GetTileAtPosition(new Vector2(mouse.x, mouse.y));
            MessagePump.instance.QueueMsg(new ClientMoveGroupIntent(GameClient.instance.GetID(), selected.GetID(), selected.GetWorldPos(), pos));
            selected = null;
        }
    }
}
