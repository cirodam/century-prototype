﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CampActor : MonoBehaviour, IMapActor
{
    public TextMeshPro nameText;
    public SpriteRenderer image;
    public GameObject highlight;

    public string campID;

    public string GetID()
    {
        return campID;
    }

    public void SetHighlight(bool val)
    {
        highlight.SetActive(val);
    }
}
