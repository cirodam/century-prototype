﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientVictoryPanelController : MonoBehaviour
{
    public GameObject panel;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.CLIENT_GAME_VICTORY_MESSAGE, OnClientGameVictory);
    }

    private void OnClientGameVictory(Message msg)
    {
        ClientGameVictoryMessage cMsg = (ClientGameVictoryMessage)msg;

        if(cMsg.clientID == ViewController.instance.playerFactionID)
        {
            Show();
        }
    }

    public void Show()
    {
        panel.SetActive(true);
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    public void OnLeaveButton()
    {
        Application.Quit();
    }
}
