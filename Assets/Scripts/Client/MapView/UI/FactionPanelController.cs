﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FactionPanelController : MonoBehaviour
{
    public GameObject factionPanel;

    public GameObject listRoot;
    public GameObject factionListEntry;
    List<GameObject> list;

    private void Awake()
    {
        list = new List<GameObject>();
    }

    public void Show()
    {
        if (!factionPanel.activeSelf)
        {
            factionPanel.SetActive(true);
            RefreshList();
        }
    }

    public void Hide()
    {
        ClearList();
        factionPanel.SetActive(false);
    }

    public void RefreshList()
    {
        ClearList();
        foreach(ClientData c in ClientStore.instance.clientData.Values)
        {
            GameObject g = Instantiate(factionListEntry);
            g.transform.SetParent(listRoot.transform);
            g.GetComponentInChildren<TextMeshProUGUI>().text = c.clientFactionName;
            list.Add(g);
        }
    }

    public void ClearList()
    {
        foreach(GameObject g in list)
        {
            Destroy(g);
        }
        list.Clear();
    }
}
