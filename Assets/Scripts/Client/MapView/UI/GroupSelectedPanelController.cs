﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GroupSelectedPanelController : MonoBehaviour
{
    public GameObject panel;
    GroupRemote targetGroup;

    public TextMeshProUGUI groupNameText;
    public TextMeshProUGUI soldierCountText;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_SELECTED_MESSAGE, OnMapActorSelected);
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_DESELECTED_MESSAGE, OnMapActorDeselected);
    }

    private void OnMapActorSelected(Message msg)
    {
        MapActorSelectedMessage cMsg = (MapActorSelectedMessage)msg;
        targetGroup = GroupStore.instance.GetGroup(cMsg.mapActorID);
        if(targetGroup != null)
        {
            Show();
        }
    }

    private void OnMapActorDeselected(Message msg)
    {
        if(targetGroup != null)
        {
            targetGroup = null;
            Hide();
        }
    }

    public void Show()
    {
        panel.SetActive(true);
        groupNameText.text = targetGroup.GetTitle();
        //soldierCountText.text = target.GetCurrentSoldierCount() + "/" + target.GetMaxSoldierCount();
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    public void OnBackButton()
    {
        MapSelectionController.instance.DeselectCurrent();
    }

    public void OnEditButton()
    {
        GroupEditPanelController.instance.Show(targetGroup);
    }

    public void OnResourceCampButton()
    {
        Natural n = WorldCache.instance.GetNaturalAtPosition(targetGroup.GetWorldPos());
        if (n != null)
        {
            NaturalDefinition nd = WorldDataController.instance.GetDataForNatural(n.name);
            GameServer.instance.CreateCamp(GameClient.instance.GetID(), nd.campName, targetGroup.GetWorldPos());
        }
    }

    public void OnFortButton()
    {
        Natural n = WorldCache.instance.GetNaturalAtPosition(targetGroup.GetWorldPos());
        if (n == null)
        {
            GameServer.instance.CreateCamp(GameClient.instance.GetID(), "Fort", targetGroup.GetWorldPos());
        }
    }
}
