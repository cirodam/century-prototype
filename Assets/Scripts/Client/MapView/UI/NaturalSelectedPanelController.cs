﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NaturalSelectedPanelController : MonoBehaviour
{
    public GameObject panel;

    public TextMeshProUGUI titleText;

    public Natural target;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_SELECTED_MESSAGE, OnMapActorSelected);
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_DESELECTED_MESSAGE, OnMapActorDeselected);
    }

    private void OnMapActorSelected(Message msg)
    {
        MapActorSelectedMessage cMsg = (MapActorSelectedMessage)msg;

        if(WorldCache.instance.naturals.ContainsKey(cMsg.mapActorID))
        {
            target = WorldCache.instance.naturals[cMsg.mapActorID];
            Show();
        }
    }

    private void OnMapActorDeselected(Message msg)
    {
        Hide();
    }

    public void OnBackButton()
    {
        MapSelectionController.instance.DeselectCurrent();
    }

    private void Show()
    {
        panel.SetActive(true);

        titleText.text = target.name;
    }

    private void Hide()
    {
        panel.SetActive(false);
    }
}
