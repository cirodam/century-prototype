﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EditListEntry : MonoBehaviour
{
    public TextMeshProUGUI soldierText;
    public TextMeshProUGUI weaponText;

    public Image weaponImage;
}
