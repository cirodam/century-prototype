﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PendingBattlePanelController : MonoBehaviour
{
    public static PendingBattlePanelController instance;
    public GameObject panel;

    public TextMeshProUGUI attackingNameText;
    public TextMeshProUGUI defendingNameText;

    GroupRemote attacker;
    GroupRemote defender;

    ICity hostCity;

    string battleID;
    string groupID;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        MessagePump.instance.Register(this, MessageType.CLIENT_BATTLE_PENDING_MESSAGE, OnClientBattlePending);
    }

    private void OnClientBattlePending(Message msg)
    {
        ClientBattlePendingMessage cMsg = (ClientBattlePendingMessage)msg;

        attacker = GroupStore.instance.remotes[cMsg.attackerID];
        defender = GroupStore.instance.GetGroup(cMsg.defenderID);
        battleID = cMsg.battleID;

        if (attacker.GetOwnerID() == GameClient.instance.GetID())
        {
            groupID = cMsg.attackerID;
            Show();
        }
        else if(defender.GetOwnerID() == GameClient.instance.GetID())
        {
            groupID = cMsg.defenderID;
            Show();
        }
    }

    public void Show()
    {
        panel.SetActive(true);

        defendingNameText.text = defender.GetTitle();
        attackingNameText.text = attacker.GetTitle();
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    public void OnFightButton()
    {
        GameServer.instance.FightBattle(GameClient.instance.GetID(), battleID);
        ViewController.instance.ToBattleView(battleID, groupID);
    }
}
