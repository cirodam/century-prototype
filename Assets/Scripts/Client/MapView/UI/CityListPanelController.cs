﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityListPanelController : MonoBehaviour
{
    public GameObject panel;

    public GameObject entryPrefab;
    public GameObject listRoot;
    List<GameObject> list;

    private void Awake()
    {
        list = new List<GameObject>();
    }

    public void Show()
    {
        panel.SetActive(true);
        RefreshList();
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    private void RefreshList()
    {
        ClearList();
        foreach(CityRemote c in CityStore.instance.remotes.Values)
        {
            GameObject g = Instantiate(entryPrefab);
            g.transform.SetParent(listRoot.transform);
            g.GetComponent<CityListEntry>().cityNameText.text = c.GetCityName();
            //g.GetComponent<CityListEntry>().cityZoomButton.onClick.AddListener(() => { OnCityZoomClicked(c); });
            list.Add(g);
        }
    }

    private void ClearList()
    {
        foreach(GameObject g in list)
        {
            Destroy(g);
        }
        list.Clear();
    }

    private void OnCityZoomClicked(CityRemote c)
    {
        ViewController.instance.ToCityView(c);
    }
}
