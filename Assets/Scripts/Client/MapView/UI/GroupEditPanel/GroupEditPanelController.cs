﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GroupEditPanelController : MonoBehaviour
{
    public static GroupEditPanelController instance;

    public GameObject panel;

    public GameObject listPrefab;
    public GameObject listRoot;
    List<GameObject> list;

    GroupRemote target;

    private void Awake()
    {
        if(instance == null)
        {
            list = new List<GameObject>();
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        instance = this;

        MessagePump.instance.Register(this, MessageType.REMOTE_VALUE_UPDATED_MESSAGE, OnRemoteValueUpdated);
    }

    private void OnRemoteValueUpdated(Message msg)
    {
        RemoteValueUpdatedMessage cMsg = (RemoteValueUpdatedMessage)msg;
        if(panel.activeSelf)
        {
            if (target.GetUnit(cMsg.id) != null)
            {
                RefreshList();
            }
        }
    }

    public void Show(GroupRemote newTarget)
    {
        target = newTarget;
        panel.SetActive(true);
        RefreshList();
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    public void OnBackButton()
    {
        Hide();
    }

    public void RefreshList()
    {
        ClearList();
        foreach(UnitRemote u in target.units.Values)
        {
            GameObject g = Instantiate(listPrefab);
            g.transform.SetParent(listRoot.transform);

            g.GetComponent<UnitEditEntry>().SetUnit(u);
            list.Add(g);
        }
    }

    public void ClearList()
    {
        foreach(GameObject g in list)
        {
            Destroy(g);
        }
        list.Clear();
    }
}
