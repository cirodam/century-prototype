﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShipEditEntry : MonoBehaviour
{
    public TextMeshProUGUI shipNameText;
    public TextMeshProUGUI shipHullText;
}
