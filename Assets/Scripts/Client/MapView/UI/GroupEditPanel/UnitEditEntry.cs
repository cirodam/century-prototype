﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitEditEntry : MonoBehaviour
{
    public TextMeshProUGUI unitNameText;
    public TextMeshProUGUI soldierCountText;
    public TextMeshProUGUI weaponCountText;

    public Image weaponImage;

    UnitRemote remote;

    public void SetUnit(UnitRemote remoteArg)
    {
        remote = remoteArg;

        //unitNameText.text = remote.GetTitle();

        //weaponImage.sprite = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(remote.GetString("WeaponName")).iconPath);
    }

}
