﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BattalionEditEntry : MonoBehaviour
{
    public TextMeshProUGUI soldierCountText;
    public TextMeshProUGUI attackStrengthText;
    public TextMeshProUGUI healthText;
}
