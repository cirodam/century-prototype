﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CitySelectedPanelController : MonoBehaviour
{
    public GameObject panel;

    public TextMeshProUGUI cityNameText;

    CityRemote targetCity;

    public GameObject garrisonButtonPrefab;
    public GameObject garrisonButtonRoot;
    List<GameObject> garrisonList;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_SELECTED_MESSAGE, OnMapActorSelected);
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_DESELECTED_MESSAGE, OnMapActorDeselected);

        garrisonList = new List<GameObject>();
    }

    private void OnMapActorSelected(Message msg)
    {
        MapActorSelectedMessage cMsg = (MapActorSelectedMessage)msg;

        targetCity = CityStore.instance.GetCity(cMsg.mapActorID);
        if(targetCity != null)
        {
            Show();
        }
    }

    private void OnMapActorDeselected(Message msg)
    {
        MapActorDeselectedMessage cMsg = (MapActorDeselectedMessage)msg;

        if(targetCity != null)
        {
            targetCity = null;
            Hide();
        }
    }

    public void OnBackButton()
    {
        MapSelectionController.instance.DeselectCurrent();
    }

    public void Show()
    {
        panel.SetActive(true);
        cityNameText.text = targetCity.GetCityName();

        foreach(string s in targetCity.GetGarrisons())
        {
            GameObject g = Instantiate(garrisonButtonPrefab);
            g.transform.SetParent(garrisonButtonRoot.transform);
            g.GetComponent<GarrisonButton>().button.onClick.AddListener(() => { OnGarrisonButton(g, s); });
            garrisonList.Add(g);
        }
    }

    public void OnGarrisonButton(GameObject button, string groupID)
    {
        Destroy(button);
        garrisonList.Remove(button);

        GameServer.instance.DeployGroup(groupID, targetCity.GetID(), targetCity.GetWorldPos());
        MapSelectionController.instance.DeselectCurrent();
    }

    public void ClearList()
    {
        foreach(GameObject g in garrisonList)
        {
            Destroy(g);
        }
        garrisonList.Clear();
    }

    public void OnBuildingButton()
    {
        ViewController.instance.ToCityView(targetCity);
    }

    public void OnRaiseArmyButton()
    {
        GroupRaisePanelController.instance.Show(targetCity.GetID());
    }

    public void OnRaiseFleetButton()
    {
        RaiseArmyPanelController.instance.ShowFleet(targetCity.GetID());
    }

    public void OnRaiseAirgroupButton()
    {
        RaiseArmyPanelController.instance.ShowAirgroup(targetCity.GetID());
    }

    public void Hide()
    {
        panel.SetActive(false);
        ClearList();
    }
}
