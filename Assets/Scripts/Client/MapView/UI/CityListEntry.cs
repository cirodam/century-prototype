﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CityListEntry : MonoBehaviour
{
    public TextMeshProUGUI cityNameText;
    public Button cityZoomButton;
}
