﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnitListEntry 
{
    void SetUnitID(int newID);
    UnitTemplate GetTemplate();
}
