﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockpilePanelController : MonoBehaviour
{
    public GameObject panel;

    public GameObject listPrefab;
    public GameObject listRoot;
    List<GameObject> list;

    StockpileRemote stockpile;

    private void Awake()
    {
        list = new List<GameObject>();

        MessagePump.instance.Register(this, MessageType.STOCKPILE_REMOTE_UPDATED_MESSAGE, OnStockpileRemoteUpdated);
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);

        stockpile = StockpileStore.instance.remote;
        if(stockpile != null)
        {
            RefreshList();
        }
    }

    private void OnStockpileRemoteUpdated(Message msg)
    {
        stockpile = StockpileStore.instance.remote;
        RefreshList();
    }

    private void OnNewDay(Message msg)
    {
        RefreshList();
    }

    public void Show()
    {
        panel.SetActive(true);
    }

    public void Hide()
    {
        panel.SetActive(false);
    }

    private void RefreshList()
    {
        if(stockpile != null)
        {
            ClearList();
            foreach (ResourceInstance r in stockpile.resources.Values)
            {
                GameObject g = Instantiate(listPrefab);
                g.transform.SetParent(listRoot.transform);

                StockpileListEntry e = g.GetComponent<StockpileListEntry>();
                e.resourceImage.sprite = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(r.resourceName).iconPath);
                e.amountText.text = Math.Round(r.resourceAmount,2).ToString();

                float delta = (float) Math.Round(r.resourceAmount - r.lastAmount, 2);

                if(delta > 0)
                {
                    e.deltaText.text = "+" + delta;
                }
                else if(delta < 0)
                {
                    e.deltaText.text = delta.ToString();
                }
                else
                {
                    e.deltaText.text = "";
                }

                list.Add(g);
            }
        }
    }

    private void ClearList()
    {
        foreach(GameObject g in list)
        {
            Destroy(g);
        }
        list.Clear();
    }
}

