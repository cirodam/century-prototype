﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StockpileListEntry : MonoBehaviour
{
    public Image resourceImage;
    public TextMeshProUGUI amountText;
    public TextMeshProUGUI deltaText;
}
