﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitTemplateListEntry : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI soldierCountText;

    public Image weaponImage;
    public Image soldierImage;

    public Button addButton;

    UnitTemplate template;

    public void SetTemplate(UnitTemplate templateArg)
    {
        template = templateArg;

        nameText.text = template.unitName;
        soldierCountText.text = template.soldierMax.ToString();

        weaponImage.sprite = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(template.weaponName).iconPath);
    }

    public UnitTemplate GetTemplate()
    {
        return template;
    }
}
