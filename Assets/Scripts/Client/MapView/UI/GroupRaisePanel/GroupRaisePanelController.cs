﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GroupRaisePanelController : MonoBehaviour
{
    public static GroupRaisePanelController instance;

    public GameObject panel;

    public GameObject templateEntryPrefab;
    public GameObject templateListRoot;
    List<GameObject> templateList;

    public GameObject unitListEntryPrefab;
    public GameObject unitListRoot;
    List<GameObject> unitList;

    public TextMeshProUGUI unitCountText;
    public TMP_InputField nameInput;

    string cityID;
    int nextUnit;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        templateList = new List<GameObject>();
        unitList = new List<GameObject>();
    }

    public void Show(string cityIDArg)
    {
        cityID = cityIDArg;
        nextUnit = 1;

        panel.SetActive(true);

        AddTemplateEntry(new UnitTemplate("Sword Battalion", "Soldier", "Sword", 40, 40, UnitType.UNIT_TYPE_BATTALION));
        AddTemplateEntry(new UnitTemplate("Axe Battalion", "Soldier", "Axe", 40, 40, UnitType.UNIT_TYPE_BATTALION));
        AddTemplateEntry(new UnitTemplate("Spear Battalion", "Soldier", "Spear", 40, 40, UnitType.UNIT_TYPE_BATTALION));

        nameInput.text = "Army Name";
        RefreshUnitDetails();
    }

    public void Hide()
    {
        panel.SetActive(false);

        foreach(GameObject g in templateList)
        {
            Destroy(g);
        }
        templateList.Clear();

        foreach (GameObject g in unitList)
        {
            Destroy(g);
        }
        unitList.Clear();
    }

    private void AddTemplateEntry(UnitTemplate template)
    {
        GameObject g = Instantiate(templateEntryPrefab);
        g.transform.SetParent(templateListRoot.transform);

        g.GetComponent<UnitTemplateListEntry>().SetTemplate(template);
        g.GetComponent<UnitTemplateListEntry>().addButton.onClick.AddListener(() => { OnTemplateAddButton(g); });

        templateList.Add(g);
    }

    private void OnTemplateAddButton(GameObject unitTemplateEntry)
    {
        AddUnitEntry(unitTemplateEntry.GetComponent<UnitTemplateListEntry>().GetTemplate());
    }

    public void OnBackButton()
    {
        Hide();
    }

    private void AddUnitEntry(UnitTemplate template)
    {
        GameObject g = Instantiate(unitListEntryPrefab);
        g.transform.SetParent(unitListRoot.transform);

        g.GetComponent<UnitListEntry>().SetTemplate(template);
        g.GetComponent<UnitListEntry>().orderText.text = nextUnit.ToString();
        g.GetComponent<UnitListEntry>().deleteButton.onClick.AddListener(() => { OnUnitDeleteButton(g); });

        unitList.Add(g);

        RefreshUnitDetails();
        nextUnit++;
    }

    private void OnUnitDeleteButton(GameObject unitEntry)
    {
        unitList.Remove(unitEntry);
        Destroy(unitEntry);

        RefreshUnitDetails();
    }

    private void RefreshUnitDetails()
    {
        unitCountText.text = "Units: " + unitList.Count;
    }

    public void OnRaiseButton()
    {
        GroupTemplate g = new GroupTemplate();

        g.groupName = nameInput.text;
        g.movementType = MovementType.MOVEMENT_LAND;
        g.units = new List<UnitTemplate>();

        foreach(GameObject o in unitList)
        {
            UnitListEntry e = o.GetComponent<UnitListEntry>();
            g.units.Add(e.GetTemplate());
        }

        GameServer.instance.CreateGroup(GameClient.instance.GetID(), cityID, g);

        Hide();
    }
}
