﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitListEntry : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI soldierCountText;
    public TextMeshProUGUI weaponCountText;
    public TextMeshProUGUI orderText;

    public Image soldierImage;
    public Image weapnImage;

    public Button deleteButton;
    public Button upButton;
    public Button downButton;

    UnitTemplate template;

    public void SetTemplate(UnitTemplate templateArg)
    {
        template = templateArg;

        nameText.text = template.unitName;
        soldierCountText.text = template.soldierMax.ToString();
        weaponCountText.text = template.weaponMax.ToString();

        weapnImage.sprite = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(template.weaponName).iconPath);

    }

    public UnitTemplate GetTemplate()
    {
        return template;
    }
}
