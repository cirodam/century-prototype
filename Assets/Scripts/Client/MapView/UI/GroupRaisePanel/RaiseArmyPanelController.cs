﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RaiseArmyPanelController : MonoBehaviour
{
    public static RaiseArmyPanelController instance;
    public GameObject panel;

    public TMP_InputField nameInput;

    public GameObject battalionEntryPrefab;
    public GameObject sectionEntryPrefab;
    public GameObject shipEntryPrefab;

    public GameObject listRoot;
    List<GameObject> entryList;
    public GameObject additionListEntry;
    int nextBatt;

    MovementType movement;
    string cityID;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        additionListEntry.GetComponent<AdditionListEntry>().battalionButton.button.onClick.AddListener(AddBattalionListEntry);
        additionListEntry.GetComponent<AdditionListEntry>().sectionButton.button.onClick.AddListener(AddSectionListEntry);
        additionListEntry.GetComponent<AdditionListEntry>().shipButton.button.onClick.AddListener(AddShipListEntry);

        entryList = new List<GameObject>();
        nameInput.text = "Army 1";
    }

    public void ShowArmy(string newCityID)
    {
        panel.SetActive(true);
        additionListEntry.GetComponent<AdditionListEntry>().ConfigureArmy();
        nextBatt = 1;

        movement = MovementType.MOVEMENT_LAND;
        cityID = newCityID; 
    }

    public void ShowFleet(string newCityID)
    {
        panel.SetActive(true);
        additionListEntry.GetComponent<AdditionListEntry>().ConfigureFleet();
        nextBatt = 1;

        movement = MovementType.MOVEMENT_SEA;
        cityID = newCityID;
    }

    public void ShowAirgroup(string newCityID)
    {
        panel.SetActive(true);
        additionListEntry.GetComponent<AdditionListEntry>().ConfigureAirgroup();
        nextBatt = 1;

        movement = MovementType.MOVEMENT_AIR;
        cityID = newCityID;
    }

    public void Hide()
    {
        panel.SetActive(false);
        ClearList();
    }

    public void OnBackButton()
    {
        Hide();
    }

    public void OnRaiseButton()
    {
        List<UnitTemplate> units = new List<UnitTemplate>();
        foreach (GameObject g in entryList)
        {
            IUnitListEntry i = g.GetComponent<IUnitListEntry>();
            if (i != null)
            {
                units.Add(i.GetTemplate());
            }
        }

        GameServer.instance.CreateGroup(GameClient.instance.GetID(), cityID, new GroupTemplate(nameInput.text, movement, units));
        Hide();
    }

    public void AddBattalionListEntry()
    {
        GameObject entry = Instantiate(battalionEntryPrefab);
        entry.transform.SetParent(listRoot.transform);
        entry.GetComponent<BattalionListEntry>().deleteButton.onClick.AddListener(() => { OnDeleteButton(entry); });
        entry.GetComponent<BattalionListEntry>().SetUnitID(nextBatt);

        foreach (ResourceDefinition r in ResourceDataController.instance.GetResourcesMeetingRole("BattalionWeaponHand"))
        {
            Sprite s = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(r.resourceName).iconPath);
            entry.GetComponent<BattalionListEntry>().weaponDropdown.options.Add(new TMP_Dropdown.OptionData(s));
        }

        entryList.Add(entry);
        additionListEntry.transform.SetAsLastSibling();
        nextBatt++;
    }

    public void AddSectionListEntry()
    {
        GameObject entry = Instantiate(sectionEntryPrefab);
        entry.transform.SetParent(listRoot.transform);
        entry.GetComponent<SectionListEntry>().deleteButton.onClick.AddListener(() => { OnDeleteButton(entry); });
        entry.GetComponent<SectionListEntry>().SetUnitID(nextBatt);

        foreach(ResourceDefinition r in ResourceDataController.instance.GetResourcesMeetingRole("Vehicle"))
        {
            Sprite s = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(r.resourceName).iconPath);
            entry.GetComponent<SectionListEntry>().weaponDropdown.options.Add(new TMP_Dropdown.OptionData(s));
        }

        entryList.Add(entry);
        additionListEntry.transform.SetAsLastSibling();
        nextBatt++;
    }

    public void AddShipListEntry()
    {
        GameObject entry = Instantiate(shipEntryPrefab);
        entry.transform.SetParent(listRoot.transform);
        entry.GetComponent<ShipListEntry>().deleteButton.onClick.AddListener(() => { OnDeleteButton(entry); });
        entry.GetComponent<ShipListEntry>().SetUnitID(nextBatt);

        foreach (ResourceDefinition r in ResourceDataController.instance.GetResourcesMeetingRole("Ship"))
        {
            Sprite s = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(r.resourceName).iconPath);
            entry.GetComponent<ShipListEntry>().weaponDropdown.options.Add(new TMP_Dropdown.OptionData(s));
        }

        entryList.Add(entry);
        additionListEntry.transform.SetAsLastSibling();
        nextBatt++;
    }

    public void OnDeleteButton(GameObject entry)
    {
        Destroy(entry);
        entryList.Remove(entry);

        nextBatt = 1;
        foreach (GameObject g in entryList)
        {
            IUnitListEntry i = g.GetComponent<IUnitListEntry>();
            if (i != null)
            {
                i.SetUnitID(nextBatt);
                nextBatt++;
                continue;
            }
        }
    }

    private void ClearList()
    {
        foreach(GameObject g in entryList)
        {
            Destroy(g);
        }
        entryList.Clear();
    }
}
