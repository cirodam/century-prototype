﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AdditionListEntry : MonoBehaviour
{
    public AdditionListButton battalionButton;
    public AdditionListButton sectionButton;
    public AdditionListButton shipButton;

    public Sprite armySectionSprite;
    public Sprite airgroupSectionSprite;
    public Sprite airgroupShipSprite;
    public Sprite fleetShipSprite;

    public void ResetButtons()
    {
        battalionButton.gameObject.SetActive(false);
        sectionButton.gameObject.SetActive(false);
        shipButton.gameObject.SetActive(false);
    }

    public void ConfigureArmy()
    {
        ResetButtons();

        battalionButton.gameObject.SetActive(true);
        sectionButton.gameObject.SetActive(true);

        sectionButton.detailImage.sprite = armySectionSprite;
    }

    public void ConfigureFleet()
    {
        ResetButtons();

        shipButton.gameObject.SetActive(true);
        shipButton.detailImage.sprite = fleetShipSprite;
    }

    public void ConfigureAirgroup()
    {
        ResetButtons();

        sectionButton.gameObject.SetActive(true);
        shipButton.gameObject.SetActive(true);

        sectionButton.detailImage.sprite = airgroupSectionSprite;
        shipButton.detailImage.sprite = airgroupShipSprite;
    }
}
