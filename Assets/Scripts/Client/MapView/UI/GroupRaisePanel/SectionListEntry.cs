﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SectionListEntry : MonoBehaviour, IUnitListEntry
{
    public TextMeshProUGUI numberText;
    public TMP_InputField weaponInput;
    public TMP_Dropdown weaponDropdown;
    public Button deleteButton;

    UnitTemplate template;

    private void Awake()
    {
        template = new UnitTemplate();
    }

    public void SetUnitID(int newID)
    {
        numberText.text = newID.ToString();
    }

    public void OnWeaponDropdownChange()
    {

    }

    public void OnWeaponInputChange()
    {
        template.weaponMax = int.Parse(weaponInput.text);
        template.soldierMax = template.weaponMax;
    }

    public UnitTemplate GetTemplate()
    {
        return template;
    }
}
