﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShipListEntry : MonoBehaviour, IUnitListEntry
{
    public TextMeshProUGUI numberText;
    public TMP_InputField nameInput;
    public TMP_Dropdown weaponDropdown;
    public Button deleteButton;

    UnitTemplate template;

    private void Awake()
    {
        template = new UnitTemplate();

        template.soldierName = "Soldier";
        template.soldierMax = 80;

        template.weaponName = "Ship";
        template.weaponMax = 1;
        template.unitType = UnitType.UNIT_TYPE_SHIP;
    }

    public void SetUnitID(int newID)
    {
        numberText.text = newID.ToString();
    }

    public void OnWeaponDropdownChange()
    {

    }

    public void OnNameInputChange()
    {
        template.unitName = nameInput.text;
    }

    public UnitTemplate GetTemplate()
    {
        return template;
    }
}
