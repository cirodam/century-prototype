﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BattalionListEntry : MonoBehaviour, IUnitListEntry
{
    public TextMeshProUGUI numberText;
    public TMP_InputField soldierInput;
    public TMP_Dropdown weaponDropdown;
    public Button deleteButton;

    UnitTemplate template;

    private void Awake()
    {
        template = new UnitTemplate();
        template.soldierName = "Soldier";
        template.weaponName = ResourceDataController.instance.GetResourcesMeetingRole("BattalionWeaponHand")[weaponDropdown.value].resourceName;
    }

    public void SetUnitID(int newID)
    {
        numberText.text = newID.ToString();
    }

    public void OnWeaponDropdownChange()
    {
        template.weaponName = ResourceDataController.instance.GetResourcesMeetingRole("BattalionWeaponHand")[weaponDropdown.value].resourceName;
    }

    public void OnSoldierInputChange()
    {
        template.soldierMax = int.Parse(soldierInput.text);
        template.weaponMax = int.Parse(soldierInput.text);
    }

    public UnitTemplate GetTemplate()
    {
        return template;
    }
}
