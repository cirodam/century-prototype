﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CampSelectedPanelController : MonoBehaviour
{
    public GameObject panel;
    public TextMeshProUGUI titleText;

    public GameObject garrisonButtonPrefab;
    public GameObject garrisonButtonRoot;
    List<GameObject> garrisonButtons;

    CampRemote camp;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_SELECTED_MESSAGE, OnMapActorSelected);
        MessagePump.instance.Register(this, MessageType.MAP_ACTOR_DESELECTED_MESSAGE, OnMapActorDeselected);

        garrisonButtons = new List<GameObject>();
    }

    private void OnMapActorSelected(Message msg)
    {
        MapActorSelectedMessage cMsg = (MapActorSelectedMessage)msg;

        CampRemote c = CampStore.instance.GetCamp(cMsg.mapActorID);
        if(c != null)
        {
            camp = c;
            Show();
        }
    }

    private void OnMapActorDeselected(Message msg)
    {
        MapActorDeselectedMessage cMsg = (MapActorDeselectedMessage)msg;

        if(camp != null)
        {
            if (camp.GetID() == cMsg.mapActorID)
            {
                Hide();
            }
        }
    }

    public void Show()
    {
        panel.SetActive(true);
        titleText.text = camp.GetCampName();
        RefreshList();
    }

    public void Hide()
    {
        panel.SetActive(false);
        camp = null;
    }

    public void OnBackButton()
    {
        MapSelectionController.instance.DeselectCurrent();
    }

    public void RefreshList()
    {
        ClearList();

        foreach(string s in camp.GetGarrisons())
        {
            AddGarrisonButton(s);
        }
    }

    public void ClearList()
    {
        foreach(GameObject g in garrisonButtons)
        {
            Destroy(g);
        }
        garrisonButtons.Clear();
    }

    public void AddGarrisonButton(string targetID)
    {
        GameObject g = Instantiate(garrisonButtonPrefab);
        g.transform.SetParent(garrisonButtonRoot.transform);
        g.GetComponent<Button>().onClick.AddListener(() => { OnGarrisonButtonClicked(g, targetID); });
        garrisonButtons.Add(g);
    }

    private void OnGarrisonButtonClicked(GameObject g, string targetID)
    {
        garrisonButtons.Remove(g);
        Destroy(g);

        GameServer.instance.DeployGroup(targetID, camp.GetID(), camp.GetMapPos());
    }
}
