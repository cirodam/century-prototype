﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimePanelController : MonoBehaviour
{
    public TextMeshProUGUI dayText;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
        dayText.text = TimeController.instance.GetCurrentDay().ToString("00") + " . " + TimeController.instance.GetCurrentMonth().ToString("00") + " . " + TimeController.instance.GetCurrentYear().ToString("0000");
    }

    private void OnNewDay(Message msg)
    {
        NewDayMessage cMsg = (NewDayMessage)msg;
        dayText.text = cMsg.day.ToString("00") + " . " + cMsg.month.ToString("00") + " . " + cMsg.year.ToString("0000");
    }

    public void OnPauseClick()
    {
        TimeController.instance.PauseSimulation();
    }

    public void OnPlayClick()
    {
        TimeController.instance.UnpauseSimulation();
    }
}
