﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StringValueEntry 
{
    public string key;
    public string value;
    public StringValueType type;

    public StringValueEntry()
    {

    }

    public StringValueEntry(string keyArg, string valueArg, StringValueType typeArg)
    {
        key   = keyArg;
        value = valueArg;
        type  = typeArg;
    }
}
