﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ValueType 
{
    VALUE_TYPE_INT,
    VALUE_TYPE_FLOAT,
    VALUE_TYPE_STRING,
    VALUE_TYPE_BOOL,
    VALUE_TYPE_MAPPOS,
    VALUE_TYPE_VECTOR2
}
