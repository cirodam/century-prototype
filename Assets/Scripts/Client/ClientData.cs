﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientData
{
    public ClientType clientType;
    public string clientFactionName;
    public Color clientFactionColor;

    public ClientData(ClientType newType, string newFactionName, Color newFactionColor)
    {
        clientType         = newType;
        clientFactionName  = newFactionName;
        clientFactionColor = newFactionColor;
    }
}
