﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIClient : MonoBehaviour
{
    private string clientID;
    private ClientData clientData;

    string groupID = "";
    string battleID;
    List<MapPos> deployArea;

    private void Awake()
    {
        MessagePump.instance.Register(this, MessageType.SERVER_PRESENT_MESSAGE, OnServerPresent);
        MessagePump.instance.Register(this, MessageType.ALL_CLIENTS_PRESENT_MESSAGE, OnAllClientsPresent);
        MessagePump.instance.Register(this, MessageType.DEPLOYMENT_AREA_DEFINED_MESSAGE, OnDeploymentAreaDefined);
        MessagePump.instance.Register(this, MessageType.CLIENT_BATTLE_PENDING_MESSAGE, OnBattlePending);
    }

    private void OnServerPresent(Message msg)
    {
        MessagePump.instance.QueueMsg(new ClientPresentMessage(clientID));
    }

    private void OnAllClientsPresent(Message msg)
    {
        MessagePump.instance.QueueMsg(new ClientDataReadyMessage(clientID, clientData));
        MessagePump.instance.QueueMsg(new ClientReadyMessage(clientID));
    }

    private void OnBattlePending(Message msg)
    {
        ClientBattlePendingMessage cMsg = (ClientBattlePendingMessage)msg;

        GroupRemote attacker = GroupStore.instance.GetGroup(cMsg.attackerID);
        GroupRemote defender = GroupStore.instance.GetGroup(cMsg.defenderID);

        battleID = cMsg.battleID;
        if (attacker.GetOwnerID() == clientID)
        {
            groupID = attacker.GetID();
        }
        else if(defender.GetOwnerID() == clientID)
        {
            groupID = defender.GetID();
        }
    }

    private void OnDeploymentAreaDefined(Message msg)
    {
        DeploymentAreaDefinedMessage cMsg = (DeploymentAreaDefinedMessage)msg;

        if(cMsg.groupID == groupID)
        {
            deployArea = new List<MapPos>(cMsg.deploymentArea);

            List<MapPos> avTiles = new List<MapPos>(deployArea);
            GroupRemote group = GroupStore.instance.GetGroup(groupID);
            foreach (UnitRemote r in group.units.Values)
            {
                MapPos m = avTiles[Random.Range(0, avTiles.Count - 1)];
                GameServer.instance.DeployUnitOnBattleField(clientID, battleID, group.GetID(), r.GetID(), m);
                avTiles.Remove(m);
            }
        }
    }

    public void SetID(string newID)
    {
        clientID = newID;
    }

    public void SetData(ClientData newData)
    {
        clientData = newData;
    }

    public string GetID()
    {
        return clientID;
    }
}
