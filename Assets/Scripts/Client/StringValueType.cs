﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StringValueType 
{
    VALUE_TYPE_STRING,
    VALUE_TYPE_INT,
    VALUE_TYPE_FLOAT,
    VALUE_TYPE_BOOL
}
