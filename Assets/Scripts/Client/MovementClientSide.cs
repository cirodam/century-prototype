﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementClientSide : MonoBehaviour
{
    Dictionary<string, GroupRemote> groups;
    Dictionary<string, PathAnimator> animators;

    List<string> toRemove;

    private void Awake()
    {
        groups = new Dictionary<string, GroupRemote>();
        animators = new Dictionary<string, PathAnimator>();
        toRemove = new List<string>();

        MessagePump.instance.Register(this, MessageType.GROUP_REMOTE_UPDATED_MESSAGE, OnGroupRemoteUpdated);
        MessagePump.instance.Register(this, MessageType.GROUP_MOVING_ALONG_PATH_MESSAGE, OnGroupMovingAlongPath);
        MessagePump.instance.Register(this, MessageType.GROUP_COLLISION_MESSAGE, OnGroupCollision);
    }

    private void OnGroupRemoteUpdated(Message msg)
    {
        GroupRemoteUpdatedMessage cMsg = (GroupRemoteUpdatedMessage)msg;
        if (!groups.ContainsKey(cMsg.groupID))
        {
            groups.Add(cMsg.groupID, GroupStore.instance.GetGroup(cMsg.groupID));
        }
    }

    private void OnGroupMovingAlongPath(Message msg)
    {
        GroupMovingAlongPathMessage cMsg = (GroupMovingAlongPathMessage)msg;

        if(!animators.ContainsKey(cMsg.groupID))
        {
            animators.Add(cMsg.groupID, new PathAnimator(groups[cMsg.groupID]));
            animators[cMsg.groupID].OnMoveComplete.AddListener(() => { OnPathAnimatorComplete(cMsg.groupID); });
        }
        animators[cMsg.groupID].FollowPath(cMsg.path);
    }

    private void OnGroupCollision(Message msg)
    {
        GroupCollisionMessage cMsg = (GroupCollisionMessage)msg;

        if(animators.ContainsKey(cMsg.groupID))
        {
            animators.Remove(cMsg.groupID);
        }
    }

    private void OnPathAnimatorComplete(string id)
    {
        toRemove.Add(id);
    }

    private void Update()
    {
        foreach(PathAnimator p in animators.Values)
        {
            p.Update();
        }

        foreach(string s in toRemove)
        {
            animators.Remove(s);
        }
        toRemove.Clear();
    }
}
