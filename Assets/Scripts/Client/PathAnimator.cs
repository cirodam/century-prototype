﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PathAnimator 
{
    public GroupRemote remote;
    public Queue<MapPos> remaining;
    MapPos current;

    bool started;

    public UnityEvent OnMoveComplete;

    public PathAnimator(GroupRemote remoteArg)
    {
        OnMoveComplete = new UnityEvent();
        remote = remoteArg;
    }

    public void FollowPath(List<MapPos> path)
    {
        started = true;
        remaining = new Queue<MapPos>();
        foreach (MapPos m in path)
        {
            remaining.Enqueue(m);
        }
    }

    public void Update()
    {
        if (started)
        {
            if (current == null)
            {
                if (remaining.Count > 0)
                {
                    current = remaining.Dequeue();
                }
                else
                {
                    started = false;
                    OnMoveComplete.Invoke();
                }
            }
            else
            {
                if (Vector2.Distance(remote.GetVectorPos(), WorldRenderer.instance.GetPositionOfTile(current)) >= 0.01f)
                {
                    remote.SetVectorPos(Vector2.MoveTowards(remote.GetVectorPos(), WorldRenderer.instance.GetPositionOfTile(current), 0.01f));
                }
                else
                {
                    current = null;
                }
            }
        }
    }
}
