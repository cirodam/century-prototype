﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IBuildingDetailPanelController 
{
    void Show(BuildingRemote building);
    void OnNewDay();
    void OnBuildingRemoteUpdated(string buildingID);
    Button GetBackButton();
    Button GetDestroyButton();
}
