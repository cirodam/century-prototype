﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HouseDetailPanelController : MonoBehaviour, IBuildingDetailPanelController
{
    public TextMeshProUGUI upkeepValText;
    public TextMeshProUGUI housingValText;

    public Button backButton;
    public Button destroyButton;

    public Button GetBackButton()
    {
        return backButton;
    }

    public Button GetDestroyButton()
    {
        return destroyButton;
    }

    public void OnBuildingRemoteUpdated(string buildingID)
    {
        throw new System.NotImplementedException();
    }

    public void OnNewDay()
    {
        
    }

    public void Show(BuildingRemote building)
    {
        housingValText.text = building.GetInt("Housing").ToString();
        upkeepValText.text = building.GetUpkeep().ToString();
    }
}
