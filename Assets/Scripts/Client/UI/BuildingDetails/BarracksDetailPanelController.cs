﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BarracksDetailPanelController : MonoBehaviour, IBuildingDetailPanelController
{
    public TextMeshProUGUI progressText;
    public TMP_InputField input;
    public Button backButton;
    public Button destroyButton;

    Barracks barracks;

    public Button GetBackButton()
    {
        return backButton;
    }

    public void Show(BuildingRemote building)
    {
        //barracks = (Barracks) building;

        //progressText.text = "Progress " + barracks.GetProgress()*100 + "%";
        //input.text = barracks.minimum.ToString();
    }

    public void OnMinimumInputChanged()
    {
        barracks.minimum = int.Parse(input.text);
    }

    public void OnNewDay()
    {
        progressText.text = "Progress " + barracks.GetProgress()*100 + "%";
    }

    public Button GetDestroyButton()
    {
        return destroyButton;
    }

    public void OnBuildingRemoteUpdated(string buildingID)
    {
        throw new System.NotImplementedException();
    }
}
