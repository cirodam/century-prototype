﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProducerDetailPanelController : MonoBehaviour, IBuildingDetailPanelController
{
    public GameObject panel;
    public Button BackButton;
    public Button destroyButton;

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI employeeText;
    public TextMeshProUGUI efficiencyText;
    public TextMeshProUGUI LevelText;

    public Dropdown dropdown;

    public GameObject listPrefab;
    public GameObject inputListRoot;
    public GameObject outputListRoot;

    List<GameObject> inputList;
    List<GameObject> outputList;

    BuildingRemote remote;

    string buildingName;
    string prodName;

    int currentIndex;

    private void Awake()
    {
        inputList = new List<GameObject>();
        outputList = new List<GameObject>();
    }

    public void Show(BuildingRemote b)
    {
        panel.SetActive(true);

        remote = b;
        buildingName = remote.GetBuildingName();
        //prodName = remote.GetValue<string>("ProductionName");

        nameText.text = buildingName;

        int i = 0;
        /*foreach (string s in BuildingData.instance.GetProducerDefinition(buildingName).productionOptions)
        {
            dropdown.options.Add(new Dropdown.OptionData(s));
            if(s == prodName)
            {
                currentIndex = i;
            }
            i++;
        }*/

        dropdown.value = currentIndex;

        //employeeText.text = "Employees: " + building.currentJobs + "/" + building.maxJobs;
        //LevelText.text = "Level: " + remote.GetValue<int>("BuildingLevel");
        //efficiencyText.text = "Efficiency: " + building.efficiency.ToString("F2") + "%";

        RefreshList();
    }

    public void RefreshList()
    {
        ClearList();

        ResourceDefinition r = ResourceDataController.instance.GetDataForResource(prodName);

        foreach (ResourceSlot s in r.slots)
        {
            GameObject g = Instantiate(listPrefab);
            g.transform.SetParent(inputListRoot.transform);
            g.GetComponent<ResourceListEntry>().resourceImage.sprite = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(s.resource.resourceName).iconPath);
            g.GetComponent<ResourceListEntry>().resourceAmount.text = s.resource.resourceAmount.ToString();
            inputList.Add(g);
        }

        GameObject o = Instantiate(listPrefab);
        o.transform.SetParent(outputListRoot.transform);
        o.GetComponent<ResourceListEntry>().resourceImage.sprite = Resources.Load<Sprite>(ResourceDataController.instance.GetDataForResource(r.resourceName).iconPath);
        o.GetComponent<ResourceListEntry>().resourceAmount.text = "1";
        outputList.Add(o);
    }

    private void ClearList()
    {
        foreach (GameObject g in inputList)
        {
            Destroy(g);
        }
        inputList.Clear();

        foreach (GameObject g in outputList)
        {
            Destroy(g);
        }
        outputList.Clear();
    }

    public Button GetBackButton()
    {
        return BackButton;
    }

    public void OnDropdownValueChanged(int value)
    {
        currentIndex = dropdown.value;
        //string newProd = BuildingData.instance.GetProducerDefinition(buildingName).productionOptions[currentIndex];
        //MessagePump.instance.QueueMsg(new ClientChangeBuildingValueIntent(GameClient.instance.GetID(), remote.GetValue<string>("CityID"), remote.GetValue<string>("BuildingID"), "ProductionName", newProd));
    }

    public void OnUpgradeButton()
    {
        //MessagePump.instance.QueueMsg(new ClientUpgradeBuildingIntent(GameClient.instance.GetID(), remote.GetValue<string>("CityID"), remote.GetValue<string>("BuildingID")));
    }

    private void MakePurchase(List<ResourceAmount> resources)
    {
        //Stockpile s = ViewController.instance.targetCity.owner.stockpile;
        //foreach (ResourceAmount r in resources)
        // {
        //    s.AddResourceAmount(r.resourceName, -r.resourceAmount);
        //}
    }

    private bool CanAfford(List<ResourceAmount> resources)
    {
        //Stockpile s = ViewController.instance.targetCity.owner.stockpile;

        //foreach (ResourceAmount r in resources)
        //{
        //    if (s.GetResourceAmount(r.resourceName) < r.resourceAmount)
        //    {
        //        return false;
        //    }
        //}
        return true;
    }

    public void OnBuildingRemoteUpdated(string buildingID)
    {
        /*if(buildingID == remote.GetValue<string>("BuildingID"))
        {
            nameText.text = buildingName;

            int i = 0;
            foreach (string s in BuildingData.instance.GetProducerDefinition(buildingName).productionOptions)
            {
                dropdown.options.Add(new Dropdown.OptionData(s));
                if (s == prodName)
                {
                    currentIndex = i;
                }
                i++;
            }

            dropdown.value = currentIndex;

            //employeeText.text = "Employees: " + building.currentJobs + "/" + building.maxJobs;
            LevelText.text = "Level: " + remote.GetValue<int>("BuildingLevel");
            //efficiencyText.text = "Efficiency: " + building.efficiency.ToString("F2") + "%";

            RefreshList();
        }*/
    }

    public void OnNewDay()
    {

    }

    public Button GetDestroyButton()
    {
        return destroyButton;
    }
}
