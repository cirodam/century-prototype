﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueEntry 
{
    public string key;
    public object val;
    public ValueType type;

    public ValueEntry()
    {

    }

    public ValueEntry(string keyArg, object valArg, ValueType typeArg)
    {
        key = keyArg;
        val = valArg;
        type = typeArg;
    }
}
