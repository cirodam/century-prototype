﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldTile 
{
    public int terrain;
    public int specific;
    public string mappableID;
}
