﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupController : MonoBehaviour
{
    public static GroupController instance;
    public Dictionary<string, IGroup> groups;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        groups = new Dictionary<string, IGroup>();

        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
        MessagePump.instance.Register(this, MessageType.CLIENT_DEFEATED_MESSAGE, OnClientDefeated);
        MessagePump.instance.Register(this, MessageType.STOCKPILE_CREATED_MESSAGE, OnStockpileCreated);
    }

    public IGroup CreateGroup(string clientID, string cityID, bool garrison, GroupTemplate template)
    {
        string id = System.Guid.NewGuid().ToString();

        MapPos pos = CityController.instance.GetCity(cityID).GetWorldPos();
        IStockpile stockpile = StockpileController.instance.GetStockpile(clientID);

        IGroup g = new Group(id, clientID, template.groupName, pos, true, null, template.movementType, stockpile);
        groups.Add(id, g);
        MessagePump.instance.QueueMsg(new GroupCreatedMessage(clientID, id, g.GetTitle(), cityID, g.GetWorldPos(), g.GetMovementType(), g.IsStationed(), new List<ValueEntry>()));

        List<IUnit> units = new List<IUnit>();
        foreach (UnitTemplate u in template.units)
        {
            units.Add(CreateUnit(clientID, id, u));
        }

        return g;
    }

    public void DestroyGroup(string groupID)
    {
        if(groups.ContainsKey(groupID))
        {
            groups.Remove(groupID);
            MessagePump.instance.QueueMsg(new GroupDestroyedMessage(groupID));
        }
    }

    public void DeployGroup(string groupID, string defendableID, MapPos defendablePos)
    {
        if(groups.ContainsKey(groupID))
        {
            groups[groupID].OnDeploy(GetSpawnPosition(defendablePos, groups[groupID].GetMovementType()));
            MessagePump.instance.QueueMsg(new GroupDeployedMessage(groupID, defendableID, groups[groupID].GetWorldPos()));
        }
    }

    public IUnit CreateUnit(string clientID, string groupID, UnitTemplate template)
    {
        IUnit unit = TransformTemplate(template);
        groups[groupID].AddUnit(unit);

        MessagePump.instance.QueueMsg(new UnitCreatedMessage(clientID, groupID, unit.GetID(), unit.GetTitle(), unit.GetCount(), unit.GetOptions()));
        return unit;
    }

    public void DestroyUnit(string groupID, string unitID)
    {
        if(groups.ContainsKey(groupID))
        {
            groups[groupID].RemoveUnit(unitID);
            MessagePump.instance.QueueMsg(new UnitDestroyedMessage(groupID, unitID));
        }
    }

    private void OnNewDay(Message msg)
    {
        foreach (IGroup i in groups.Values)
        {
            i.OnNewDay();
        }
    }

    private void OnStockpileCreated(Message msg)
    {
        StockpileCreatedMessage cMsg = (StockpileCreatedMessage)msg;
        foreach(Group g in GetGroupsOfOwner(cMsg.ownerID))
        {
            //g.SetStockpile(cMsg.stockpile);
        }
    }

    private void OnClientDefeated(Message msg)
    {
        ClientDefeatedMessage cMsg = (ClientDefeatedMessage)msg;

        List<int> toRemove = new List<int>();
        foreach (IGroup group in groups.Values)
        {
            //if (group.GetOwnerID() == cMsg.clientID)
            //{
            //    toRemove.Add(group.GetGroupID());
            //}
        }

        foreach (int i in toRemove)
        {
           // groups.Remove(i);
        }
    }

    private IUnit TransformTemplate(UnitTemplate t)
    {
        string id = System.Guid.NewGuid().ToString();
        switch(t.unitType)
        {
            case UnitType.UNIT_TYPE_BATTALION:
                return new Battalion(id, t);
            case UnitType.UNIT_TYPE_SECTION:
                return new Section(id, t);
            case UnitType.UNIT_TYPE_SHIP:
                return new Ship(id, t);
        }
        return null;
    }

    private List<Group> GetGroupsOfOwner(string ownerID)
    {
        List<Group> ret = new List<Group>();
        foreach (Group g in groups.Values)
        {
            if(g.GetOwnerID() == ownerID)
            {
                ret.Add(g);
            }
        }

        return ret;
    }

    public IGroup GetGroup(string id)
    {
        if (groups.ContainsKey(id))
        {
            return groups[id];
        }
        return null;
    }

    private MapPos GetSpawnPosition(MapPos cityPos, MovementType movement)
    {
        switch(movement)
        {
            case MovementType.MOVEMENT_LAND:
                return WorldController.instance.SampleLandNeighbor(cityPos);

            case MovementType.MOVEMENT_SEA:
                return WorldController.instance.SampleWaterNeighbor(cityPos);
        }

        return new MapPos();
    }
}
