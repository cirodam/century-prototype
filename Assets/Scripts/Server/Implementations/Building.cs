﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building
{
    protected string buildingID;
    protected MapPos pos;
    protected BuildingFacing facing;
    protected string buildingName;
    protected float upkeep;
    protected List<MapPos> normalizedFootprint;
    protected int level;
    protected int jobs;

    public static List<MapPos> GetFootprintForFacing(List<MapPos> normalized, BuildingFacing facing)
    {
        List<MapPos> ret = new List<MapPos>();

        foreach(MapPos m in normalized)
        {
            if(facing == BuildingFacing.FACING_EAST)
            {
                ret.Add(m);
            }
            else if(facing == BuildingFacing.FACING_SOUTH)
            {
                ret.Add(new MapPos(-m.x, m.y));
            }
            else if(facing == BuildingFacing.FACING_WEST)
            {
                ret.Add(new MapPos(-m.y, -m.x));
            }
            else if(facing == BuildingFacing.FACING_NORTH)
            {
                ret.Add(new MapPos(m.x, -m.y));
            }
        }
        return ret;
    }

    public static List<MapPos> GetGroundedFootprint(List<MapPos> starting, MapPos pos)
    {
        List<MapPos> grounded = new List<MapPos>();

        foreach(MapPos m in starting)
        {
            grounded.Add(new MapPos(pos.x + m.x, pos.y + m.y));
        }

        return grounded;
    }

    public static List<MapPos> GetTrueFootprint(List<MapPos> normalized, BuildingFacing facing, MapPos origin)
    {
        return GetGroundedFootprint(GetFootprintForFacing(normalized, facing), origin);
    }
}
