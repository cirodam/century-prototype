﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FortCamp : Camp, ICamp
{
    string garrison;

    public FortCamp(string idArg, string ownerArg)
    {
        campID = idArg;
        ownerID = ownerArg;
    }

    public bool AddGarrison(string id)
    {
        if(garrison == null)
        {
            garrison = id;
            return true;
        }
        return false;
    }

    public IGroup GetDefenders()
    {
        return GroupController.instance.GetGroup(garrison);
    }

    public List<string> GetGarrisons()
    {
        return new List<string>() { garrison };
    }

    public string GetID()
    {
        return campID;
    }

    public string GetOwnerID()
    {
        return ownerID;
    }

    public string GetTitle()
    {
        return title;
    }

    public MapPos GetWorldPos()
    {
        return worldPos;
    }

    public bool IsDestroyedOnCapture()
    {
        return false;
    }

    public void OnCapture(string ownerIDArg)
    {

    }

    public void OnNewDay()
    {

    }

    public bool RemoveGarrison(string id)
    {
        garrison = null;
        return true;
    }
}
