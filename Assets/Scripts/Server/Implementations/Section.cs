﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Section : Unit, IUnit
{
    public int currentSoldiers;
    public int currentWeapons;

    public int maxSoldiers;
    public int maxWeapons;

    public Section(string idArg, UnitTemplate t)
    {
        unitID = idArg;

        maxSoldiers         = t.soldierMax;
        maxWeapons          = t.weaponMax;

        soldierResourceName = t.soldierName;
        weaponResourceName  = t.weaponName;

        weaponAttackStrength = ResourceDataController.instance.GetDataForResource(t.weaponName).GetFloat("AttackStrength");
    }

    public float GetAttackCooldown()
    {
        throw new System.NotImplementedException();
    }

    public float GetAttackStrength()
    {
        return weaponAttackStrength * currentWeapons;
    }

    public int GetCount()
    {
        return count;
    }

    public float GetDefense()
    {
        return defense;
    }

    public float GetHealth()
    {
        return 0.0f;
    }

    public string GetID()
    {
        return unitID;
    }

    public float GetMoveSpeed()
    {
        return moveSpeed;
    }

    public List<ValueEntry> GetOptions()
    {
        return new List<ValueEntry>();
    }

    public string GetSoldierResourceName()
    {
        return soldierResourceName;
    }

    public string GetTitle()
    {
        return title;
    }

    public UnitType GetUnitType()
    {
        return UnitType.UNIT_TYPE_SECTION;
    }

    public string GetWeaponResourceName()
    {
        return weaponResourceName;
    }

    public void OnAddAsGarrison()
    {
        throw new System.NotImplementedException();
    }

    public void OnDamage(float damage)
    {

    }

    public void OnNewDay(IStockpile s)
    {

    }

    public void OnRemoveAsGarrison()
    {
        throw new System.NotImplementedException();
    }
}
