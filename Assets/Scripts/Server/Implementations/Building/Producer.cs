﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Producer : Building, IBuilding
{
    public string productionName;

    public Producer(string idArg, string buildingNameArg, MapPos posArg, BuildingFacing facingArg, float upkeepArg, int jobsArg, string productionNameArg, float productionMultiplier)
    {
        buildingID = idArg;
        buildingName = buildingNameArg;
        pos = posArg;
        facing = facingArg;
        upkeep = upkeepArg;
        jobs = jobsArg;

        productionName = productionNameArg;
    }

    public string GetBuildingName()
    {
        return buildingName;
    }

    public string GetBuildingType()
    {
        return "Producer";
    }

    public BuildingFacing GetFacing()
    {
        return facing;
    }

    public float GetGDP()
    {
        return ResourceDataController.instance.GetDataForResource(productionName).value;
    }

    public string GetID()
    {
        return buildingID;
    }

    public int GetLevel()
    {
        return level;
    }

    public MapPos GetPos()
    {
        return pos;
    }

    public float GetUpkeep()
    {
        return upkeep;
    }

    public void OnAdd(ICity city, IStockpile stockpile)
    {
        city.AddJobs(jobs);
    }

    public void OnNewDay(ICity city, IStockpile stockpile)
    {

    }

    public void OnRemove(ICity city, IStockpile stockpile)
    {
        city.SubtractJobs(jobs);
    }

    public void OnUpgrade()
    {

    }
}
