﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barracks : Building, IBuilding
{
    float progress;
    float progressDelta = 0.5f;
    bool citizen;
    public int minimum;

    public Barracks(string newID, MapPos newPos, BuildingFacing newFacing, float newUpkeep, int jobsArg)
    {
        buildingID = newID;
        pos = newPos;
        facing = newFacing;
        upkeep = newUpkeep;
        jobs = jobsArg;
    }

    public string GetID()
    {
        return buildingID;
    }

    public string GetBuildingName()
    {
        return "Barracks";
    }

    public float GetProgress()
    {
        return progress;
    }

    public BuildingFacing GetFacing()
    {
        return facing;
    }

    public MapPos GetPos()
    {
        return pos;
    }

    public void OnAdd(ICity city, IStockpile stockpile)
    {
        city.AddJobs(jobs);
    }

    public void OnNewDay(ICity city, IStockpile stockpile)
    {
        if(city.GetCitizenCount("Workers") > 0)
        {
            city.SubtractCitizens("Workers", 1);
            city.AddCitizens("Soldiers", 1);
        }
    }

    public void OnRemove(ICity city, IStockpile stockpile)
    {
        city.SubtractJobs(jobs);
    }

    public float GetUpkeep()
    {
        return upkeep;
    }

    public List<MapPos> GetFootPrint()
    {
        throw new System.NotImplementedException();
    }

    public void OnUpgrade()
    {
        throw new System.NotImplementedException();
    }

    public int GetLevel()
    {
        return level;
    }

    public string GetBuildingType()
    {
        return "Barracks";
    }

    public float GetGDP()
    {
        return 0;
    }
}
