﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : Building, IBuilding
{
    int housing;

    public House(string newID, MapPos newPos, BuildingFacing newFacing, float newUpkeep, int jobsArg)
    {
        housing      = 50;
        buildingID   = newID;
        buildingName = "House";
        pos          = newPos;
        facing       = newFacing;
        upkeep       = newUpkeep;
        jobs         = jobsArg;
    }

    public string GetID()
    {
        return buildingID;
    }

    public string GetBuildingName()
    {
        return buildingName;
    }

    public MapPos GetPos()
    {
        return pos;
    }

    public void OnAdd(ICity city, IStockpile stockpile)
    {
        city.AddHousing(housing);
        city.AddJobs(jobs);
    }

    public void OnNewDay(ICity city, IStockpile stockpile)
    {
        stockpile.SubtractResourceAmount("Gold", upkeep);
    }

    public void OnRemove(ICity city, IStockpile stockpile)
    {
        city.SubtractHousing(housing);
        city.SubtractJobs(jobs);
    }

    public int GetHousing()
    {
        return housing;
    }

    public BuildingFacing GetFacing()
    {
        return facing;
    }

    public float GetUpkeep()
    {
        return upkeep;
    }

    public void OnUpgrade()
    {
        throw new System.NotImplementedException();
    }

    public int GetLevel()
    {
        return level;
    }

    public string GetBuildingType()
    {
        return "House";
    }

    public float GetGDP()
    {
        return 0;
    }
}
