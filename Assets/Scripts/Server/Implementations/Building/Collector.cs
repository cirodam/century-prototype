﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : Building, IBuilding
{
    string resourceName;
    float resourceAmount;

    public Collector(string idArg, string buildingNameArg, MapPos posArg, BuildingFacing facingArg, float upkeepArg, int jobsArg, string resourceNameArg, int resourceAmountArg)
    {
        buildingID = idArg;
        buildingName = buildingNameArg;
        pos = posArg;
        facing = facingArg;
        upkeep = upkeepArg;
        jobs = jobsArg;

        resourceName = resourceNameArg;
        resourceAmount = resourceAmountArg;
    }

    public string GetBuildingName()
    {
        return buildingName;
    }

    public string GetBuildingType()
    {
        return "Collector";
    }

    public BuildingFacing GetFacing()
    {
        return facing;
    }

    public float GetGDP()
    {
        return resourceAmount * ResourceDataController.instance.GetDataForResource(resourceName).value;
    }

    public string GetID()
    {
        return buildingID;
    }

    public int GetLevel()
    {
        return level;
    }

    public MapPos GetPos()
    {
        return pos;
    }

    public float GetUpkeep()
    {
        return upkeep;
    }

    public void OnAdd(ICity city, IStockpile stockpile)
    {
        city.AddJobs(jobs);
    }

    public void OnNewDay(ICity city, IStockpile stockpile)
    {
        stockpile.AddResourceAmount(resourceName, resourceAmount);
    }

    public void OnRemove(ICity city, IStockpile stockpile)
    {
        city.SubtractJobs(jobs);
    }

    public void OnUpgrade()
    {

    }
}
