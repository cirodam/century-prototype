﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City : ICity
{
    private string cityID;
    private string ownerID;
    private string title;
    private MapPos worldPos;

    private int cityWidth;
    private int cityHeight;

    private Dictionary<string, IBuilding> buildings;
    private Dictionary<string, int> citizens;
    private List<string> garrisons;

    IStockpile stockpile;
    bool coastal;

    private int housingCount;
    private CityTile[,] cityMap;

    bool lastFood;
    private float foodTrend;
    private float citizenProgress;

    private float foodSecurity;
    private float safety;
    private float prosperity;
    private float happiness;

    private int jobCount;

    public City(string idArg, string titleArg, string ownerIDArg, IStockpile stockpileArg, MapPos worldPosArg, bool coastalArg)
    {
        cityID    = idArg;
        ownerID   = ownerIDArg;
        title     = titleArg;
        stockpile = stockpileArg;
        worldPos  = worldPosArg;

        cityWidth = 40;
        cityHeight = 40;

        coastal = coastalArg;

        garrisons = new List<string>();
        buildings = new Dictionary<string, IBuilding>();
        citizens  = new Dictionary<string, int>();
        cityMap   = new CityTile[cityWidth, cityHeight];

        citizens.Add("Workers", 0);
        citizens.Add("Soldiers", 0);

        for(int i=0; i<cityWidth; i++)
        {
            for(int e=0; e<cityHeight; e++)
            {
                cityMap[i, e] = new CityTile();
                if(coastal && e > 24)
                {
                    cityMap[i, e].terrain = 0;
                }
                if(i < 15 ||  i > 23 || e < 15 || e > 23)
                {
                    cityMap[i, e].locked = true;
                }
            }
        }
    }

    public void Init()
    {

    }

    public string GetID()
    {
        return cityID;
    }

    public string GetOwnerID()
    {
        return ownerID;
    }

    public string GetTitle()
    {
        return title;
    }

    public MapPos GetWorldPos()
    {
        return worldPos;
    }

    public bool AddBuilding(IBuilding b)
    {
        if(!buildings.ContainsKey(b.GetID()))
        {
            buildings.Add(b.GetID(), b);
            cityMap[b.GetPos().x, b.GetPos().y].buildingID = b.GetID();
            b.OnAdd(this, stockpile);
            return true;
        }
        return false;
    }

    public bool RemoveBuilding(string buildingID)
    {
        if (buildings.ContainsKey(buildingID))
        {
            buildings[buildingID].OnRemove(this, stockpile);
            buildings.Remove(buildingID);
            return true;
        }
        return false;
    }

    public IBuilding GetBuilding(string buildingID)
    {
        if(buildings.ContainsKey(buildingID))
        {
            return buildings[buildingID];
        }
        return null;
    }

    public IBuilding GetBuildingAtPosition(MapPos pos)
    {
        if(cityMap[pos.x, pos.y].IsOccupied())
        {
            return buildings[cityMap[pos.x, pos.y].buildingID];
        }
        return null;
    }

    public List<IBuilding> GetBuildings()
    {
        return new List<IBuilding>(buildings.Values);
    }

    public void OnNewDay()
    {
        Resupply();
        GrowthCheck();
        foreach(IBuilding b in buildings.Values)
        {
            b.OnNewDay(this, stockpile);
        }
    }

    public void OnStockpileCreated(string id)
    {
        if(stockpile == null && id == ownerID)
        {
            stockpile = StockpileController.instance.GetStockpile(id);
        }
    }

    public bool AddGarrison(string id)
    {
        if(!garrisons.Contains(id))
        {
            garrisons.Add(id);
            MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "AddGarrison", id, ValueType.VALUE_TYPE_STRING));
            return true;
        }
        return false;
    }

    public bool RemoveGarrison(string id)
    {
        if(garrisons.Contains(id))
        {
            garrisons.Remove(id);
            MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "RemoveGarrison", id, ValueType.VALUE_TYPE_STRING));
            return true;
        }
        return false;
    }

    public List<string> GetGarrisons()
    {
        return garrisons;
    }

    public IGroup GetDefenders()
    {
        return null;
    }

    public CityTile[,] GetCityMap()
    {
        return cityMap;
    }

    public bool IsCoastal()
    {
        return coastal;
    }

    public bool IsPositionLocked(MapPos pos)
    {
        return cityMap[pos.x, pos.y].IsLocked();
    }

    public bool IsPositionOccupied(MapPos pos)
    {
        return cityMap[pos.x, pos.y].IsOccupied();
    }

    public MapPos SampleRandomPosition(bool locked, bool occupied)
    {
        return new MapPos();
    }

    public int GetCityHeight()
    {
        return cityHeight;
    }

    public int GetCityWidth()
    {
        return cityWidth;
    }

    public void AddHousing(int amount)
    {
        housingCount += amount;
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "Housing", housingCount, ValueType.VALUE_TYPE_INT));
    }

    public void SubtractHousing(int amount)
    {
        housingCount -= amount;
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "Housing", housingCount, ValueType.VALUE_TYPE_INT));
    }

    public int GetHousingCount()
    {
        return housingCount;
    }

    public int GetCitizenCount()
    {
        int count = 0;
        foreach (string s in citizens.Keys)
        {
            count += citizens[s];
        }
        return count;
    }

    private void Resupply()
    {
        if(stockpile.HasResourceAmount("Wheat", GetCitizenCount()))
        {
            if(GetFoodSecurity() < 0)
            {
                foodTrend = 0;
            }
            else
            {
                foodTrend++;
            }
            stockpile.SubtractResourceAmount("Wheat", GetCitizenCount());
        }
        else
        {
            if(GetFoodSecurity() > 0)
            {
                foodTrend = 0;
            }
            else
            {
                foodTrend++;
            }
            stockpile.SubtractResourceAmount("Wheat", stockpile.GetResourceAmount("Wheat"));
        }

        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "Food Security", GetFoodSecurity(), ValueType.VALUE_TYPE_FLOAT));
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "GDP", GetGDP(), ValueType.VALUE_TYPE_FLOAT));
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "Prosperity", GetProsperity(), ValueType.VALUE_TYPE_FLOAT));

    }

    private void GrowthCheck()
    {
        if(housingCount >= GetCitizenCount())
        {
            citizenProgress += GetFoodSecurity() + GetProsperity();
            if (citizenProgress >= 1)
            {
                int toAdd = Mathf.FloorToInt(citizenProgress);
                citizenProgress -= toAdd;
                OnNewCitizens(toAdd);
            }
        }
    }

    private void OnNewCitizens(int Count)
    {
        citizens["Workers"] += Count;
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "Citizens", GetCitizenCount(), ValueType.VALUE_TYPE_INT));
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "Workers", citizens["Workers"], ValueType.VALUE_TYPE_INT));
    }

    public void OnCapture(string ownerIDArg)
    {
        ownerID = ownerIDArg;
        MessagePump.instance.QueueMsg(new ServerStringValueUpdatedMessage(cityID, "OwnerID", ownerID));
    }

    public void AddCitizens(string profession, int count)
    {
        citizens[profession] += count;
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, profession, citizens[profession], ValueType.VALUE_TYPE_INT));
    }

    public void SubtractCitizens(string profession, int count)
    {
        citizens[profession] -= count;
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, profession, citizens[profession], ValueType.VALUE_TYPE_INT));
    }

    private float GetFoodSecurity()
    {
        return foodTrend * 0.05f;
    }

    private float GetProsperity()
    {
        float GDP = (GetGDP() / 100.0f) * 0.05f;

        return GDP;
    }

    private float GetGDP()
    {
        float sum = 0.0f;
        foreach(IBuilding b in buildings.Values)
        {
            sum += b.GetGDP();
        }
        return sum;
    }

    public void AddJobs(int amount)
    {
        jobCount += amount;
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "Jobs", jobCount, ValueType.VALUE_TYPE_INT));
    }

    public void SubtractJobs(int amount)
    {
        jobCount -= amount;
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(cityID, "Jobs", jobCount, ValueType.VALUE_TYPE_INT));
    }

    public int GetCitizenCount(string profession)
    {
        if(citizens.ContainsKey(profession))
        {
            return citizens[profession];
        }
        return 0;
    }
}
