﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Group : IGroup
{
    private string groupID;
    private string ownerID;
    private string title;

    private Dictionary<string, IUnit> units;

    private MapPos worldPos;
    private Vector2 vectorPos;

    private IStockpile stockpile;
    private MovementType movementType;
    private bool stationed;
    private bool provisioning;

    public Group(string idArg, string ownerIDArg, string titleArg, MapPos worldPosArg, bool garrisonArg, List<IUnit> unitsArg, MovementType movementArg, IStockpile stockpileArg)
    {
        groupID      = idArg;
        ownerID      = ownerIDArg;
        title        = titleArg;
        worldPos     = worldPosArg;
        stockpile    = stockpileArg;
        movementType = movementArg;
        vectorPos    = WorldController.instance.GetPosOfTile(worldPos);
        stationed    = garrisonArg;
        provisioning = true;

        units = new Dictionary<string, IUnit>();
        if(unitsArg != null)
        {
            foreach (IUnit u in unitsArg)
            {
                AddUnit(u);
            }
        }
    }

    public bool AddUnit(IUnit unit)
    {
        if(!units.ContainsKey(unit.GetID()))
        {
            units.Add(unit.GetID(), unit);
            return true;
        }
        return false;
    }

    public string GetID()
    {
        return groupID;
    }

    public MovementType GetMovementType()
    {
        return movementType;
    }

    public string GetOwnerID()
    {
        return ownerID;
    }

    public float GetStrength()
    {
        return 0;
    }

    public string GetTitle()
    {
        return title;
    }

    public List<IUnit> GetUnits()
    {
        return new List<IUnit>(units.Values);
    }

    public bool IsStationed()
    {
        return stationed;
    }

    public void OnStation(string defendableID, MapPos pos)
    {
        stationed = true;
        MapPos old = worldPos;
        worldPos = pos;
        vectorPos = WorldController.instance.GetPosOfTile(worldPos);

        MessagePump.instance.QueueMsg(new GroupMapPosUpdatedMessage(groupID, old, null));
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(groupID, "Garrison", stationed, ValueType.VALUE_TYPE_BOOL));
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(groupID, "WorldPos", worldPos, ValueType.VALUE_TYPE_MAPPOS));
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(groupID, "VectorPos", vectorPos, ValueType.VALUE_TYPE_VECTOR2));
    }

    public void OnDeploy(MapPos pos)
    {
        stationed = false;
        worldPos = pos;
        vectorPos = WorldController.instance.GetPosOfTile(worldPos);

        MessagePump.instance.QueueMsg(new GroupMapPosUpdatedMessage(groupID, null, pos));
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(groupID, "Garrison", stationed, ValueType.VALUE_TYPE_BOOL));
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(groupID, "WorldPos", worldPos, ValueType.VALUE_TYPE_MAPPOS));
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(groupID, "VectorPos", vectorPos, ValueType.VALUE_TYPE_VECTOR2));
    }

    public void OnNewDay()
    {
        foreach (IUnit u in units.Values)
        {
            u.OnNewDay(stockpile);
        }
    }

    public bool RemoveUnit(string id)
    {
        if(units.ContainsKey(id))
        {
            units.Remove(id);
            return true;
        }
        return false;
    }

    public void SetVectorPos(Vector2 posArg)
    {
        vectorPos = posArg;
    }

    public void SetWorldPos(MapPos posArg)
    {
        worldPos = posArg;
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(groupID, "WorldPos", worldPos, ValueType.VALUE_TYPE_MAPPOS));
    }

    public Vector2 GetVectorPos()
    {
        return vectorPos;
    }

    public MapPos GetWorldPos()
    {
        return worldPos;
    }

    public IUnit GetUnit(string id)
    {
        if(units.ContainsKey(id))
        {
            return units[id];
        }
        return null;
    }

    public bool IsProvisioning()
    {
        return provisioning;
    }
}
