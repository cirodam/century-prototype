﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : Building, IBuilding
{
    public Road(string newID, MapPos newPos, BuildingFacing newFacing, float newUpkeep)
    {
        buildingID = newID;
        pos = newPos;
        facing = newFacing;
        upkeep = newUpkeep;
    }

    public string GetID()
    {
        return buildingID;
    }

    public string GetBuildingName()
    {
        return "Road";
    }

    public BuildingFacing GetFacing()
    {
        return facing;
    }

    public List<MapPos> GetFootPrint()
    {
        throw new System.NotImplementedException();
    }

    public MapPos GetPos()
    {
        return pos;
    }

    public float GetUpkeep()
    {
        return upkeep;
    }

    public void OnAdd(ICity city, IStockpile stockpile)
    {

    }

    public void OnNewDay(ICity city, IStockpile stockpile)
    {

    }

    public void OnRemove(ICity city, IStockpile stockpile)
    {

    }

    public void OnUpgrade()
    {
        throw new System.NotImplementedException();
    }

    public int GetLevel()
    {
        return level;
    }

    public string GetBuildingType()
    {
        return "Road";
    }

    public float GetGDP()
    {
        return 0;
    }
}
