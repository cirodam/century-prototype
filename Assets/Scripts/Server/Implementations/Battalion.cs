﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battalion : Unit, IUnit
{
    public int currentSoldiers;
    public int currentWeapons;

    public int maxSoldiers;
    public int maxWeapons;

    int resupplyRate = 4;

    int soldierHealth = 20;

    int maxHealth;
    int currentHealth;

    float attackCooldown;

    public Battalion(string idArg, UnitTemplate t)
    {
        unitID = idArg;

        maxSoldiers         = t.soldierMax;
        maxWeapons          = t.weaponMax;
        soldierResourceName = t.soldierName;
        weaponResourceName  = t.weaponName;
        title = t.unitName;

        maxHealth = maxSoldiers * soldierHealth;
        weaponAttackStrength = ResourceDataController.instance.GetDataForResource(weaponResourceName).GetFloat("AttackStrength");

        if(ResourceDataController.instance.GetDataForResource(weaponResourceName).resourceRole == "Vehicle")
        {
            moveSpeed += 0.4f;
        }
    }

    public float GetAttackCooldown()
    {
        return attackCooldown;
    }

    public float GetAttackStrength()
    {
        //return weaponAttackStrength * Mathf.Min(currentSoldiers, currentWeapons);
        return weaponAttackStrength * currentSoldiers;
    }

    public int GetCount()
    {
        return count;
    }

    public float GetDefense()
    {
        return defense;
    }

    public float GetHealth()
    {
        return currentHealth/maxHealth;
    }

    public string GetID()
    {
        return unitID;
    }

    public float GetMoveSpeed()
    {
        return moveSpeed;
    }

    public List<ValueEntry> GetOptions()
    {
        ValueEntry v = new ValueEntry("CurrentSoldiers", 0, ValueType.VALUE_TYPE_INT);
        ValueEntry w = new ValueEntry("AttackStrength", 0.0f, ValueType.VALUE_TYPE_FLOAT);
        ValueEntry x = new ValueEntry("Health", 0.0f, ValueType.VALUE_TYPE_FLOAT);
        ValueEntry y = new ValueEntry("WeaponName", weaponResourceName, ValueType.VALUE_TYPE_STRING);
        return new List<ValueEntry>() { v, w, x, y};
    }

    public string GetSoldierResourceName()
    {
        return soldierResourceName;
    }

    public string GetTitle()
    {
        return title;
    }

    public UnitType GetUnitType()
    {
        return UnitType.UNIT_TYPE_BATTALION;
    }

    public string GetWeaponResourceName()
    {
        return weaponResourceName;
    }

    public void OnAddAsGarrison()
    {
        throw new System.NotImplementedException();
    }

    public void OnDamage(float damage)
    {
        currentHealth -= (int) damage;
    }

    public void OnNewDay(IStockpile s)
    {
        float toResupply = Mathf.Min(maxSoldiers - currentSoldiers, resupplyRate, s.GetResourceAmount("Soldier"));
        if(toResupply > 0)
        {
            s.SubtractResourceAmount("Soldier", toResupply);
            currentSoldiers += (int)toResupply;
            currentHealth = currentSoldiers * soldierHealth;
            MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(unitID, "CurrentSoldiers", currentSoldiers, ValueType.VALUE_TYPE_INT));
            MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(unitID, "AttackStrength", GetAttackStrength(), ValueType.VALUE_TYPE_FLOAT));
            MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(unitID, "Health", (float)currentHealth, ValueType.VALUE_TYPE_INT));
        }

        toResupply = Mathf.Min(maxWeapons - currentWeapons, resupplyRate, s.GetResourceAmount("Sword"));
        if(toResupply > 0)
        {
            s.SubtractResourceAmount("Sword", toResupply);
            currentSoldiers += (int)toResupply;
            MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(unitID, "CurrentWeapons", currentWeapons, ValueType.VALUE_TYPE_INT));
        }
    }

    public void OnRemoveAsGarrison()
    {
        throw new System.NotImplementedException();
    }
}
