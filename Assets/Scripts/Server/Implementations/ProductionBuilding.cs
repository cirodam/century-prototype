﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductionBuilding : Building, IBuilding
{
    List<ProductionOption> productions;
    ProductionOption production;
    int productionIndex;
    int multiplier;

    public ProductionBuilding(string newID, MapPos newPos, BuildingFacing newFacing, string newName, float newUpkeep)
    {
        buildingID = newID;
        pos = newPos;
        buildingName = newName;
        facing = newFacing;
        upkeep = newUpkeep;

        productions = new List<ProductionOption>();
        multiplier = 1;
    }

    public void AddProductionOption(string name, List<ResourceAmount> inputs, ResourceAmount output)
    {
        productions.Add(new ProductionOption(inputs, output));
        production = productions[0];
        productionIndex = 0;
    }

    public void AddProductionOption(ResourceDefinition r)
    {
        List<ResourceAmount> inputs = new List<ResourceAmount>();
        ResourceAmount output = new ResourceAmount(r.resourceName, 1);

        foreach(ResourceSlot s in r.slots)
        {
            inputs.Add(s.resource);
        }
        AddProductionOption(r.resourceName, inputs, output);
    }

    public void SetProduction(int newIndex)
    {
        productionIndex = newIndex;
        production = productions[productionIndex];
    }

    public int GetProductionIndex()
    {
        return productionIndex;
    }

    public ProductionOption GetProduction()
    {
        return production;
    }

    public List<ProductionOption> GetProductionOptions()
    {
        return productions;
    }

    public string GetBuildingName()
    {
        return buildingName;
    }

    public MapPos GetPos()
    {
        return pos;
    }

    public void OnAdd(ICity city, IStockpile stockpile)
    {

    }

    public void OnNewDay(ICity city, IStockpile stockpile)
    {
        bool tick = true;

        foreach (ResourceAmount r in production.inputs)
        {
            if (stockpile.GetResourceAmount(r.resourceName) < r.resourceAmount * multiplier)
            {
                tick = false;
                break;
            }
        }

        if (tick)
        {
            foreach (ResourceAmount r in production.inputs)
            {
                stockpile.SubtractResourceAmount(r.resourceName, r.resourceAmount * multiplier);
            }
        
            stockpile.AddResourceAmount(production.output.resourceName, production.output.resourceAmount * multiplier);
        }
    }

    public void OnRemove(ICity city, IStockpile stockpile)
    {

    }

    public string GetID()
    {
        return buildingID;
    }

    public BuildingFacing GetFacing()
    {
        return facing;
    }

    public float GetUpkeep()
    {
        return upkeep;
    }

    public List<MapPos> GetFootPrint()
    {
        throw new System.NotImplementedException();
    }

    public void OnUpgrade()
    {
        level++;
        multiplier++;
    }

    public int GetLevel()
    {
        return level;
    }

    public string GetBuildingType()
    {
        return "Producer";
    }

    public float GetGDP()
    {
        throw new System.NotImplementedException();
    }
}
