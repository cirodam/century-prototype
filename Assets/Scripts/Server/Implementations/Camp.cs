﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camp
{
    protected string campID;
    protected string ownerID;
    protected MapPos worldPos;
    protected string title;

    protected List<string> garrisons;
    protected Stockpile stockpile;

    protected bool destroyedOnCapture;
    protected int level;
}
