﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : Unit, IUnit
{
    public float hullPoints;
    public float maxHullPoints;

    public int currentSoldiers;
    public int maxSoldiers;

    public Ship(string idArg, UnitTemplate t)
    {
        unitID = idArg;
        title = t.unitName;

        maxSoldiers = t.soldierMax;

        soldierResourceName = t.soldierName;
        weaponResourceName = t.weaponName;

        hullPoints = ResourceDataController.instance.GetDataForResource(t.weaponName).GetFloat("HullPoints");
        maxHullPoints = hullPoints;
        weaponAttackStrength = ResourceDataController.instance.GetDataForResource(t.weaponName).GetFloat("AttackStrength");
    }

    public float GetAttackCooldown()
    {
        throw new System.NotImplementedException();
    }

    public float GetAttackStrength()
    {
        return weaponAttackStrength * (hullPoints/maxHullPoints);
    }

    public int GetCount()
    {
        return count;
    }

    public float GetDefense()
    {
        return defense;
    }

    public float GetHealth()
    {
        return 0.0f;
    }

    public string GetID()
    {
        return unitID;
    }

    public float GetMoveSpeed()
    {
        return moveSpeed;
    }

    public List<ValueEntry> GetOptions()
    {
        ValueEntry a = new ValueEntry("HullPoints", hullPoints, ValueType.VALUE_TYPE_FLOAT);
        ValueEntry b = new ValueEntry("MaxHullPoints", hullPoints, ValueType.VALUE_TYPE_FLOAT);

        return new List<ValueEntry>() { a,b };
    }

    public string GetSoldierResourceName()
    {
        return soldierResourceName;
    }

    public string GetTitle()
    {
        return title;
    }

    public UnitType GetUnitType()
    {
        return UnitType.UNIT_TYPE_SHIP;
    }

    public string GetWeaponResourceName()
    {
        return weaponResourceName;
    }

    public void OnAddAsGarrison()
    {
        throw new System.NotImplementedException();
    }

    public void OnDamage(float damage)
    {
        hullPoints -= damage;
        MessagePump.instance.QueueMsg(new ServerValueUpdatedMessage(unitID, "HullPoints", hullPoints, ValueType.VALUE_TYPE_FLOAT));
    }

    public void OnNewDay(IStockpile s)
    {

    }

    public void OnRemoveAsGarrison()
    {
        throw new System.NotImplementedException();
    }
}
