﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceCamp : Camp, ICamp
{
    private string resourceName;
    private float resourceDelta;

    public ResourceCamp(string idArg, string ownerIdArg, MapPos worldPosArg, string titleArg, string resourceNameArg, float resourceDeltaArg)
    {
        campID        = idArg;
        ownerID       = ownerIdArg;
        worldPos      = worldPosArg;
        resourceName  = resourceNameArg;
        resourceDelta = resourceDeltaArg;
        title         = titleArg;

        garrisons = new List<string>();
        level = 1;
    }

    public bool AddGarrison(string id)
    {
        if(!garrisons.Contains(id))
        {
            garrisons.Add(id);
            return true;
        }
        return false;
    }

    public IGroup GetDefenders()
    {
        return null;
    }

    public List<string> GetGarrisons()
    {
        return garrisons;
    }

    public string GetID()
    {
        return campID;
    }

    public string GetOwnerID()
    {
        return ownerID;
    }

    public string GetTitle()
    {
        return title;
    }

    public MapPos GetWorldPos()
    {
        return worldPos;
    }

    public bool IsDestroyedOnCapture()
    {
        return destroyedOnCapture;
    }

    public void OnCapture(string ownerIDArg)
    {
        ownerID = ownerIDArg;
        level = 1;
        stockpile = StockpileController.instance.GetStockpile(ownerID);

        MessagePump.instance.QueueMsg(new ServerIntValueUpdatedMessage(campID, "", "Level", level));
    }

    public void OnNewDay()
    {
        if(stockpile != null)
        {
            stockpile.AddResourceAmount(resourceName, resourceDelta);
        }
    }

    public bool RemoveGarrison(string id)
    {
        if(garrisons.Contains(id))
        {
            garrisons.Remove(id);
            return true;
        }
        return false;
    }
}
