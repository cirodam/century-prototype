﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit 
{
    protected string unitID;
    protected string title;
    protected int count;

    protected float weaponAttackStrength;
    protected float defense;
    protected float moveSpeed;

    protected string soldierResourceName;
    protected string weaponResourceName;
}
