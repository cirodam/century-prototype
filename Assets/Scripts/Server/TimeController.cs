﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    public static TimeController instance;

    private float deltaTime;
    public float maxTime;

    private int currentDay;
    private int currentMonth;
    private int currentYear;

    bool paused = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if(!paused)
        {
            deltaTime += Time.deltaTime;

            if (deltaTime >= maxTime)
            {
                deltaTime = 0;
                NextDay();
            }
        }
    }

    private void NextDay()
    {
        if(currentDay >= 30)
        {
            currentDay = 0;
            if(currentMonth >= 12)
            {
                currentMonth = 0;
                currentYear++;
                MessagePump.instance.QueueMsg(new NewYearMessage(currentYear));
            }
            else
            {
                currentMonth++;
                MessagePump.instance.QueueMsg(new NewMonthMessage(currentMonth));
            }
        }
        else
        {
            currentDay++;
        }

        MessagePump.instance.QueueMsg(new PreNewDayMessage(currentDay, currentMonth, currentYear));
        MessagePump.instance.QueueMsg(new NewDayMessage(currentDay, currentMonth, currentYear));
        MessagePump.instance.QueueMsg(new PostNewDayMessage(currentDay, currentMonth, currentYear));
    }

    public int GetCurrentDay()
    {
        return currentDay;
    }

    public int GetCurrentMonth()
    {
        return currentMonth;
    }

    public int GetCurrentYear()
    {
        return currentYear;
    }

    public void PauseSimulation()
    {
        paused = true;
        MessagePump.instance.QueueMsg(new SimulationPausedMessage());
    }

    public void UnpauseSimulation()
    {
        paused = false;
        MessagePump.instance.QueueMsg(new SimulationUnpausedMessage());
    }
}
