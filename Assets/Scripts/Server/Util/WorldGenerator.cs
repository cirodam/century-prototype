﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
using Unity.Mathematics;
using System;
using System.Text;

public class WorldGenerator : MonoBehaviour
{
    public static WorldGenerator instance;

    private float seaLvl = 0.7f;
    private int worldSizeX;
    private int worldSizeY;
    private string seed;

    private const int INFERIOR_EQUAL = -2;
    private const int INFERIOR = -1;
    private const int EQUAL = 0;
    private const int SUPERIOR = 1;
    private const int SUPERIOR_EQUAL = 2;

    private static readonly int[] HEIGHT_DIRECTIONS = new int[] { 1, 1, 1, 1, 0, 1, 1, 1, 1 };

    private Random randomSeed;

    private int[,] grid;
    private Dictionary<int, List<Vector2>> patches;

    private Vector2[] translations = {
        new Vector2 (-1, 1),
        new Vector2 (0, 1),
        new Vector2 (1, 1),
        new Vector2 (-1, 0),
        new Vector2 (0, 0),
        new Vector2 (1, 0),
        new Vector2 (-1, -1),
        new Vector2 (0, -1),
        new Vector2 (1, -1)
    };

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public string GetSeed()
    {
        return seed;
    }

    public int[,] GetGrid()
    {
        return grid;
    }

    public Dictionary<int, List<Vector2>> GetPatches()
    {
        return patches;
    }

    public WorldChunk GenerateChunk(MapPos pos)
    {
        WorldChunk chunk = new WorldChunk(pos);
        chunk.grid = GetContinent(pos, 64);

        return chunk;
    }

    public int[,] GetContinent(MapPos pos, int width)
    {
        int[,] map = new int[width,width];
        float seed = randomSeed.Next();

        for(int i=0; i<width; i++)
        {
            for(int e=0; e<width; e++)
            {
                float2 s = new float2((i+seed) / width, (e+seed) / width);

                float f = 0.5f;
                f += AddOctave(s, 1, 2);
                f += AddOctave(s, 0.5f, 8);
                f += AddOctave(s, 0.25f, 12);

                map[(int)(i - seed), (int)(e - seed)] = GetTerrainValue(f, i, e, seed);
            }
        }

        return map;
    }

    public int[,] GenerateWorld(string seed, int newSizeX, int newSizeY, float newSeaLvl)
    {
        patches = new Dictionary<int, List<Vector2>>();

        worldSizeX = newSizeX;
        worldSizeY = newSizeY;
        seaLvl = newSeaLvl;

        ConfigureSeed(seed);
        ResetGrid();

        GenerateContinent();
        GeneratePatches(2, 1, new int[] { 1 }, new int[] { 0 }, 20, 100, 20);

        return grid;
    }

    private float AddOctave(float2 sample, float amplitude, float frequency)
    {
        return amplitude * noise.snoise(sample * frequency);
    }

    private void GenerateContinent()
    {
        patches.Add(1, new List<Vector2>());
        float se = randomSeed.Next(0, worldSizeX);
        Vector2 center = new Vector2(se + 50, se + 50);

        for (float i = se + 1; i < se + worldSizeX - 1; i++)
        {
            for (float j = se + 1; j < se + worldSizeY - 1; j++)
            {
                float2 s = new float2(i / worldSizeX, j / worldSizeY);

                float dist = Mathf.Sqrt(((center.x - i) * (center.x - i)) + ((center.y - j) * (center.y - j)));

                float f = 0.5f;
                f += AddOctave(s, 1, 2);
                f += AddOctave(s, 0.5f, 8);
                f += AddOctave(s, 0.25f, 12);

                f += Mathf.Lerp(1, -0.5f, (dist / 50));

                grid[(int)(i - se), (int)(j - se)] = GetTerrainValue(f, i, j, se);
            }
        }

        RecursiveSmooth(1, 1, worldSizeX - 1, worldSizeY - 1, 1, 0, 1);
    }

    private int GetTerrainValue(float f, float i, float j, float se)
    {
        if (f < seaLvl)
        {
            return 0;
        }

        patches[1].Add(new Vector2((i - se), (j - se)));
        return 1;
    }

    private void GeneratePatches(int value, int smoothWith, int[] placeOn, int[] unnacceptableAdjacent, int minSize, int maxSize, int maxPatches)
    {
        int count = maxPatches;

        while (count > 0)
        {
            Vector2 pos = SampleRandomPosition(1);
            int size = randomSeed.Next(minSize, maxSize);

            GeneratePatch(pos, size, value, placeOn, unnacceptableAdjacent);
            count--;
        }

        RecursiveSmooth(1, 1, worldSizeX - 1, worldSizeY - 1, value, smoothWith, 1);
    }

    private void GeneratePatch(Vector2 pos, int size, int value, int[] placeOn, int[] unnacceptableAdjacent)
    {
        List<Vector2> patch = new List<Vector2>();
        patch.Add(pos);

        while (size > 0)
        {
            size--;
            Vector3 next = putAdjacent(new List<Vector2>(patch), placeOn);

            if (unnacceptableAdjacent.Length > 0)
            {
                bool skip = false;
                foreach (int val in unnacceptableAdjacent)
                {
                    if (TileHasNeighbor((int)next.x, (int)next.y, val) || TileHasAltNeighbor((int)next.x, (int)next.y, val))
                    {
                        skip = true;
                        break;
                    }
                }
                if (skip)
                {
                    continue;
                }
            }

            grid[(int)next.x, (int)next.y] = value;
            grid[(int)next.x, (int)next.y] = 2;
            patch.Add(next);
        }

        if (patch.Count > 0)
        {
            if (!patches.ContainsKey(value))
            {
                patches.Add(value, patch);
            }
            patches[value].AddRange(patch);
        }
    }

    private void SimpleSmooth(int index, int replace, int passes = 1)
    {
        for (int p = 0; p < passes; p++)
        {
            for (int i = 1; i < worldSizeX - 1; i++)
            {
                for (int j = 1; j < worldSizeY - 1; j++)
                {
                    if (grid[i, j] == index)
                    {
                        if (!OpposingNeighborCheck(i, j))
                        {
                            grid[i, j] = replace;
                        }
                    }
                }
            }
        }
    }

    private void RecursiveSmooth(int x, int y, int width, int height, int index, int replace, int passes = 1)
    {
        for (int p = 0; p < passes; p++)
        {
            for (int i = x; i < width; i++)
            {
                for (int j = y; j < height; j++)
                {
                    RecursiveNeighborCheck(i, j, index, replace, 0, 10);
                }
            }
        }
    }

    private void RecursiveNeighborCheck(int i, int j, int index, int replace, int current, int max)
    {
        if (i > 0 && i < worldSizeX - 1 && j > 0 && j < worldSizeY - 1)
        {
            if (grid[i, j] == index)
            {
                if (!OpposingNeighborCheck(i, j))
                {
                    grid[i, j] = replace;

                    if (current < max)
                    {
                        RecursiveNeighborCheck(i, j + 1, index, replace, current + 1, max);
                        RecursiveNeighborCheck(i + 1, j, index, replace, current + 1, max);
                        RecursiveNeighborCheck(i, j - 1, index, replace, current + 1, max);
                        RecursiveNeighborCheck(i - 1, j, index, replace, current + 1, max);
                        RecursiveNeighborCheck(i + 1, j + 1, index, replace, current + 1, max);
                        RecursiveNeighborCheck(i + 1, j - 1, index, replace, current + 1, max);
                        RecursiveNeighborCheck(i - 1, j - 1, index, replace, current + 1, max);
                        RecursiveNeighborCheck(i - 1, j + 1, index, replace, current + 1, max);
                    }
                }
            }
        }
    }

    private bool OpposingNeighborCheck(int i, int j)
    {
        int value = grid[i, j];

        if ((grid[i - 1, j] != value && grid[i + 1, j] != value) ||
            (grid[i, j + 1] != value && grid[i, j - 1] != value) ||
            (grid[i + 1, j + 1] != value && grid[i - 1, j - 1] != value) ||
            (grid[i - 1, j + 1] != value && grid[i + 1, j - 1] != value))
        {
            return false;
        }
        return true;
    }

    private void ConfigureSeed(string s)
    {
        if (s == "RANDOM")
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            s = new String(stringChars);
            seed = s;
        }
        randomSeed = new Random(BitConverter.ToInt32(Encoding.ASCII.GetBytes(seed), 0));
    }

    Vector3 putAdjacent(List<Vector2> tiles, int[] acceptedReplace)
    {
        int pos = randomSeed.Next(0, tiles.Count);

        Vector2[] adjacents = {
            new Vector2 (0, 1),
            new Vector2 (1, 0),
            new Vector2 (0, -1),
            new Vector2 (-1, 0)
        };

        int initialIndex = randomSeed.Next(0, adjacents.Length);
        int index = initialIndex;

        int x = (int)(tiles[pos].x + adjacents[index].x);
        int y = (int)(tiles[pos].y + adjacents[index].y);
        int iter = 0;

        while (Array.IndexOf(acceptedReplace, grid[x, y]) == -1 || x < 3 || y < 3 || x > worldSizeX - 5 || y > worldSizeY - 5)
        {
            x = (int)(tiles[pos].x + adjacents[index].x);
            y = (int)(tiles[pos].y + adjacents[index].y);

            if (index == initialIndex && iter != 0)
            {
                tiles.RemoveAt(pos);

                if (tiles.Count > 0)
                {
                    Vector2 res = putAdjacent(tiles, acceptedReplace);
                    x = (int)res.x;
                    y = (int)res.y;
                    break;
                }
                else
                {
                    return new Vector3(0, 0, 0);
                }
            }

            index += 1;
            if (index >= adjacents.Length)
            {
                index = 0;
            }
            iter++;
        }

        return new Vector3(x, y, pos);
    }

    int hasAdjacent(int[,,] inArray, Vector3 pos, int val, int[] mask = null, int operand = EQUAL)
    {
        mask = mask ?? HEIGHT_DIRECTIONS;

        int found = 0;

        for (int i = 0; i < mask.Length; i++)
        {
            if (mask[i] == 1)
            {
                switch (operand)
                {
                    case INFERIOR_EQUAL:
                        // check if the value is <= to what we're looking for
                        if (inArray[(int)pos.x + (int)translations[i].x, (int)pos.y + (int)translations[i].y, (int)pos.z] <= val)
                        {
                            found++;
                        }
                        break;
                    // <
                    case INFERIOR:
                        // check if the value is < to what we're looking for
                        if (inArray[(int)pos.x + (int)translations[i].x, (int)pos.y + (int)translations[i].y, (int)pos.z] < val)
                        {
                            found++;
                        }
                        break;
                    // ==
                    case EQUAL:
                        // check if the value is == to what we're looking for
                        if (inArray[(int)pos.x + (int)translations[i].x, (int)pos.y + (int)translations[i].y, (int)pos.z] == val)
                        {
                            found++;
                        }
                        break;
                    // >
                    case SUPERIOR:
                        // check if the value is > to what we're looking for
                        if (inArray[(int)pos.x + (int)translations[i].x, (int)pos.y + (int)translations[i].y, (int)pos.z] > val)
                        {
                            found++;
                        }
                        break;
                    // >=
                    case SUPERIOR_EQUAL:
                        // check if the value is >= to what we're looking for
                        if (inArray[(int)pos.x + (int)translations[i].x, (int)pos.y + (int)translations[i].y, (int)pos.z] >= val)
                        {
                            found++;
                        }
                        break;
                }
            }
        }
        return found;
    }

    private void ResetGrid()
    {
        grid = new int[worldSizeX, worldSizeY];
        for (int i = 0; i < worldSizeX; i++)
        {
            for (int j = 0; j < worldSizeY; j++)
            {
                grid[i, j] = 0;
            }
        }
    }

    public Vector2 SampleRandomPosition(int value)
    {
        return patches[value][randomSeed.Next(0, patches[value].Count)];
    }

    private bool TileHasNeighbor(int x, int y, int value)
    {
        if (grid[x, y + 1] == value)
        {
            return true;
        }

        if (grid[x + 1, y] == value)
        {
            return true;
        }

        if (grid[x, y - 1] == value)
        {
            return true;
        }

        if (grid[x - 1, y] == value)
        {
            return true;
        }

        return false;
    }

    private bool TileHasAltNeighbor(int x, int y, int value)
    {
        if (grid[x + 1, y + 1] == value)
        {
            return true;
        }

        if (grid[x + 1, y - 1l] == value)
        {
            return true;
        }

        if (grid[x - 1, y - 1] == value)
        {
            return true;
        }

        if (grid[x - 1, y + 1] == value)
        {
            return true;
        }

        return false;
    }
}
