﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WorldChunk
{
    public MapPos pos;

    public int[,] grid;
    public string[,] mappables;

    public Natural[] naturals;

    public WorldChunk(MapPos posArg)
    {
        pos = posArg;
    }
}
