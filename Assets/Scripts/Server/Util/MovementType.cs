﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementType
{
    MOVEMENT_LAND,
    MOVEMENT_SEA,
    MOVEMENT_AIR
}
