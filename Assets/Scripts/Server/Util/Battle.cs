﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle 
{
    public string battleID;

    IGroup attacker;
    IGroup defender;

    public BattleState state;

    public int battleWidth;
    public int battleHeight;

    List<string> remainingAttacker;
    List<string> remainingDefender;

    List<UnitAI> attackerAI;
    List<UnitAI> defenderAI;

    List<UnitSync> syncs;

    public float tileWidth;

    public Battle(string battleIDArg, IGroup attackerArg, IGroup defenderArg)
    {
        battleID = battleIDArg;
        attacker = attackerArg;
        defender = defenderArg;

        battleWidth = 60;
        battleHeight = 30;

        tileWidth = 0.5f;

        remainingAttacker = new List<string>();
        remainingDefender = new List<string>();

        attackerAI   = new List<UnitAI>();
        defenderAI = new List<UnitAI>();

        syncs = new List<UnitSync>();
    }

    public string GetID()
    {
        return battleID;
    }

    public void AutoResolve()
    {

    }

    public void StartDeployment()
    {
        state = BattleState.DEPLOYMENT;
        MessagePump.instance.QueueMsg(new BattleStateChangedMessage(battleID, BattleState.DEPLOYMENT));

        List<MapPos> attackArea = new List<MapPos>();
        List<MapPos> defendArea = new List<MapPos>();

        for (int i = 0; i < battleWidth; i++)
        {
            for (int e = 0; e < battleHeight; e++)
            {
                if (i >= 12 && i <= 18 && e >= 16 && e <= 28)
                {
                    attackArea.Add(new MapPos(i, e));
                }

                if (i >= 38 && i <= 44 && e >= 16 && e <= 28)
                {
                    defendArea.Add(new MapPos(i, e));
                }
            }
        }

        MessagePump.instance.QueueMsg(new DeploymentAreaDefinedMessage(battleID, attacker.GetID(), attackArea));
        MessagePump.instance.QueueMsg(new DeploymentAreaDefinedMessage(battleID, defender.GetID(), defendArea));
    }

    public void OnUnitKilled(string groupID, string unitID)
    {
        if(groupID == attacker.GetID())
        {
            remainingAttacker.Remove(unitID);
        }
        else if(groupID == defender.GetID())
        {
            remainingDefender.Remove(unitID);
        }

        foreach(UnitAI ai in attackerAI)
        {
            ai.OnUnitKilled(unitID);
        }
        foreach(UnitAI ai in defenderAI)
        {
            ai.OnUnitKilled(unitID);
        }

        CheckResult();
    }

    public void OnUnitRouted(string groupID, string unitID)
    {
        if (groupID == attacker.GetID())
        {
            remainingAttacker.Remove(unitID);
        }
        else if (groupID == defender.GetID())
        {
            remainingDefender.Remove(unitID);
        }

        foreach (UnitAI ai in attackerAI)
        {
            ai.OnUnitRouted(unitID);
        }
        foreach (UnitAI ai in defenderAI)
        {
            ai.OnUnitRouted(unitID);
        }

        CheckResult();
    }

    public void CheckResult()
    {
        if(remainingAttacker.Count <= 0)
        {
            state = BattleState.RESULTS;
            MessagePump.instance.QueueMsg(new BattleStateChangedMessage(battleID, state));
        }
        else if(remainingDefender.Count <= 0)
        {
            state = BattleState.RESULTS;
            MessagePump.instance.QueueMsg(new BattleStateChangedMessage(battleID, state));
        }
    }

    public void StartBattle()
    {
        state = BattleState.FIGHT;
        MessagePump.instance.QueueMsg(new BattleStateChangedMessage(battleID, state));

        foreach(UnitAI ai in attackerAI)
        {
            ai.StartFight(defenderAI);
        }

        foreach(UnitAI ai in defenderAI)
        {
            ai.StartFight(attackerAI);
        }
    }

    public void Update()
    {
        if(state == BattleState.FIGHT)
        {
            syncs.Clear();
            foreach(UnitAI ai in attackerAI)
            {
                ai.Update();
                syncs.Add(ai.GetSync());
            }
            foreach (UnitAI ai in defenderAI)
            {
                ai.Update();
                syncs.Add(ai.GetSync());
            }
            MessagePump.instance.QueueMsg(new BattleSyncMessage(battleID, syncs));
        }
    }

    public void DeployUnit(string groupID, string unitID, MapPos pos)
    {
        Debug.Log(groupID);
        Debug.Log(defender.GetID());
        if(attacker.GetID() == groupID)
        {
            UnitAI ai = new UnitAI(groupID, attacker.GetUnit(unitID), GetVectorPosAtTile(pos));
            remainingAttacker.Add(unitID);
            attackerAI.Add(ai);
        }
        else if(defender.GetID() == groupID)
        {
            UnitAI ai = new UnitAI(groupID, defender.GetUnit(unitID), GetVectorPosAtTile(pos));
            remainingDefender.Add(unitID);
            defenderAI.Add(ai);
        }
        MessagePump.instance.QueueMsg(new UnitDeployedOnBattlefieldMessage(battleID, groupID, unitID, pos));
    }

    public Vector2 GetVectorPosAtTile(MapPos pos)
    {
        return new Vector2((pos.x * tileWidth) - 14, (pos.y * tileWidth) - 12);
    }
}
