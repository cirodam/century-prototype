﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitType
{
    UNIT_TYPE_BATTALION,
    UNIT_TYPE_SECTION,
    UNIT_TYPE_SHIP
}
