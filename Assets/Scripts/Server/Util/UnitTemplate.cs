﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnitTemplate 
{
    public UnitType unitType;
    public string unitName;
    public string soldierName;
    public int soldierMax;
    public string weaponName;
    public int weaponMax;

    public UnitTemplate()
    {

    }

    public UnitTemplate(string unitNameArg, string soldierNameArg, string weaponNameArg, int soldierMaxArg, int weaponMaxArg, UnitType unitTypeArg)
    {
        unitType = unitTypeArg;
        unitName = unitNameArg;
        soldierName = soldierNameArg;
        weaponName = weaponNameArg;
        soldierMax = soldierMaxArg;
        weaponMax = weaponMaxArg;
    }
}
