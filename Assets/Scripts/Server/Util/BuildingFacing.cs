﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuildingFacing 
{
    FACING_EAST,
    FACING_SOUTH,
    FACING_WEST,
    FACING_NORTH
}
