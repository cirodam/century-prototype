﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupTemplate 
{
    public string groupName;
    public MovementType movementType;
    public List<UnitTemplate> units;

    public GroupTemplate()
    {
        units = new List<UnitTemplate>();
    }

    public GroupTemplate(string groupNameArg, MovementType movementTypeArg, List<UnitTemplate> unitsArg)
    {
        groupName = groupNameArg;
        movementType = movementTypeArg;
        units = unitsArg;
    }
}
