﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Mover 
{
    IMoveable target;
    bool started;

    Queue<MapPos> remaining;
    MapPos current;

    public UnityEvent OnMoveComplete;
    public UnityEvent OnIDCollision;

    public string collidedID;

    public Mover(IGroup newTarget)
    {
        target = newTarget;
        remaining = new Queue<MapPos>();
        OnMoveComplete = new UnityEvent();
        OnIDCollision = new UnityEvent();
    }

    public void FollowPath(List<MapPos> newPath)
    {
        remaining = new Queue<MapPos>();
        foreach (MapPos m in newPath)
        {
            remaining.Enqueue(m);
        }
        started = true;
    }

    public void Update()
    {
        if(started)
        {
            if(current == null)
            {
                if(remaining.Count > 0)
                {
                    current = remaining.Dequeue();

                    if(WorldController.instance.GetObjectAtPos(current) == "")
                    {
                        MessagePump.instance.QueueMsg(new GroupMapPosUpdatedMessage(target.GetID(), target.GetWorldPos(), current));
                        target.SetWorldPos(current);
                    }
                    else
                    {
                        started = false;
                        remaining.Clear();

                        collidedID = WorldController.instance.GetObjectAtPos(current);

                        current = null;
                        OnIDCollision.Invoke();
                    }
                }
                else
                {
                    started = false;
                    OnMoveComplete.Invoke();
                }
            }
            else
            {
                if (Vector2.Distance(target.GetVectorPos(), WorldRenderer.instance.GetPositionOfTile(current)) >= 0.01f)
                {
                    target.SetVectorPos(Vector2.MoveTowards(target.GetVectorPos(), WorldRenderer.instance.GetPositionOfTile(current), 0.01f));
                }
                else
                {
                    current = null;
                }
            }
        }
    }

    public IMoveable GetTarget()
    {
        return target;
    }
}
