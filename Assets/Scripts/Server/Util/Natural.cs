﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Natural 
{
    public string naturalID;
    public string name;
    public MapPos pos;

    public Natural(string idArg, string nameArg, MapPos posArg)
    {
        naturalID = idArg;
        name = nameArg;
        pos = posArg;
    }
}
