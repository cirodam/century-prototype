﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProductionOption 
{
    public List<ResourceAmount> inputs;
    public ResourceAmount output;

    public ProductionOption(List<ResourceAmount> newInputs, ResourceAmount newOutput)
    {
        inputs = newInputs;
        output = newOutput;
    }
}
