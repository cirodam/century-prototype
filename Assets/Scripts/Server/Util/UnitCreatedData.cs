﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCreatedData 
{
    public string unitID;
    public string title;
    public int count;

    public List<ValueEntry> options;

    public UnitCreatedData(string unitIDArg, string titleArg, int countArg, List<ValueEntry> optionsArg)
    {
        unitID  = unitIDArg;
        title   = titleArg;
        count   = countArg;
        options = optionsArg;
    }
}
