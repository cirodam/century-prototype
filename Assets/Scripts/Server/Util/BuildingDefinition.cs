﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuildingDefinition 
{
    public string buildingName;
    public string buildingType;

    public string uiSpritePath;
    public string spritePath;

    public GameObject actor;
    public GameObject panel;

    public string productionRole;
    public float upkeep;

    public List<MapPos> Footprint;
    public List<ResourceAmount> buildingCosts;

    public int jobs;

    public List<StringValueEntry> options;

    public string GetBuildingName()
    {
        return buildingName;
    }

    public string GetString(string key)
    {
        foreach(StringValueEntry v in options)
        {
            if(v.key == key)
            {
                return v.value;
            }
        }
        return "Absent";
    }

    public int GetInt(string key)
    {
        foreach(StringValueEntry v in options)
        {
            if(v.key == key)
            {
                return int.Parse(v.value);
            }
        }
        return -1;
    }

    public float GetFloat(string key)
    {
        foreach(StringValueEntry v in options)
        {
            if(v.key == key)
            {
                return float.Parse(v.value);
            }
        }
        return -1;
    }
}
