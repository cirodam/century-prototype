﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GroupType 
{
    GROUP_TYPE_ARMY,
    GROUP_TYPE_FLEET,
    GROUP_TYPE_AIRGROUP
}
