﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDataController : MonoBehaviour
{
    public static BuildingDataController instance;
    public List<BuildingDefinition> buildings;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public BuildingDefinition GetDataForBuilding(string name)
    {
        foreach(BuildingDefinition b in buildings)
        {
            if(b.buildingName == name)
            {
                return b;
            }
        }

        return null;
    }
}
