﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : MonoBehaviour
{
    public static BattleController instance;

    Dictionary<string, Battle> battles;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        battles = new Dictionary<string, Battle>();

        MessagePump.instance.Register(this, MessageType.UNIT_KILLED_MESSAGE, OnUnitKilled);
        MessagePump.instance.Register(this, MessageType.UNIT_ROUTED_MESSAGE, OnUnitRouted);
    }

    public void ResolveBattle(IGroup attacker, IGroup defender, ICity city)
    {

        Battle b = new Battle(System.Guid.NewGuid().ToString(), (Group) attacker, (Group) defender);
        battles.Add(b.GetID(), b);

        if(ClientDataController.instance.GetData(attacker.GetOwnerID()).clientType == ClientType.CLIENT_TYPE_AI && ClientDataController.instance.GetData(defender.GetOwnerID()).clientType == ClientType.CLIENT_TYPE_AI)
        {
            b.AutoResolve();
        }
        else
        {
            TimeController.instance.PauseSimulation();

            MessagePump.instance.QueueMsg(new ClientBattlePendingMessage(b.GetID(), attacker.GetID(), defender.GetID()));
        }
    }

    public void Update()
    {
        foreach(Battle b in battles.Values)
        {
            b.Update();
        }
    }

    private void OnUnitKilled(Message msg)
    {
        UnitKilledMessage cMsg = (UnitKilledMessage)msg;
        foreach(Battle b in battles.Values)
        {
            b.OnUnitKilled(cMsg.groupID, cMsg.unitID);
        }
    }

    private void OnUnitRouted(Message msg)
    {
        UnitRoutedMessage cMsg = (UnitRoutedMessage)msg;
        foreach (Battle b in battles.Values)
        {
            b.OnUnitRouted(cMsg.groupID, cMsg.unitID);
        }
    }

    public void FightBattle(string clientID, string battleID)
    {
        battles[battleID].StartDeployment();
    }

    public void StartBattle(string clientID, string battleID)
    {
        battles[battleID].StartBattle();
    }

    public void DeployUnitOnBattlefield(string battleID, string groupID, string unitID, MapPos battlePos)
    {
        battles[battleID].DeployUnit(groupID, unitID, battlePos);
    }
}
