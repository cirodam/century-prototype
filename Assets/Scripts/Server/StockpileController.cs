﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockpileController : MonoBehaviour
{
    public static StockpileController instance;
    
    public Dictionary<string, Stockpile> stockpiles;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        stockpiles = new Dictionary<string, Stockpile>();

        MessagePump.instance.Register(this, MessageType.CLIENT_DATA_READY_MESSAGE, OnClientDataReady);
        MessagePump.instance.Register(this, MessageType.STOCKPILE_CREATED_MESSAGE, OnStockpileCreated);

        MessagePump.instance.Register(this, MessageType.PRE_NEW_DAY_MESSAGE, OnPreNewDay);
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
        MessagePump.instance.Register(this, MessageType.POST_NEW_DAY_MESSAGE, OnPostNewDay);
    }

    private void OnClientDataReady(Message msg)
    {
        ClientDataReadyMessage cMsg = (ClientDataReadyMessage)msg;

        if(!stockpiles.ContainsKey(cMsg.clientID))
        {
            stockpiles.Add(cMsg.clientID, new Stockpile(cMsg.clientID));
            MessagePump.instance.QueueMsg(new StockpileCreatedMessage(cMsg.clientID, stockpiles[cMsg.clientID]));
        }
    }

    private void OnStockpileCreated(Message msg)
    {
        StockpileCreatedMessage cMsg = (StockpileCreatedMessage)msg;

        if(stockpiles.ContainsKey(cMsg.ownerID))
        {
            stockpiles[cMsg.ownerID].AddResourceAmount("Gold", 1000);
            stockpiles[cMsg.ownerID].AddResourceAmount("Lumber", 1000);
        }
    }

    public void CreateStockpile(string clientID)
    {

    }

    private void OnPreNewDay(Message msg)
    {
        foreach(Stockpile s in stockpiles.Values)
        {
            s.OnPreNewDay();
        }
    }

    private void OnNewDay(Message msg)
    {
        foreach (Stockpile s in stockpiles.Values)
        {
            s.OnNewDay();
        }
    }

    private void OnPostNewDay(Message msg)
    {
        foreach (Stockpile s in stockpiles.Values)
        {
            s.OnPostNewDay();
        }
    }

    public Stockpile GetStockpile(string ownerID)
    {
        if(stockpiles.ContainsKey(ownerID))
        {
            return stockpiles[ownerID];
        }
        return null;
    }
}
