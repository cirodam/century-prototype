﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMoveable : IMappable 
{
    void SetVectorPos(Vector2 posArg);
    Vector2 GetVectorPos();
    MovementType GetMovementType();
    void SetWorldPos(MapPos posArg);
}
