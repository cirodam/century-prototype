﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDefendable 
{
    List<string> GetGarrisons();
    bool AddGarrison(string id);
    bool RemoveGarrison(string id);
    IGroup GetDefenders();
    void OnCapture(string ownerIDArg);
}
