﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGroup : IMappable, IMoveable, IDefender
{
    List<IUnit> GetUnits();
    float GetStrength();
    void OnNewDay();
    bool AddUnit(IUnit unit);
    bool RemoveUnit(string id);
    IUnit GetUnit(string id);
    bool IsProvisioning();
}
