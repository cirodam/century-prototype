﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBuilding 
{
    string GetID();
    MapPos GetPos();
    BuildingFacing GetFacing();
    string GetBuildingName();
    string GetBuildingType();
    void OnAdd(ICity city, IStockpile stockpile);
    void OnNewDay(ICity city, IStockpile stockpile);
    void OnRemove(ICity city, IStockpile stockpile);
    void OnUpgrade();
    float GetUpkeep();
    int GetLevel();
    float GetGDP();
}
