﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICity : IMappable, IDefendable
{
    List<IBuilding> GetBuildings();
    IBuilding GetBuilding(string buildingID);
    IBuilding GetBuildingAtPosition(MapPos pos);

    bool IsPositionLocked(MapPos pos);
    bool IsPositionOccupied(MapPos pos);
    MapPos SampleRandomPosition(bool locked, bool occupied);
    bool IsCoastal();

    void OnStockpileCreated(string id);
    void OnNewDay();

    int GetCityWidth();
    int GetCityHeight();

    void AddHousing(int amount);
    void SubtractHousing(int amount);

    int GetHousingCount();
    int GetCitizenCount();

    CityTile[,] GetCityMap();

    bool AddBuilding(IBuilding b);
    bool RemoveBuilding(string buildingID);

    void AddCitizens(string profession, int count);
    void SubtractCitizens(string profession, int count);

    void AddJobs(int amount);
    void SubtractJobs(int amount);
    int GetCitizenCount(string profession);
}
