﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMappable : IFindable
{
    string GetOwnerID();
    string GetTitle();
    MapPos GetWorldPos();
}
