﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnit 
{
    string GetID();
    int GetCount();
    string GetTitle();

    float GetHealth();
    float GetMoveSpeed();
    float GetAttackStrength();
    float GetDefense();
    float GetAttackCooldown();

    string GetSoldierResourceName();
    string GetWeaponResourceName();

    void OnDamage(float damage);
    void OnNewDay(IStockpile s);
    void OnAddAsGarrison();
    void OnRemoveAsGarrison();

    UnitType GetUnitType();
    List<ValueEntry> GetOptions();
}
