﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDefender 
{
    bool IsStationed();
    void OnStation(string defendableID, MapPos pos);
    void OnDeploy(MapPos pos);
}
