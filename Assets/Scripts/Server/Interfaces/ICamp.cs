﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICamp: IMappable, IDefendable 
{
    void OnNewDay();
    bool IsDestroyedOnCapture();
}
