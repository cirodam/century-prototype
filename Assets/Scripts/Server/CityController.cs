﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityController : MonoBehaviour
{
    public static CityController instance;
    
    public Dictionary<string, City> cities;

    private string[] cityNames;
    private int nextName;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        MessagePump.instance.Register(this, MessageType.CLIENT_DATA_READY_MESSAGE, OnClientDataReady);
        MessagePump.instance.Register(this, MessageType.STOCKPILE_CREATED_MESSAGE, OnStockpileCreated);
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
        MessagePump.instance.Register(this, MessageType.CLIENT_CHANGE_BUILDING_VALUE_INTENT, OnClientChangeBuildingValue);
        MessagePump.instance.Register(this, MessageType.CLIENT_UPGRADE_BUILDING_INTENT, OnClientUpgradeBuilding);
        MessagePump.instance.Register(this, MessageType.GROUP_DEPLOYED_MESSAGE, OnGroupDeployed);
        MessagePump.instance.Register(this, MessageType.GROUP_CREATED_MESSAGE, OnGroupCreated);

        cities = new Dictionary<string, City>();

        cityNames = new string[] { "Barfun", "Maxim", "Lavanda", "Morrison", "Axiom", "Lamori", "Foreior", "Erioa", "Portia", "Leariol", "Borsi", "Verivia"};
        nextName = 0;
    }

    private void OnClientDataReady(Message msg)
    {
        ClientDataReadyMessage cMsg = (ClientDataReadyMessage) msg;

        MapPos pos = WorldController.instance.SampleRandomCoastline();
        CreateCity(cityNames[nextName], cMsg.clientID, StockpileController.instance.GetStockpile(cMsg.clientID), pos);
        nextName++;
    }

    private void OnGroupCreated(Message msg)
    {
        GroupCreatedMessage cMsg = (GroupCreatedMessage)msg;

        if(cities.ContainsKey(cMsg.cityID))
        {
            cities[cMsg.cityID].AddGarrison(cMsg.groupID);
        }
    }

    private void OnGroupDeployed(Message msg)
    {
        GroupDeployedMessage cMsg = (GroupDeployedMessage)msg;

        if(cities.ContainsKey(cMsg.defendableID))
        {
            cities[cMsg.defendableID].RemoveGarrison(cMsg.groupID);
        }
    }

    public void CreateCity(string ownerID, string cityName, MapPos worldPos)
    {

    }

    public void DestroyCity(string cityID)
    {
        if(cities.ContainsKey(cityID))
        {
            cities.Remove(cityID);
            MessagePump.instance.QueueMsg(new CityDestroyedMessage(cityID));
        }
    }

    public void CreateBuilding(string ownerID, string cityID, string buildingName, MapPos cityPos, BuildingFacing facing)
    {
        string id = System.Guid.NewGuid().ToString();
        BuildingDefinition bd = BuildingDataController.instance.GetDataForBuilding(buildingName);

        if(!PurchaseBuilding(ownerID, bd))
        {
            return;
        }

        IBuilding building = null;
        List<ValueEntry> options = new List<ValueEntry>();

        switch(bd.buildingType)
        {
            case "House":
                building = new House(id, cityPos, facing, 1, bd.jobs);
                options.Add(new ValueEntry("Housing", 50, ValueType.VALUE_TYPE_INT));
                break;

            case "Barracks":
                building = new Barracks(id, cityPos, facing, 1, bd.jobs);
                break;

            case "Collector":
                building = new Collector(id, bd.buildingName, cityPos, facing, 1, bd.jobs, bd.GetString("ResourceName"), bd.GetInt("ResourceAmount"));
                break;

            case "Producer":
                building = new Producer(id, bd.buildingName, cityPos, facing, 1, bd.jobs, bd.GetString("ProductionName"), bd.GetFloat("ProductionMultiplier"));
                break;

            case "Farm":
                building = new ProductionBuilding(id, cityPos, facing, "Farm", 1);
                ((ProductionBuilding)building).AddProductionOption(ResourceDataController.instance.GetDataForResource("Wheat"));
                options.Add(new ValueEntry("ProductionName", "Wheat", ValueType.VALUE_TYPE_STRING));
                break;

            case "Road":
                building = new Road(id, cityPos, facing, 1);
                break;

            case "Ironmine":
                building = new ProductionBuilding(id, cityPos, facing, "Ironmine", 1);
                ((ProductionBuilding)building).AddProductionOption(ResourceDataController.instance.GetDataForResource("Iron"));
                options.Add(new ValueEntry("ProductionName", "Iron", ValueType.VALUE_TYPE_STRING));
                break;

            case "Weaponsmith":
                building = new ProductionBuilding(id, cityPos, facing, "Weaponsmith", 1);
                ((ProductionBuilding)building).AddProductionOption(ResourceDataController.instance.GetDataForResource("Sword"));
                options.Add(new ValueEntry("ProductionName", "Sword", ValueType.VALUE_TYPE_STRING));
                break;

            case "Shipyard":
                building = new ProductionBuilding(id, cityPos, facing, "Shipyard", 1);
                ((ProductionBuilding)building).AddProductionOption(ResourceDataController.instance.GetDataForResource("Ship"));
                options.Add(new ValueEntry("ProductionName", "Ship", ValueType.VALUE_TYPE_STRING));
                break;

            case "Market":
                building = new ProductionBuilding(id, cityPos, facing, "Market", 1);
                ((ProductionBuilding)building).AddProductionOption(ResourceDataController.instance.GetDataForResource("Gold"));
                options.Add(new ValueEntry("ProductionName", "Gold", ValueType.VALUE_TYPE_STRING));
                break;

            case "Ranch":
                building = new ProductionBuilding(id, cityPos, facing, "Ranch", 1);
                ((ProductionBuilding)building).AddProductionOption(ResourceDataController.instance.GetDataForResource("Horse"));
                options.Add(new ValueEntry("ProductionName", "Horse", ValueType.VALUE_TYPE_STRING));
                break;
        }

        cities[cityID].AddBuilding(building);
        MessagePump.instance.QueueMsg(new BuildingCreatedMessage(ownerID, cityID, building.GetID(), building.GetBuildingName(), building.GetLevel(), building.GetUpkeep(), building.GetPos(), building.GetFacing(), options));
    }

    public void DestroyBuilding(string cityID, string buildingID)
    {
        if(cities.ContainsKey(cityID))
        {
            cities[cityID].RemoveBuilding(buildingID);
            MessagePump.instance.QueueMsg(new BuildingDestroyedMessage(cityID, buildingID));
        }
    }

    private void OnClientChangeBuildingValue(Message msg)
    {
        ClientChangeBuildingValueIntent cMsg = (ClientChangeBuildingValueIntent)msg;

        IBuilding b = cities[cMsg.cityID].GetBuilding(cMsg.buildingID);

        MessagePump.instance.QueueMsg(new BuildingValueUpdatedMessage(cMsg.cityID, cMsg.buildingID, cMsg.key, cMsg.value));
    }

    private void OnClientUpgradeBuilding(Message msg)
    {
        ClientUpgradeBuildingIntent cMsg = (ClientUpgradeBuildingIntent)msg;

        IBuilding b = cities[cMsg.cityID].GetBuilding(cMsg.buildingID);
        b.OnUpgrade();

        MessagePump.instance.QueueMsg(new BuildingValueUpdatedMessage(cMsg.cityID, cMsg.buildingID, "BuildingLevel", b.GetLevel()));
    }

    private void OnStockpileCreated(Message msg)
    {
        StockpileCreatedMessage cMsg = (StockpileCreatedMessage)msg;
        foreach(City c in GetCitiesOfOwner(cMsg.ownerID))
        {
            c.OnStockpileCreated(cMsg.ownerID);
        }
    }

    private void OnNewDay(Message msg)
    {
        foreach(City c in cities.Values)
        {
            c.OnNewDay();
        }
    }

    public void CreateCity(string name, string ownerID, IStockpile stockpile, MapPos pos)
    {
        string id = System.Guid.NewGuid().ToString();
        City c    = new City(id, name, ownerID, stockpile, pos, true);

        cities.Add(c.GetID(), c);
        MessagePump.instance.QueueMsg(new CityCreatedMessage(c.GetOwnerID(), c.GetID(), name, pos, c.GetCityMap(), c.IsCoastal(), new List<ValueEntry>()));
        c.Init();
    }

    public ICity GetCity(string cityID)
    {
        if(cities.ContainsKey(cityID))
        {
            return cities[cityID];
        }
        return null;
    }

    public void ChangeCityOwner(int cityId, int newOwnerID)
    {
        //int oldOwner = cities[cityId].GetOwnerID();
        //cities[cityId].SetOwner(newOwnerID);
        //MessagePump.instance.QueueMsg(new CityOwnerChangedMessage(cityId, oldOwner, newOwnerID));
    }

    public int GetFactionCityCount(int ownerID)
    {
        int count = 0;
        foreach(ICity i in cities.Values)
        {
            //if (i.GetOwnerID() == ownerID)
            //{
            //    count++;
            //}
        }

        return count;
    }

    private List<City> GetCitiesOfOwner(string ownerID)
    {
        List<City> ret = new List<City>();
        foreach(City c in cities.Values)
        {
            if(c.GetOwnerID() == ownerID)
            {
                ret.Add(c);
            }
        }

        return ret;
    }

    private bool PurchaseBuilding(string ownerID, BuildingDefinition bd)
    {
        IStockpile s = StockpileController.instance.GetStockpile(ownerID);

        bool enough = true;
        foreach(ResourceAmount ra in bd.buildingCosts)
        {
            if(!s.HasResourceAmount(ra.resourceName, ra.resourceAmount))
            {
                enough = false;
            }
        }

        if(enough)
        {
            foreach(ResourceAmount ra in bd.buildingCosts)
            {
                s.SubtractResourceAmount(ra.resourceName, ra.resourceAmount);
            }
            return true;
        }

        return false;
    }

    public void CreateCity(string clientID)
    {

    }
}
