﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameServer : MonoBehaviour
{
    public static GameServer instance;

    private int expectedClients;
    private int presentClients;
    private int readyClients;

    private List<string> clients;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        expectedClients = SessionController.instance.GetClientCount();
        clients = new List<string>();

        MessagePump.instance.Register(this, MessageType.CLIENT_READY_MESSAGE, OnClientReady);
        MessagePump.instance.Register(this, MessageType.CLIENT_PRESENT_MESSAGE, OnClientPresent);
    }

    private void Start()
    {
        MessagePump.instance.QueueMsg(new ServerPresentMessage());
    }

    private void OnClientPresent(Message msg)
    {
        ClientPresentMessage cMsg = (ClientPresentMessage) msg;
        if (!clients.Contains(cMsg.clientID))
        {
            clients.Add(cMsg.clientID);
            presentClients++;

            if (presentClients >= expectedClients)
            {
                MessagePump.instance.QueueMsg(new AllClientsPresentMessage());
            }
        }
    }

    private void OnClientReady(Message msg)
    {
        readyClients++;
        if (readyClients >= expectedClients)
        {
            MessagePump.instance.QueueMsg(new GameStartingMessage());
        }
    }

    private void StartServer()
    {
        WorldController.instance.Generate();
    }

    public void RegisterClient(string clientID)
    {
        StockpileController.instance.CreateStockpile(clientID);
        TechController.instance.CreateTree(clientID);
        CityController.instance.CreateCity(clientID);
    }

    public void CreateCamp(string clientID, string campName, MapPos worldPos)
    {
        CampController.instance.CreateCamp(clientID, worldPos, campName);
    }

    public void DeployGroup(string defenderID, string defendableID, MapPos defendablePos)
    {
        GroupController.instance.DeployGroup(defenderID, defendableID, defendablePos);
    }

    public void FightBattle(string clientID, string battleID)
    {
        BattleController.instance.FightBattle(clientID, battleID);
    }
    public void CreateGroup(string clientID, string cityID, GroupTemplate template)
    {
        GroupController.instance.CreateGroup(clientID, cityID, false, template);
    }

    public void DestroyGroup(string groupID)
    {
        GroupController.instance.DestroyGroup(groupID);
    }

    public void CreateUnit(string clientID, string groupID, UnitTemplate template)
    {
        GroupController.instance.CreateUnit(clientID, groupID, template);
    }

    public void DestroyUnit(string groupID, string unitID)
    {
        GroupController.instance.DestroyUnit(groupID, unitID);
    }

    public void DeployUnitOnBattleField(string clientID, string battleID, string groupID, string unitID, MapPos battlePos)
    {
        BattleController.instance.DeployUnitOnBattlefield(battleID, groupID, unitID, battlePos);
    }

    public void StartBattle(string clientID, string battleID)
    {
        BattleController.instance.StartBattle(clientID, battleID);
    }

    public void CreateCity(string ownerID, string cityName, MapPos worldPos)
    {
        CityController.instance.CreateCity(ownerID, cityName, worldPos);
    }

    public void DestroyCity(string cityID)
    {
        CityController.instance.DestroyCity(cityID);
    }

    public void CreateBuilding(string ownerID, string cityID, string buildingName, MapPos cityPos, BuildingFacing facing)
    {
        CityController.instance.CreateBuilding(ownerID, cityID, buildingName, cityPos, facing);
    }

    public void DestroyBuilding(string cityID, string buildingID)
    {
        CityController.instance.DestroyBuilding(cityID, buildingID);
    }
}
