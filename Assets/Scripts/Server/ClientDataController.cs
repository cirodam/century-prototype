﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientDataController : MonoBehaviour
{
    public static ClientDataController instance;
    Dictionary<string, ClientData> clientData;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        clientData = new Dictionary<string, ClientData>();

        MessagePump.instance.Register(this, MessageType.CLIENT_DATA_READY_MESSAGE, OnClientDataReady);
    }

    private void OnClientDataReady(Message msg)
    {
        ClientDataReadyMessage cMsg = (ClientDataReadyMessage)msg;

        if(!clientData.ContainsKey(cMsg.clientID))
        {
            clientData.Add(cMsg.clientID, cMsg.data);
        }
    }

    public ClientData GetData(string clientID)
    {
        return clientData[clientID];
    }
}
