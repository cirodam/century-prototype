﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public static MovementController instance;

    PathNode[,] map;

    Dictionary<string, Mover> movers;

    List<string> finished;

    bool paused;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        finished = new List<string>();
        movers = new Dictionary<string, Mover>();
        map = new PathNode[100, 100];
        for (int i = 0; i < 100; i++)
        {
            for (int e = 0; e < 100; e++)
            {
                map[i, e] = new PathNode(i, e);
            }
        }

        MessagePump.instance.Register(this, MessageType.SIMULATION_PAUSED_MESSAGE, OnSimulationPaused);
        MessagePump.instance.Register(this, MessageType.SIMULATION_UNPAUSED_MESSAGE, OnSimulationUnpaused);
        MessagePump.instance.Register(this, MessageType.CLIENT_MOVE_GROUP_INTENT, OnClientMoveGroupIntent);
    }

    private void OnClientMoveGroupIntent(Message msg)
    {
        ClientMoveGroupIntent cMsg = (ClientMoveGroupIntent)msg;

        IGroup g = GroupController.instance.GetGroup(cMsg.groupID);
        List<MapPos> path = FindPath(g.GetWorldPos(), cMsg.endingPos, g.GetMovementType());
        FollowPath(g, path);
        MessagePump.instance.QueueMsg(new GroupMovingAlongPathMessage(cMsg.groupID, path));
    }

    public void FollowPath(IGroup group, List<MapPos> path)
    {
        if(!movers.ContainsKey(group.GetID()))
        {
            Mover m = new Mover(group);
            m.OnMoveComplete.AddListener(() => { OnMoveComplete(group.GetID()); });
            m.OnIDCollision.AddListener(() => { OnIDCollision(group.GetID()); });
        
            movers.Add(group.GetID(), m);
        }
        movers[group.GetID()].FollowPath(path);
    }

    private void OnSimulationPaused(Message msg)
    {
        paused = true;
    }

    private void OnSimulationUnpaused(Message msg)
    {
        paused = false;
    }

    private void Update()
    {
        if(!paused)
        {
            foreach (Mover m in movers.Values)
            {
                m.Update();
            }

            foreach (string id in finished)
            {
                movers.Remove(id);
                MessagePump.instance.QueueMsg(new GroupMoveCompletedMessage(id));
            }
            finished.Clear();
        }
    }

    private void OnMoveComplete(string id)
    {
        finished.Add(id);
    }

    private void OnIDCollision(string id)
    {
        IGroup current = GroupController.instance.GetGroup(id);

        IGroup group   = GroupController.instance.GetGroup(movers[id].collidedID);
        ICity city     = CityController.instance.GetCity(movers[id].collidedID);
        ICamp camp     = CampController.instance.GetCamp(movers[id].collidedID);

        Debug.Log("Group");
        if (group != null) //Collided with group
        {
            MessagePump.instance.QueueMsg(new GroupCollisionMessage(current.GetID(), group.GetID()));
            if (group.GetOwnerID() != current.GetOwnerID())
            {
                BattleController.instance.ResolveBattle(current, group, null);
            }
            return;
        }

        Debug.Log("City");
        if(city != null) //Collided with city
        {
            MessagePump.instance.QueueMsg(new GroupCollisionMessage(current.GetID(), city.GetID()));
            Debug.Log("Collided with city");
            if (city.GetOwnerID() == current.GetOwnerID())
            {
                Debug.Log(city.AddGarrison(current.GetID()));
                current.OnStation(city.GetID(), city.GetWorldPos());
                MessagePump.instance.QueueMsg(new GroupGarrisonedMessage(current.GetID(), city.GetID()));
            }
            else
            {
                BattleController.instance.ResolveBattle(current, city.GetDefenders(), city);
            }
        }

        if(camp != null) //Collided with Camp
        {
            MessagePump.instance.QueueMsg(new GroupCollisionMessage(current.GetID(), camp.GetID()));
            if (camp.GetOwnerID() == current.GetOwnerID())
            {
                camp.AddGarrison(current.GetID());
                current.OnStation(camp.GetID(), camp.GetWorldPos());
                MessagePump.instance.QueueMsg(new GroupGarrisonedMessage(current.GetID(), camp.GetID()));
            }
            else
            {
                if(camp.GetOwnerID() == "")
                {
                    CampController.instance.ChangeCampOwner(camp.GetID(), current.GetOwnerID());
                    camp.AddGarrison(current.GetID());
                    current.OnStation(camp.GetID(), camp.GetWorldPos());
                    MessagePump.instance.QueueMsg(new GroupGarrisonedMessage(current.GetID(), camp.GetID()));
                }
                else
                {
                    BattleController.instance.ResolveBattle(current, camp.GetDefenders(), city);
                }
            }
        }
    }

    private void ResolveBattle(IGroup attacker, IGroup defender, ICity city)
    {
        
        /*if (attacker.GetOwnerID() == ViewController.instance.playerFactionID)
        {
            TimeController.instance.PauseSimulation();
            PendingBattlePanelController.instance.Show(attacker, defender, attacker, city);
        }
        else if (defender.GetOwnerID() == ViewController.instance.playerFactionID)
        {
            TimeController.instance.PauseSimulation();
            PendingBattlePanelController.instance.Show(attacker, defender, defender, city);
        }
        else
        {
            //Autoresolve
        }*/
    }

    public List<MapPos> FindPath(MapPos startPos, MapPos targetPos, MovementType movement)
    {
        List<PathNode> openList = new List<PathNode>();
        List<PathNode> closedList = new List<PathNode>();
        List<MapPos> path = new List<MapPos>();

        int step = 0;

        openList.Add(map[startPos.x, startPos.y]);

        do
        {
            PathNode sNode = GetSNode(openList, map[startPos.x, startPos.y], map[targetPos.x, targetPos.y]);

            openList.Remove(sNode);
            closedList.Add(sNode);

            if (closedList.Contains(map[targetPos.x, targetPos.y]))
            {
                PathNode node = map[targetPos.x, targetPos.y];
                while (node.parent != null)
                {
                    path.Add(new MapPos(node.pos.x, node.pos.y));
                    node = node.parent;
                }
                break;
            }

            List<MapPos> neighbors = GetNeighbors(new MapPos(sNode.pos.x, sNode.pos.y), movement);

            foreach (MapPos c in neighbors)
            {
                if (closedList.Contains(map[c.x, c.y]))
                {
                    continue;
                }

                if (!openList.Contains(map[c.x, c.y]))
                {
                    openList.Add(map[c.x, c.y]);
                    map[c.x, c.y].parent = sNode;
                }
                else
                {
                    if (GetNodeGValue(map[startPos.x, startPos.y], sNode) + map[c.x, c.y].distanceToParent < GetNodeGValue(map[startPos.x, startPos.y], map[c.x, c.y]))
                    {
                        map[c.x, c.y].parent = sNode;
                    }
                }

            }

            step++;
        }
        while (step < 1000 && openList.Count > 0);

        for (int i = 0; i < 100; i++)
        {
            for (int e = 0; e < 100; e++)
            {
                map[i, e].parent = null;
            }
        }

        path.Reverse();
        return path;
    }

    private PathNode GetSNode(List<PathNode> openList, PathNode startNode, PathNode targetNode)
    {
        PathNode lowest = openList[0];
        float lowestScore = GetNodeFValue(startNode, lowest, targetNode);

        foreach (PathNode m in openList)
        {
            float score = GetNodeFValue(startNode, m, targetNode);

            if (score <= lowestScore)
            {
                lowest = m;
                lowestScore = score;
            }
        }

        return lowest;
    }

    private float GetNodeFValue(PathNode startNode, PathNode node, PathNode targetNode)
    {
        return GetNodeGValue(startNode, node) + GetNodeHValue(node, targetNode);
    }

    private float GetNodeGValue(PathNode startNode, PathNode node)
    {
        if (node.parent != null)
        {
            return GetNodeGValue(startNode, node.parent) + node.distanceToParent;
        }
        else
        {
            return 0;
        }
    }

    private int GetNodeHValue(PathNode node, PathNode targetNode)
    {
        return (int)Mathf.Sqrt((targetNode.pos.x - node.pos.x) * (targetNode.pos.x - node.pos.x) + (targetNode.pos.y - node.pos.y) * (targetNode.pos.y - node.pos.y));
    }

    private List<MapPos> GetNeighbors(MapPos tile, MovementType movement)
    {
        List<MapPos> neighbors = new List<MapPos>();

        if (tile.x > 0 && IsTilePassable(new MapPos(tile.x - 1, tile.y), movement))
        {
            if (map[tile.x - 1, tile.y].parent == null)
            {
                map[tile.x - 1, tile.y].distanceToParent = 1;
            }
            neighbors.Add(new MapPos(tile.x - 1, tile.y));
        }

        if (tile.y > 0 && IsTilePassable(new MapPos(tile.x, tile.y - 1), movement))
        {
            if (map[tile.x, tile.y - 1].parent == null)
            {
                map[tile.x, tile.y - 1].distanceToParent = 1;
            }
            neighbors.Add(new MapPos(tile.x, tile.y - 1));
        }

        if (tile.x < WorldController.instance.worldSizeX - 1 && IsTilePassable(new MapPos(tile.x + 1, tile.y), movement))
        {
            if (map[tile.x + 1, tile.y].parent == null)
            {
                map[tile.x + 1, tile.y].distanceToParent = 1;
            }
            neighbors.Add(new MapPos(tile.x + 1, tile.y));
        }

        if (tile.x < WorldController.instance.worldSizeY - 1 && IsTilePassable(new MapPos(tile.x, tile.y + 1), movement))
        {
            if (map[tile.x, tile.y + 1].parent == null)
            {
                map[tile.x, tile.y + 1].distanceToParent = 1;
            }
            neighbors.Add(new MapPos(tile.x, tile.y + 1));
        }

        if (tile.x > 0 && IsTilePassable(new MapPos(tile.x - 1, tile.y - 1), movement))
        {
            if (map[tile.x - 1, tile.y - 1].parent == null)
            {
                map[tile.x - 1, tile.y - 1].distanceToParent = 1.10f;
            }
            neighbors.Add(new MapPos(tile.x - 1, tile.y - 1));
        }

        if (tile.y > 0 && IsTilePassable(new MapPos(tile.x + 1, tile.y - 1), movement))
        {
            if (map[tile.x + 1, tile.y - 1].parent == null)
            {
                map[tile.x + 1, tile.y - 1].distanceToParent = 1.10f;
            }
            neighbors.Add(new MapPos(tile.x + 1, tile.y - 1));
        }

        if (tile.x < WorldController.instance.worldSizeX - 1 && IsTilePassable(new MapPos(tile.x + 1, tile.y + 1), movement))
        {
            if (map[tile.x + 1, tile.y + 1].parent == null)
            {
                map[tile.x + 1, tile.y + 1].distanceToParent = 1.10f;
            }
            neighbors.Add(new MapPos(tile.x + 1, tile.y + 1));
        }

        if (tile.x < WorldController.instance.worldSizeY - 1 && IsTilePassable(new MapPos(tile.x - 1, tile.y + 1), movement))
        {
            if (map[tile.x - 1, tile.y + 1].parent == null)
            {
                map[tile.x - 1, tile.y + 1].distanceToParent = 1.10f;
            }
            neighbors.Add(new MapPos(tile.x - 1, tile.y + 1));
        }

        neighbors.Reverse();
        return neighbors;
    }

    private bool IsTilePassable(MapPos pos, MovementType movement)
    {
        if(movement == MovementType.MOVEMENT_LAND && WorldController.instance.IsTileLand(pos))
        {
            return true;
        }
        else if(movement == MovementType.MOVEMENT_SEA && WorldController.instance.IsTileWater(pos))
        {
            return true;
        }
        else if(movement == MovementType.MOVEMENT_AIR)
        {
            return true;
        }

        return false;
    }
}
