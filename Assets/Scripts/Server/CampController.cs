﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampController : MonoBehaviour
{
    public static CampController instance;
    public Dictionary<string, ICamp> camps;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        camps = new Dictionary<string, ICamp>();

        MessagePump.instance.Register(this, MessageType.WORLD_READY_MESSAGE, OnWorldReady);

        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
        MessagePump.instance.Register(this, MessageType.GROUP_DEPLOYED_MESSAGE, OnGroupDeployed);
    }

    private void OnNewDay(Message msg)
    {
        foreach(ICamp c in camps.Values)
        {
            c.OnNewDay();
        }
    }

    private void OnWorldReady(Message msg)
    {
        WorldReadyMessage cMsg = (WorldReadyMessage)msg;

        /*for (int i = 0; i < 5; i++)
        {
            MapPos pos = WorldController.instance.SampleRandomPassable();
            CreateCamp("", pos, "Iron Mine", "ResourceCamp");
        }

        for (int i = 0; i < 5; i++)
        {
            MapPos pos = WorldController.instance.SampleRandomPassable();
            CreateCamp("", pos, "Lumber Camp", "ResourceCamp");
        }*/
    }

    private void OnGroupDeployed(Message msg)
    {
        GroupDeployedMessage cMsg = (GroupDeployedMessage)msg;

        if(camps.ContainsKey(cMsg.defendableID))
        {
            camps[cMsg.defendableID].RemoveGarrison(cMsg.groupID);
        }
    }

    public void CreateCamp(string ownerID, MapPos pos, string campName)
    {
        string id = System.Guid.NewGuid().ToString();
        ICamp camp = null;
        CampDefinition cd = CampDataController.instance.GetDataForCamp(campName);

        switch(cd.campType)
        {
            case "ResourceCamp":
                camp = new ResourceCamp(id, ownerID, pos, campName, cd.GetString("ResourceName"), cd.GetFloat("ResourceAmount"));
                break;
            case "FortCamp":
                camp = new FortCamp(id, ownerID);
                break;
        }

        camps.Add(id, camp);
        MessagePump.instance.QueueMsg(new CampCreatedMessage(id, ownerID, pos, campName, cd.campType));
    }

    public void ChangeCampOwner(string campID, string newOwnerID)
    {
        camps[campID].OnCapture(newOwnerID);
        MessagePump.instance.QueueMsg(new CampValueChangedMessage(campID, "OwnerID", newOwnerID));
    }

    public ICamp GetCamp(string id)
    {
        if(camps.ContainsKey(id))
        {
            return camps[id];
        }
        return null;
    }
}
