﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TechTreeController : MonoBehaviour
{
    public static TechTreeController instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
