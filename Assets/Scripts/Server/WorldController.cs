﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Unity.Mathematics;
using UnityEngine;

public class WorldController : MonoBehaviour
{
    public static WorldController instance;

    public float tileWidth = 0.48f;
    public int worldSizeX = 100;
    public int worldSizeY = 100;
    public int zLevels = 4;

    public int[,,] grid;
    private Dictionary<int, List<Vector2>> patches;

    public bool isReady;

    public string[,] objects;

    public List<Natural> naturals;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(instance);
        }
    }

    private void Start()
    {
        Generate();
    }

    private void Init()
    {
        objects = new string[worldSizeX, worldSizeY];
        naturals = new List<Natural>();

        for(int i=0; i<worldSizeX; i++)
        {
            for(int e=0; e<worldSizeY; e++)
            {
                objects[i, e] = "";
            }
        }

        MessagePump.instance.Register(this, MessageType.CITY_CREATED_MESSAGE, OnCityCreated);
        MessagePump.instance.Register(this, MessageType.GROUP_CREATED_MESSAGE, OnGroupCreated);
        MessagePump.instance.Register(this, MessageType.CAMP_CREATED_MESSAGE, OnCampCreated);
        MessagePump.instance.Register(this, MessageType.GROUP_MAP_POS_UPDATED_MESSAGE, OnGroupMapPosUpdated);
    }

    private void OnGroupMapPosUpdated(Message msg)
    {
        GroupMapPosUpdatedMessage cMsg = (GroupMapPosUpdatedMessage)msg;
        if (cMsg.oldMapPos != null)
        {
            objects[cMsg.oldMapPos.x, cMsg.oldMapPos.y] = "";
        }

        if (cMsg.newMapPos != null)
        {
            objects[cMsg.newMapPos.x, cMsg.newMapPos.y] = cMsg.groupID;
        }
    }

    public void Generate()
    {
        patches = new Dictionary<int, List<Vector2>>();
        ResetGrid();

        int[,] s = WorldGenerator.instance.GenerateWorld("RANDOM", worldSizeX, worldSizeY, 0.7f);
        for(int i=0; i<worldSizeX; i++)
        {
            for(int e=0; e<worldSizeY; e++)
            {
                grid[i, e, 0] = s[i, e];
            }
        }
        patches = WorldGenerator.instance.GetPatches();

        AddNaturals();

        isReady = true;
        MessagePump.instance.QueueMsg(new WorldReadyMessage(grid, naturals, worldSizeX, worldSizeY, zLevels));
    }

    private void AddNaturals()
    {
        foreach (NaturalDefinition nd in WorldDataController.instance.naturals)
        {
            for (int i = 0; i < nd.prevalence; i++)
            {
                naturals.Add(new Natural(Guid.NewGuid().ToString(), nd.naturalName, SampleRandomLand()));
            }
        }
    }

    private void OnCityCreated(Message msg)
    {
        CityCreatedMessage cMsg = (CityCreatedMessage)msg;

        MapPos m = cMsg.worldPos;
        objects[m.x, m.y] = cMsg.cityID;
    }

    private void OnGroupCreated(Message msg)
    {
        GroupCreatedMessage cMsg = (GroupCreatedMessage)msg;

        MapPos m = cMsg.worldPos;
        //objects[m.x, m.y] = cMsg.groupID;
    }

    private void OnCampCreated(Message msg)
    {
        CampCreatedMessage cMsg = (CampCreatedMessage)msg;

        MapPos m = cMsg.worldPos;
        objects[m.x, m.y] = cMsg.campID;
    }

    private void ResetGrid()
    {
        grid = new int[worldSizeX, worldSizeY, zLevels];
        for (int i = 0; i < worldSizeX; i++)
        {
            for (int j = 0; j < worldSizeY; j++)
            {
                grid[i, j, 0] = 0;
                for (int k = 1; k < zLevels; k++)
                {
                    grid[i, j, k] = -1;
                }
            }
        }
    }

    public MapPos SampleRandomLand()
    {
        Vector2 v = patches[1][UnityEngine.Random.Range(0, patches[1].Count-1)];
        return new MapPos((int)v.x, (int)v.y);
    }

    public MapPos SampleRandomCoastline()
    {
        Vector2 v;
        do
        {
            v = patches[1][UnityEngine.Random.Range(0, patches[1].Count - 1)];
        }
        while (!HasNeighbor(new MapPos((int)v.x, (int)v.y), 0));

        return new MapPos((int) v.x, (int)v.y);

    }

    public MapPos SampleRandomPassable()
    {
        Vector2 v;
        do
        {
            v = patches[1][UnityEngine.Random.Range(0, patches[1].Count - 1)];
        }
        while (grid[(int)v.x, (int)v.y, 0] != 1);

        return new MapPos((int)v.x, (int)v.y);
    }

    public MapPos SampleLandNeighbor(MapPos pos)
    {
        return GetNeighborWithValue(pos, 1);
    }

    public MapPos SampleWaterNeighbor(MapPos pos)
    {
        return GetNeighborWithValue(pos, 0);
    }

    public bool IsTileLand(MapPos pos)
    {
        if(grid[pos.x, pos.y, 0] > 0)
        {
            return true;
        }
        return false;
    }

    public bool IsTileWater(MapPos pos)
    {
        if(grid[pos.x, pos.y, 0] == 0 || CityController.instance.GetCity(objects[pos.x, pos.y]) != null)
        {
            return true;
        }
        return false;
    }

    public string GetObjectAtPos(MapPos pos)
    {
        return objects[pos.x, pos.y];
    }

    public Vector2 GetPosOfTile(MapPos pos)
    {
        return new Vector2(pos.x * tileWidth, pos.y * tileWidth);
    }

    public MapPos GetNeighborWithValue(MapPos targ, int neighbor)
    {
        if (grid[targ.x, targ.y + 1, 0] == neighbor)
        {
            return new MapPos(targ.x, targ.y + 1);
        }
        if (grid[targ.x + 1, targ.y + 1, 0] == neighbor)
        {
            return new MapPos(targ.x + 1, targ.y + 1);
        }
        if (grid[targ.x + 1, targ.y, 0] == neighbor)
        {
            return new MapPos(targ.x + 1, targ.y);
        }
        if (grid[targ.x + 1, targ.y - 1, 0] == neighbor)
        {
            return new MapPos(targ.x + 1, targ.y - 1);
        }
        if (grid[targ.x, targ.y - 1, 0] == neighbor)
        {
            return new MapPos(targ.x, targ.y - 1);
        }
        if (grid[targ.x - 1, targ.y - 1, 0] == neighbor)
        {
            return new MapPos(targ.x - 1, targ.y - 1);
        }
        if (grid[targ.x - 1, targ.y, 0] == neighbor)
        {
            return new MapPos(targ.x - 1, targ.y);
        }
        if (grid[targ.x - 1, targ.y + 1, 0] == neighbor)
        {
            return new MapPos(targ.x - 1, targ.y + 1);
        }

        return new MapPos();
    }

    public bool HasNeighbor(MapPos targ, int neighbor)
    {
        if (grid[targ.x, targ.y+1, 0] == neighbor)
        {
            return true;
        }
        if (grid[targ.x + 1, targ.y + 1, 0] == neighbor)
        {
            return true;
        }
        if (grid[targ.x + 1, targ.y, 0] == neighbor)
        {
            return true;
        }
        if (grid[targ.x + 1, targ.y - 1, 0] == neighbor)
        {
            return true;
        }
        if (grid[targ.x, targ.y - 1, 0] == neighbor)
        {
            return true;
        }
        if (grid[targ.x - 1, targ.y - 1, 0] == neighbor)
        {
            return true;
        }
        if (grid[targ.x - 1, targ.y, 0] == neighbor)
        {
            return true;
        }
        if (grid[targ.x - 1, targ.y + 1, 0] == neighbor)
        {
            return true;
        }

        return false;
    }
}
