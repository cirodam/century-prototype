﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceDataController : MonoBehaviour
{
    public static ResourceDataController instance;
    public List<ResourceDefinition> definitions;
    public List<WeaponDefinition> weapons;
    public List<VehicleDefinition> vehicles;

    Dictionary<string, List<ResourceDefinition>> byRole;

    public List<ResourceNodeDefinition> nodes;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        byRole = new Dictionary<string, List<ResourceDefinition>>();

        foreach(ResourceDefinition def in definitions)
        {
            if(!byRole.ContainsKey(def.resourceRole))
            {
                byRole.Add(def.resourceRole, new List<ResourceDefinition>());
            }
            byRole[def.resourceRole].Add(def);
        }
    }

    public List<ResourceDefinition> GetResourcesMeetingRole(string roleName)
    {
        if(byRole.ContainsKey(roleName))
        {
            return byRole[roleName];
        }
        return new List<ResourceDefinition>();
    }

    public ResourceDefinition GetDataForResource(string name)
    {
        foreach(ResourceDefinition r in definitions)
        {
            if(r.resourceName == name)
            {
                return r;
            }
        }
        return null;
    }

    public WeaponDefinition GetWeaponData(string name)
    {
        foreach(WeaponDefinition w in weapons)
        {
            if(w.weaponName == name)
            {
                return w;
            }
        }
        return null;
    }

    public VehicleDefinition GetVehicleData(string name)
    {
        foreach(VehicleDefinition v in vehicles)
        {
            if(v.vehicleName == name)
            {
                return v;
            }
        }
        return null;
    }

    public Sprite GetSpriteForResourceNode(string name)
    {
        foreach(ResourceNodeDefinition r in nodes)
        {
            if(r.resourceNodeName == name)
            {
                return Resources.Load<Sprite>(r.spritePath);
            }
        }
        return null;
    }
}
