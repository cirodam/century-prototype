﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICreator : MonoBehaviour
{
    public GameObject AIClientPrefab;
    public GameObject clientRoot;

    private void Awake()
    {
        Dictionary<string, ClientData> clients = SessionController.instance.GetClients();
        foreach (string id in clients.Keys)
        {
            if(clients[id].clientType == ClientType.CLIENT_TYPE_AI)
            {
                GameObject g = Instantiate(AIClientPrefab);
                g.transform.SetParent(clientRoot.transform);
                g.name = "AIClient";
                g.GetComponent<AIClient>().SetID(id);
                g.GetComponent<AIClient>().SetData(clients[id]);
            }
        }
    }
}
