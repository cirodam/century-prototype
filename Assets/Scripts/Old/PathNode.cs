﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode 
{
    public MapPos pos;
    public PathNode parent;
    public float distanceToParent;

    public int g;
    public int h;
    public int f;

    public PathNode(int newX, int newY)
    {
        pos = new MapPos(newX, newY);
    }
}
