﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopulationController : MonoBehaviour
{
    public static PopulationController instance;
    Dictionary<int, Population> populations;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        populations = new Dictionary<int, Population>();

        MessagePump.instance.Register(this, MessageType.CLIENT_FACTION_CHANGED_MESSAGE, OnFactionChanged);
        MessagePump.instance.Register(this, MessageType.STOCKPILE_CREATED_MESSAGE, OnStockpileCreated);
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
    }

    private void OnFactionChanged(Message msg)
    {
        ClientFactionChangedMessage cMsg = (ClientFactionChangedMessage)msg;

        if (!populations.ContainsKey(cMsg.clientID))
        {
            AddPopulation(cMsg.clientID);

            //if(StockpileController.instance.GetStockpile(cMsg.clientID) != null)
            //{
            //    populations[cMsg.clientID].stockpile = StockpileController.instance.GetStockpile(cMsg.clientID);
            //}
        }
    }

    private void OnStockpileCreated(Message msg)
    {
        StockpileCreatedMessage cMsg = (StockpileCreatedMessage)msg;

        //if(populations.ContainsKey(cMsg.stockpileID))
        //{
        //    populations[cMsg.stockpileID].stockpile = cMsg.stockpile;
        //}
    }

    private void OnNewDay(Message msg)
    {
        foreach(Population p in populations.Values)
        {
            p.OnNewDay();
        }
    }

    public void AddPopulation(int id)
    {
        if(!populations.ContainsKey(id))
        {
            populations.Add(id, new Population());
        }
    }

    public IPopulation GetPopulation(int id)
    {
        return populations[id];
    }
}
