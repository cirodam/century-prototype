﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionController : MonoBehaviour
{
    public static FactionController instance;
    public Dictionary<int, Faction> factions;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(instance);
        }
    }

    private void Init()
    {
        factions = new Dictionary<int, Faction>();

        MessagePump.instance.Register(this, MessageType.CLIENT_FACTION_CHANGED_MESSAGE, OnClientFactionChanged);
    }

    private void OnClientFactionChanged(Message msg)
    {
        ClientFactionChangedMessage cMsg = (ClientFactionChangedMessage)msg;

        if(!factions.ContainsKey(cMsg.clientID))
        {
            factions.Add(cMsg.clientID, cMsg.faction);
        }
        else
        {
            factions[cMsg.clientID] = cMsg.faction;
        }
    }

    public IFaction GetFaction(int factionID)
    {
        return factions[factionID];
    }
}
