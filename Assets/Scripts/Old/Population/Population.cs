﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Population : IPopulation
{
    int populationCount;
    public IStockpile stockpile;

    public void OnNewDay()
    {
        stockpile.SubtractResourceAmount("Wheat", stockpile.GetResourceAmount("Citizen"));
    }
}
