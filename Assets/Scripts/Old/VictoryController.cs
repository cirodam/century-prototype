﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryController : MonoBehaviour
{
    List<int> undefeated;

    private void Awake()
    {
        undefeated = new List<int>();

        MessagePump.instance.Register(this, MessageType.CITY_OWNER_CHANGED_MESSAGE, OnCityOwnerChanged);
        MessagePump.instance.Register(this, MessageType.CLIENT_DEFEATED_MESSAGE, OnClientDefeated);
    }

    private void OnCityOwnerChanged(Message msg)
    {
        CityOwnerChangedMessage cMsg = (CityOwnerChangedMessage)msg;

        if (CityController.instance.GetFactionCityCount(cMsg.oldOwnerID) <= 0)
        {
            MessagePump.instance.QueueMsg(new ClientDefeatedMessage(cMsg.oldOwnerID));
        }
    }

    private void OnClientDefeated(Message msg)
    {
        ClientDefeatedMessage cMsg = (ClientDefeatedMessage)msg;

        if (undefeated.Contains(cMsg.clientID))
        {
            undefeated.Remove(cMsg.clientID);
            if (undefeated.Count == 1)
            {
                MessagePump.instance.QueueMsg(new ClientGameVictoryMessage(undefeated[0]));
            }
        }
    }
}
