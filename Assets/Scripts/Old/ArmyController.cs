﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmyController : MonoBehaviour
{
    /*
    public static ArmyController instance;

    Dictionary<int, IGroup> groups;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        MessagePump.instance.Register(this, MessageType.NEW_DAY_MESSAGE, OnNewDay);
        MessagePump.instance.Register(this, MessageType.CLIENT_DEFEATED_MESSAGE, OnClientDefeated);

        groups = new Dictionary<int, IGroup>();
    }

    private void OnNewDay(Message msg)
    {
        foreach(IGroup i in groups.Values)
        {
            i.OnNewDay();
        }
    }

    private void OnClientDefeated(Message msg)
    {
        ClientDefeatedMessage cMsg = (ClientDefeatedMessage)msg;

        List<int> toRemove = new List<int>();
        foreach(IGroup group in groups.Values)
        {
            if(group.GetOwnerID() == cMsg.clientID)
            {
                toRemove.Add(group.GetGroupID());
            }
        }

        foreach(int i in toRemove)
        {
            groups.Remove(i);
        }
    }

    public void CreateGroup(string groupName, int factionID, MapPos pos, List<IUnit> units, MovementType movement, IStockpile stockpile)
    {
        IGroup group = null;

        if(movement == MovementType.MOVEMENT_LAND)
        {
            group = new Army(IDController.instance.GetNextID(), factionID, groupName, pos, units, stockpile);
        }
        else if(movement == MovementType.MOVEMENT_SEA)
        {
            group = new Fleet(IDController.instance.GetNextID(), factionID, groupName, pos, units, stockpile);
        }
        else if(movement == MovementType.MOVEMENT_AIR)
        {
            group = new Airgroup(IDController.instance.GetNextID(), factionID, groupName, pos, units, stockpile);
        }

        groups.Add(group.GetGroupID(), group);
        WorldController.instance.SetIDAtPos(pos, group.GetGroupID());

        MessagePump.instance.QueueMsg(new GroupCreatedMessage(group.GetGroupID(), group));
    }

    public void CreateStartingArmy(int factionID, MapPos pos)
    {
        List<IUnit> units = new List<IUnit>();
        for(int i=0; i<5; i++)
        {
            Battalion b = new Battalion();
            b.SetUnitID(i+1);
            b.currentSoldiers = 20;
            b.maxSoldiers = 20;
            b.SetWeapon("Sword");
            b.maxWeapons = 20;
            b.currentWeapons = 14;
            units.Add(b);
        }
        CreateGroup("Army", factionID, pos, units, MovementType.MOVEMENT_LAND, StockpileController.instance.GetStockpile(factionID));
    }

    public IGroup GetGroup(int id)
    {
        if(groups.ContainsKey(id))
        {
            return groups[id];
        }
        return null;
    }

    public Dictionary<int, IGroup> GetGroups()
    {
        return groups;
    }*/
}
