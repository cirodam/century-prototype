﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFaction 
{
    int GetFactionID();
    string GetFactionName();
    Color GetFactionColor();
}
