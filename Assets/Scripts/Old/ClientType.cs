﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ClientType 
{
    CLIENT_TYPE_PLAYER,
    CLIENT_TYPE_AI
}
