﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Faction : IFaction
{
    public int factionID;
    public string factionName;
    public Color factionColor;

    public Color GetFactionColor()
    {
        return factionColor;
    }

    public int GetFactionID()
    {
        return factionID;
    }

    public string GetFactionName()
    {
        return factionName;
    }
}
