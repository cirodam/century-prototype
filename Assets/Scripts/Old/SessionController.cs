﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SessionController : MonoBehaviour
{
    public static SessionController instance;

    private string localClient;
    private Dictionary<string, ClientData> sessionClients;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        sessionClients = new Dictionary<string, ClientData>();

        localClient = System.Guid.NewGuid().ToString();

        sessionClients.Add(localClient, new ClientData(ClientType.CLIENT_TYPE_PLAYER, "The Empire", Color.blue));
        sessionClients.Add(System.Guid.NewGuid().ToString(), new ClientData(ClientType.CLIENT_TYPE_AI, "The Kingdom", Color.red));
        sessionClients.Add(System.Guid.NewGuid().ToString(), new ClientData(ClientType.CLIENT_TYPE_AI, "The Republic", Color.green));
    }

    public string GetLocalClientID()
    {
        return localClient;
    }

    public Dictionary<string, ClientData> GetClients()
    {
        return sessionClients;
    }

    public int GetClientCount()
    {
        return sessionClients.Count;
    }

    public ClientData GetClientData(string clientID)
    {
        if(sessionClients.ContainsKey(clientID))
        {
            return sessionClients[clientID];
        }
        return new ClientData(ClientType.CLIENT_TYPE_AI, "Error", Color.black);
    }
}
