﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDController : MonoBehaviour
{
    public static IDController instance;
    int nextID = 0;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
        }
    }

    public int GetNextID()
    {
        nextID++;
        return nextID-1;
    }
}
