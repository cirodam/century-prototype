﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericDictionary
{
    public Dictionary<string, object> _dict = new Dictionary<string, object>();

    public void Add<T>(string key, T value)
    {
        _dict.Add(key, value);
    }

    public T GetValue<T>(string key) 
    {
        if(_dict.ContainsKey(key))
        {
            return (T)_dict[key];
        }

        return default;
    }
}
