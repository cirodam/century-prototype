﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MapPos 
{
    public int x;
    public int y;

    public MapPos()
    {
        x = 0;
        y = 0;
    }

    public MapPos(int newX, int newY)
    {
        x = newX;
        y = newY;
    }

    public bool Equals(MapPos other)
    {
        if (other == null)
        {
            return false;
        }

        if (x == other.x && y == other.y)
        {
            return true;
        }
        return false;
    }

    public static bool operator ==(MapPos a, MapPos b)
    {
        if (ReferenceEquals(a, null))
        {
            if (ReferenceEquals(b, null))
            {
                return true;
            }
            return false;
        }

        return a.Equals(b);
    }

    public static bool operator !=(MapPos a, MapPos b)
    {
        return !(a == b);
    }

    public override string ToString()
    {
        return "MapPos: " + x + " " + y;
    }
}
