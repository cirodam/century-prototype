﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampDataController : MonoBehaviour
{
    public static CampDataController instance;
    public List<CampDefinition> camps;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public CampDefinition GetDataForCamp(string campName)
    {
        foreach(CampDefinition cd in camps)
        {
            if(cd.campName == campName)
            {
                return cd;
            }
        }
        return null;
    }
}
